package com.nvlhrd.lmslearningsys;


import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import com.nvlhrd.lmslearningsys.adapter.JSONParser;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class forum extends Activity {

	ArrayList<HashMap<String, String>> contactList;
	// url to make request
	static String url;
	static String NAMA;
	static final String JUDUL = "a", JUDUL2 = "b", JUDUL3 = "c", JUDUL4 = "d",
			JUDUL5 = "e", JUDUL6 = "f", JUDUL7 = "g", JUDUL8 = "h",
			JUDUL9 = "i", JUDUL10 = "j", JUDUL11 = "k", JUDUL12 = "l";

	// JSON Node names
	public static final String TAG_CONTACTS = "info";
	public static final String TAG_JUDUL = "judul";
	public static final String TAG_DESKRIPSI = "deskripsi";
	public static final String TAG_FORUM = "idforum";
	public static final String TAG_IDUSER = "iduser";
	public static final String TAG_NAMA = "nama";
	public static final String TAG_TGL = "tgl";
	public static final String TAG_FOTO = "foto";
	public static final String TAG_KELAS = "kelas";
	String link;
	int current_page = 1;
	// contacts JSONArray
	JSONArray contacts = null;
	chatAdapterForum adapter2;
	ListView lv;
	TextView name2;
	LinearLayout linlaHeaderProgress;
	LinearLayout tidakhasil;
	String foto;

	// Progress Dialog
	private ProgressDialog pDialog;

	// Progress dialog type (0 - for Horizontal progress bar)
	public static final int progress_bar_type = 0;

	// File url to download
	private static String file_url = "http://api.androidhive.info/progressdialog/hive.jpg";
	File folder, folderpict;
	String txtjudul, txtfile;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list);

		lv = (ListView) findViewById(R.id.list);
		ActionBar actionBar = getActionBar();

		// Enabling Up / Back navigation
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setTitle("Forum");
		contactList = new ArrayList<HashMap<String, String>>();
		// Loading INBOX in Background Thread
		tidakhasil = (LinearLayout) findViewById(R.id.noitem);
		tidakhasil.setVisibility(View.GONE);

		url = "http://lms.novalherdinata.net/forum.php?page=1";

		//new LoadInbox().execute();
	}

	class LoadInbox extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
			linlaHeaderProgress.setVisibility(View.VISIBLE);
		}

		/**
		 * getting Inbox JSON
		 * */
		protected String doInBackground(String... args) {
			// Creating JSON Parser instance
			JSONParser jParser = new JSONParser();

			// getting JSON string from URL
			JSONObject json = jParser.getJSONFromUrl(url);

			try {
				// Getting Array of Contacts
				contacts = json.getJSONArray(TAG_CONTACTS);

				// looping through All Contacts
				for (int i = 0; i < contacts.length(); i++) {
					JSONObject c = contacts.getJSONObject(i);

					// Storing each json item in variable
					String judul = c.getString(TAG_JUDUL);
					String deskripsi = c.getString(TAG_DESKRIPSI);
					String idforum = c.getString(TAG_FORUM);
					String iduser = c.getString(TAG_IDUSER);
					String nama = c.getString(TAG_NAMA);
					String tgl = c.getString(TAG_TGL);
					String foto = c.getString(TAG_FOTO);
					String kelas = c.getString(TAG_KELAS);
				

					foto = foto.replaceAll(" ", "%20");

					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();

					// adding each child node to HashMap key => value
					// adding each child node to HashMap key => value
					map.put(TAG_JUDUL, judul);
					map.put(TAG_DESKRIPSI, deskripsi);
					map.put(TAG_FORUM, idforum);
					map.put(TAG_IDUSER, iduser);
					map.put(TAG_NAMA, nama);
					map.put(TAG_TGL, tgl);
					map.put(TAG_FOTO, foto);
					map.put(TAG_KELAS, kelas);
				

					// adding HashList to ArrayList
					contactList.add(map);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			// HIDE THE SPINNER AFTER LOADING FEEDS
			linlaHeaderProgress.setVisibility(View.GONE);

			if (contactList.size() < 1) {
				tidakhasil.setVisibility(View.VISIBLE);
			}
			// LoadMore button
			if (contactList.size() > 9) {

				Button btnLoadMore = new Button(forum.this);
				btnLoadMore.setText("Load More");

				// Adding Load More button to lisview at bottom
				lv.addFooterView(btnLoadMore);

				btnLoadMore.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// Starting a new async task
						new loadMoreListView().execute();
					}
				});

			}

			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					adapter2 = new chatAdapterForum(forum.this, contactList);
					adapter2.notifyDataSetChanged();
					lv.setAdapter(adapter2);
					lv.invalidateViews();

					lv.setOnItemClickListener(new OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							String judul = ((TextView) view.findViewById(R.id.title)).getText().toString();
							String deskripsi = ((TextView) view.findViewById(R.id.deskripsi)).getText().toString();
							String idforum = ((TextView) view.findViewById(R.id.idforum)).getText().toString();
							String iduser = ((TextView) view.findViewById(R.id.iduser)).getText().toString();
							String nama = ((TextView) view.findViewById(R.id.nama)).getText().toString();
							String foto = ((TextView) view.findViewById(R.id.foto)).getText().toString();
							String kelas = ((TextView) view.findViewById(R.id.kelas)).getText().toString();
							String tgl = ((TextView) view.findViewById(R.id.tglupload)).getText().toString();
							Intent in = new Intent(forum.this, forumkomentar.class);
							in.putExtra(JUDUL, judul);
							in.putExtra(JUDUL2, deskripsi);
							in.putExtra(JUDUL3, idforum);
							in.putExtra(JUDUL4, iduser);
							in.putExtra(JUDUL5, nama);
							in.putExtra(JUDUL6, foto);
							in.putExtra(JUDUL7, kelas);
							in.putExtra(JUDUL8, tgl);
							startActivity(in);
						}
					});
				}
			});
		}
	}

	private class loadMoreListView extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// Showing progress dialog before sending http request
			pDialog = new ProgressDialog(forum.this);
			pDialog.setMessage("Please wait..");
			pDialog.setIndeterminate(true);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		protected Void doInBackground(Void... unused) {
			// Creating JSON Parser instance
			current_page += 1;

			url = "http://lms.novalherdinata.net/forum.php?page="
					+ current_page;
			JSONParser jParser = new JSONParser();

			// getting JSON string from URL
			JSONObject json = jParser.getJSONFromUrl(url);

			try {
				// Getting Array of Contacts
				contacts = json.getJSONArray(TAG_CONTACTS);

				// looping through All Contacts
				for (int i = 0; i < contacts.length(); i++) {
					JSONObject c = contacts.getJSONObject(i);

					// Storing each json item in variable
					String judul = c.getString(TAG_JUDUL);
					String deskripsi = c.getString(TAG_DESKRIPSI);
					String idforum = c.getString(TAG_FORUM);
					String iduser = c.getString(TAG_IDUSER);
					String nama = c.getString(TAG_NAMA);
					String tgl = c.getString(TAG_TGL);
					String foto = c.getString(TAG_FOTO);
					String kelas = c.getString(TAG_KELAS);
				

					foto = foto.replaceAll(" ", "%20");

					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();

					// adding each child node to HashMap key => value
					// adding each child node to HashMap key => value
					map.put(TAG_JUDUL, judul);
					map.put(TAG_DESKRIPSI, deskripsi);
					map.put(TAG_FORUM, idforum);
					map.put(TAG_IDUSER, iduser);
					map.put(TAG_NAMA, nama);
					map.put(TAG_TGL, tgl);
					map.put(TAG_FOTO, foto);
					map.put(TAG_KELAS, kelas);
					contactList.add(map);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		protected void onPostExecute(Void unused) {
			// closing progress dialog
			pDialog.dismiss();

			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					int currentPosition = lv.getFirstVisiblePosition();
					adapter2 = new chatAdapterForum(forum.this, contactList);
					adapter2.notifyDataSetChanged();
					lv.setAdapter(adapter2);
					lv.invalidateViews();
					lv.setSelectionFromTop(currentPosition + 1, 0);
					lv.setOnItemClickListener(new OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							String judul = ((TextView) view.findViewById(R.id.title)).getText().toString();
							String deskripsi = ((TextView) view.findViewById(R.id.deskripsi)).getText().toString();
							String idforum = ((TextView) view.findViewById(R.id.idforum)).getText().toString();
							String iduser = ((TextView) view.findViewById(R.id.iduser)).getText().toString();
							String nama = ((TextView) view.findViewById(R.id.nama)).getText().toString();
							String foto = ((TextView) view.findViewById(R.id.foto)).getText().toString();
							String kelas = ((TextView) view.findViewById(R.id.kelas)).getText().toString();
							String tgl = ((TextView) view.findViewById(R.id.tglupload)).getText().toString();
							Intent in = new Intent(forum.this, forumkomentar.class);
							in.putExtra(JUDUL, judul);
							in.putExtra(JUDUL2, deskripsi);
							in.putExtra(JUDUL3, idforum);
							in.putExtra(JUDUL4, iduser);
							in.putExtra(JUDUL5, nama);
							in.putExtra(JUDUL6, foto);
							in.putExtra(JUDUL7, kelas);
							in.putExtra(JUDUL8, tgl);
							startActivity(in);
						}
					});
				}
			});
		}
	}

	/**
	 * Showing Dialog
	 * */
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case progress_bar_type:
			pDialog = new ProgressDialog(this);
			pDialog.setMessage("Downloading file. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setMax(100);
			pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			pDialog.setCancelable(true);
			pDialog.show();
			return pDialog;
		default:
			return null;
		}
	}

	/**
	 * Background Async Task to download file
	 * */
	class DownloadFileFromURL extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Bar Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showDialog(progress_bar_type);
		}

		/**
		 * Downloading file in background thread
		 * */
		@Override
		protected String doInBackground(String... f_url) {
			int count;
			try {
				String extStorageDirectory = Environment
						.getExternalStorageDirectory().toString();
				folderpict = new File(extStorageDirectory, "/MobileLearning");
				if (!folderpict.exists()) {
					folderpict.mkdir();
				} else {
					folder = new File(extStorageDirectory,
							"/MobileLearning/Course");
					if (!folder.exists()) {
						folder.mkdir();
					}

				}

				URL url = new URL(f_url[0]);
				URLConnection conection = url.openConnection();
				conection.connect();
				// getting file length
				int lenghtOfFile = conection.getContentLength();

				// input stream to read file - with 8k buffer
				InputStream input = new BufferedInputStream(url.openStream(),
						8192);

				// Output stream to write file
				OutputStream output = new FileOutputStream(extStorageDirectory
						+ "/MobileLearning/Course/" + txtjudul+".pdf");

				byte data[] = new byte[1024];

				long total = 0;

				while ((count = input.read(data)) != -1) {
					total += count;
					// publishing the progress....
					// After this onProgressUpdate will be called
					publishProgress("" + (int) ((total * 100) / lenghtOfFile));

					// writing data to file
					output.write(data, 0, count);
				}

				// flushing output
				output.flush();
				MediaScannerConnection
						.scanFile(forum.this,
								new String[] { extStorageDirectory
										+ "/MobileLearning/Course/" + txtjudul+".pdf" },
								new String[] { "application/pdf" }, null);
				// closing streams
				output.close();
				input.close();

			} catch (Exception e) {
				Log.e("Error: ", e.getMessage());
			}

			return null;
		}

		/**
		 * Updating progress bar
		 * */
		protected void onProgressUpdate(String... progress) {
			// setting progress percentage
			pDialog.setProgress(Integer.parseInt(progress[0]));
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		@Override
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after the file was downloaded
			dismissDialog(progress_bar_type);
			showPdf();

			// Displaying downloaded image into image view
			// Reading image path from sdcard
		}

	}
	
	 public void showPdf()
     {
		 
		 String extStorageDirectory = Environment
					.getExternalStorageDirectory().toString();
         File file = new File( extStorageDirectory
					+ "/MobileLearning/Course/" + txtjudul+".pdf" );
         PackageManager packageManager = getPackageManager();
         Intent testIntent = new Intent(Intent.ACTION_VIEW);
         testIntent.setType("application/pdf");
         List list = packageManager.queryIntentActivities(testIntent, PackageManager.MATCH_DEFAULT_ONLY);
         Intent intent = new Intent();
         intent.setAction(Intent.ACTION_VIEW);
         Uri uri = Uri.fromFile(file);
         intent.setDataAndType(uri, "application/pdf");
         startActivity(intent);
     }
	 
	 @Override
		public boolean onCreateOptionsMenu(Menu menu) {
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.main, menu);
			
			MenuItem tambah = (MenuItem) menu.findItem(R.id.action_tambah);
			
			return super.onCreateOptionsMenu(menu);
			
		}
		
		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			// toggle nav drawer on selecting action bar app icon/title
		
			// Handle action bar actions click
			switch (item.getItemId()) {
			case R.id.action_tambah:
					Intent intent = new Intent(this, inputforum.class);
					startActivity(intent);
				
				break;
			default:
				return super.onOptionsItemSelected(item);
			}
			return true;
		}
		
		
		  @Override
			public boolean onKeyDown(int keyCode, KeyEvent event)  {
			    if (keyCode == KeyEvent.KEYCODE_BACK ) {
			        // do something on back.
			    	  Intent intent = new Intent(forum.this, dashboard.class);
			    	  intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				        startActivity(intent);
			        return true;
			    }

			    return super.onKeyDown(keyCode, event);
			}


}