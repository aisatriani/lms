package com.nvlhrd.lmslearningsys;

import java.util.ArrayList;
import java.util.HashMap;


import com.nvlhrd.lmslearningsys.adapter.ImageLoader;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class chatAdapterRapor extends BaseAdapter {
    
    private Activity activity;
    private ArrayList<HashMap<String, String>> data;
    private static LayoutInflater inflater=null;
    public ImageLoader imageLoader; 
    
    public chatAdapterRapor(Activity fragmentActivity,  ArrayList<HashMap<String, String>> d) {
        activity = fragmentActivity;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader=new ImageLoader(activity.getApplicationContext());
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
//        	adapter = new SimpleAdapter(
//					chatutama.this, inboxList,
//					R.layout.inbox_list_item, new String[] { TAG_NAME, TAG_MESS },
//					new int[] { R.id.from, R.id.subject});
        	 vi = inflater.inflate(R.layout.layout_rapor, null);

        TextView judultugas = (TextView)vi.findViewById(R.id.judultugasrapor);
        TextView kelas = (TextView)vi.findViewById(R.id.kelasrapor);
        TextView nilai = (TextView)vi.findViewById(R.id.nilairapor);
        HashMap<String, String> song = new HashMap<String, String>();
        song = data.get(position);
        
        // Setting all values in listview
        judultugas.setText(song.get(raporlist.TAG_JUDULTUGAS));
        kelas.setText(song.get(raporlist.TAG_KELAS));
        nilai.setText("Nilai: "+song.get(raporlist.TAG_NILAI));
        
        return vi;
    }
}