package com.nvlhrd.lmslearningsys;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.ls.LSInput;


import com.nvlhrd.lmslearningsys.adapter.JSONParser;
import com.nvlhrd.lmslearningsys.api.ApiGenerator;
import com.nvlhrd.lmslearningsys.api.ServiceRaport;
import com.nvlhrd.lmslearningsys.api.pojo.NilaiTugas;
import com.nvlhrd.lmslearningsys.library.DialogInfo;
import com.nvlhrd.lmslearningsys.library.SessionManager;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class raporlist extends Activity {

    private ProgressDialog pDialog;
    ArrayList<HashMap<String, String>> contactList;
    // url to make request
    static String url;
    static String NAMA;
    static final String JUDUL = "a", JUDUL2 = "b", JUDUL3 = "c", JUDUL4 = "d", JUDUL5 = "e", JUDUL6 = "f", JUDUL7 = "g", JUDUL8 = "h", JUDUL9 = "i", JUDUL10 = "j", JUDUL11 = "k", JUDUL12 = "l";

    // JSON Node names
    public static final String TAG_CONTACTS = "info";
    public static final String TAG_JUDULTUGAS = "judul";
    public static final String TAG_KELAS = "kelas";
    public static final String TAG_NILAI = "nilai";
    String link;
    int current_page = 1;
    // contacts JSONArray
    JSONArray contacts = null;
    chatAdapterRapor adapter2;
    ListView lv;
    TextView name2;
    LinearLayout linlaHeaderProgress;
    LinearLayout tidakhasil;
    String foto;
    String txtjudul, txtdeskripsi, txtidtugas, txtidguru, txtnama, txtfoto, txtfile, txttglupload;
    SessionManager session;
    HashMap<String, String> user;
    String nama;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list);
        session = new SessionManager(getApplicationContext());
        session.checkLogin();
        user = session.getUserDetails();

        lv = (ListView) findViewById(R.id.list);
        ActionBar actionBar = getActionBar();

        // Enabling Up / Back navigation
        actionBar.setDisplayHomeAsUpEnabled(true);
        contactList = new ArrayList<HashMap<String, String>>();

        Intent in = getIntent();
        nama = in.getStringExtra(JUDUL);

        tidakhasil = (LinearLayout) findViewById(R.id.noitem);
        tidakhasil.setVisibility(View.GONE);
        if (dashboard.JUDULSTATUS.equals("s")) {
            actionBar.setTitle("Rapor " + nama);

            url = "http://lms.novalherdinata.net/viewnilaisiswa.php?nama=" + user.get(SessionManager.KEY_NAME);

            //new LoadInbox().execute();
            loadNilaiSiswa();
        }
        if (dashboard.JUDULSTATUS.equals("g")) {
            actionBar.setTitle("LOG Aktvitas Tugas");

            url = "http://lms.novalherdinata.net/viewnilaiguru.php?nama=" + user.get(SessionManager.KEY_NAME);

            //new LoadInbox().execute();
            loadNilaiGuru();
        }
    }

    private void loadNilaiGuru() {
        linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
        linlaHeaderProgress.setVisibility(View.VISIBLE);

        ServiceRaport service = ApiGenerator.createService(ServiceRaport.class);
        Call<List<NilaiTugas>> call = service.getRaportByGuru(user.get(SessionManager.KEY_EMAIL));
        call.enqueue(new Callback<List<NilaiTugas>>() {
            @Override
            public void onResponse(Call<List<NilaiTugas>> call, Response<List<NilaiTugas>> response) {
                linlaHeaderProgress.setVisibility(View.GONE);
                List<NilaiTugas> list = response.body();
                for (NilaiTugas nilai : list){

                    if (nilai.getTugas() != null) {
                        // creating new HashMap
                        HashMap<String, String> map = new HashMap<String, String>();

                        // adding each child node to HashMap key => value
                        // adding each child node to HashMap key => value

                        map.put(TAG_JUDULTUGAS, nilai.getTugas().getJudul());

                        map.put(TAG_KELAS, nilai.getKelas());
                        map.put(TAG_NILAI, nilai.getNilai());

                        // adding HashList to ArrayList
                        contactList.add(map);
                    }

                }

                populateList();

            }

            @Override
            public void onFailure(Call<List<NilaiTugas>> call, Throwable t) {
                linlaHeaderProgress.setVisibility(View.GONE);
                DialogInfo.showErrorDialog(raporlist.this, getString(R.string.error_connection));
            }
        });
    }

    private void populateList() {
        adapter2 = new chatAdapterRapor(raporlist.this, contactList);
        adapter2.notifyDataSetChanged();
        lv.setAdapter(adapter2);
        lv.invalidateViews();

        lv.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {


            }
        });
    }


    private void loadNilaiSiswa() {
        linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
        linlaHeaderProgress.setVisibility(View.VISIBLE);

        ServiceRaport service = ApiGenerator.createService(ServiceRaport.class);
        Call<List<NilaiTugas>> call = service.getRaportBySiswa(user.get(SessionManager.KEY_EMAIL));
        call.enqueue(new Callback<List<NilaiTugas>>() {
            @Override
            public void onResponse(Call<List<NilaiTugas>> call, Response<List<NilaiTugas>> response) {
                linlaHeaderProgress.setVisibility(View.GONE);
                List<NilaiTugas> list = response.body();
                for (NilaiTugas nilai : list){

                    // creating new HashMap
                    HashMap<String, String> map = new HashMap<String, String>();

                    // adding each child node to HashMap key => value
                    // adding each child node to HashMap key => value
                    map.put(TAG_JUDULTUGAS, nilai.getTugas().getJudul());
                    map.put(TAG_KELAS, nilai.getKelas());
                    map.put(TAG_NILAI, nilai.getNilai());

                    // adding HashList to ArrayList
                    contactList.add(map);

                }

                populateList();

            }

            @Override
            public void onFailure(Call<List<NilaiTugas>> call, Throwable t) {
                linlaHeaderProgress.setVisibility(View.GONE);
                DialogInfo.showErrorDialog(raporlist.this, getString(R.string.error_connection));
            }
        });
    }

    class LoadInbox extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
            linlaHeaderProgress.setVisibility(View.VISIBLE);
        }

        /**
         * getting Inbox JSON
         */
        protected String doInBackground(String... args) {
            // Creating JSON Parser instance
            JSONParser jParser = new JSONParser();

            // getting JSON string from URL
            JSONObject json = jParser.getJSONFromUrl(url);

            try {
                // Getting Array of Contacts
                contacts = json.getJSONArray(TAG_CONTACTS);

                // looping through All Contacts
                for (int i = 0; i < contacts.length(); i++) {
                    JSONObject c = contacts.getJSONObject(i);

                    // Storing each json item in variable
                    String judultugas = c.getString(TAG_JUDULTUGAS);
                    String kelas = c.getString(TAG_KELAS);
                    String nilai = c.getString(TAG_NILAI);


                    // creating new HashMap
                    HashMap<String, String> map = new HashMap<String, String>();

                    // adding each child node to HashMap key => value
                    // adding each child node to HashMap key => value
                    map.put(TAG_JUDULTUGAS, judultugas);
                    map.put(TAG_KELAS, kelas);
                    map.put(TAG_NILAI, nilai);

                    // adding HashList to ArrayList
                    contactList.add(map);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products
            // HIDE THE SPINNER AFTER LOADING FEEDS
            linlaHeaderProgress.setVisibility(View.GONE);

            if (contactList.size() < 1) {
                tidakhasil.setVisibility(View.VISIBLE);
            }
            // LoadMore button
            if (contactList.size() > 9) {


                Button btnLoadMore = new Button(raporlist.this);
                btnLoadMore.setText("Load More");

                // Adding Load More button to lisview at bottom
                lv.addFooterView(btnLoadMore);

                btnLoadMore.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        // Starting a new async task
                        new loadMoreListView().execute();
                    }
                });

            }

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    adapter2 = new chatAdapterRapor(raporlist.this, contactList);
                    adapter2.notifyDataSetChanged();
                    lv.setAdapter(adapter2);
                    lv.invalidateViews();

                    lv.setOnItemClickListener(new OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                int position, long id) {


                        }
                    });
                }
            });
        }
    }

    private class loadMoreListView extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            // Showing progress dialog before sending http request
            pDialog = new ProgressDialog(
                    raporlist.this);
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        protected Void doInBackground(Void... unused) {
            // Creating JSON Parser instance
            current_page += 1;

            url = "http://lms.novalherdinata.net/nilaitugasguru.php?nama=" + txtidtugas + "&page=" + current_page;
            JSONParser jParser = new JSONParser();

            // getting JSON string from URL
            JSONObject json = jParser.getJSONFromUrl(url);

            try {
                // Getting Array of Contacts
                contacts = json.getJSONArray(TAG_CONTACTS);

                // looping through All Contacts
                for (int i = 0; i < contacts.length(); i++) {
                    JSONObject c = contacts.getJSONObject(i);

                    // Storing each json item in variable
                    String judultugas = c.getString(TAG_JUDULTUGAS);
                    String kelas = c.getString(TAG_KELAS);
                    String nilai = c.getString(TAG_NILAI);


                    // creating new HashMap
                    HashMap<String, String> map = new HashMap<String, String>();

                    // adding each child node to HashMap key => value
                    // adding each child node to HashMap key => value
                    map.put(TAG_JUDULTUGAS, judultugas);
                    map.put(TAG_KELAS, kelas);
                    map.put(TAG_NILAI, nilai);

                    // adding HashList to ArrayList
                    contactList.add(map);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(Void unused) {
            // closing progress dialog
            pDialog.dismiss();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    int currentPosition = lv.getFirstVisiblePosition();
                    adapter2 = new chatAdapterRapor(raporlist.this, contactList);
                    adapter2.notifyDataSetChanged();
                    lv.setAdapter(adapter2);
                    lv.invalidateViews();
                    lv.setSelectionFromTop(currentPosition + 1, 0);
                    lv.setOnItemClickListener(new OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                int position, long id) {

                        }
                    });
                }
            });
        }
    }

}