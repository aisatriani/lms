package com.nvlhrd.lmslearningsys;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Random;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;


import com.nvlhrd.lmslearningsys.library.SessionManager;

import android.app.ActionBar;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class OnlineText extends Activity {

	private static final String TAG = OnlineText.class.getCanonicalName();
	EditText judul, deskripsi;
	TextView idcourse;
	Button btnupload;
	
	//upload
	 private static final int SELECT_FILE1 = 1;
	  String selectedPath1 = "NONE";
	  HttpEntity resEntity;
	  ImageView fileupload;
	  String a,b,c,d,e,f,g,h,i;
	  
	  //array yang akan di update di database sql handler
		 String[] kelas = {   
				 	"X ANKIM 1"
			        ,"X ANKIM 2"
			        ,"XI ANKIM 1"
			        ,"XI ANKIM 2"
			        ,"XII ANKIM 1"
			        ,"XII ANKIM 2"
			        ,"X TKJ 1"
			        ,"X TKJ 2"
			        ,"X TKJ 3"
			        ,"XI TKJ 1"
			        ,"XI TKJ 2"
			        ,"XII TKJ 1"
			        ,"XII TKJ 2"
			        ,"XII TKJ 3"
			        ,"X RPL 1"
			        ,"X RPL 2"
			        ,"XI RPL 1"
			        ,"XI RPL 2"
			        ,"XII RPL 1"
			        ,"XII RPL 2"
			        ,"X MM 1"
			        ,"X MM 2"
			        ,"XI MM 1"
			        ,"XI MM 2"
			        ,"XII MM 1"
			        ,"XII MM 2"
			        ,"X TP3R 1"
			        ,"XI TP3R 1"
			        ,"XII TP3R 1"
			        ,"X UPW 1"
			        ,"XI UPW 1"
			        ,"XI UPW 2"
			        ,"XII UPW 1"
			        ,"XII UPW 2"
			        ,"X AK 1"
			        ,"X AK 2"
			        ,"X AK 3"
			        ,"X AK 4"
			        ,"X AK 5"
			        ,"XI AK 1"
			        ,"XI AK 2"
			        ,"XI AK 3"
			        ,"XI AK 4"
			        ,"XI AK 5"
			        ,"XII AK 1"
			        ,"XII AK 2"
			        ,"XII AK 3"
			        ,"XII AK 4"
			        ,"XII AK 5"
			        ,"XII AK 6"
			        ,"X AP 1"
			        ,"X AP 2"
			        ,"X AP 3"
			        ,"X AP 4"
			        ,"X AP 5"
			        ,"X AP 6"
			        ,"X AP 7"
			        ,"XI AP 1"
			        ,"XI AP 2"
			        ,"XI AP 3"
			        ,"XI AP 4"
			        ,"XI AP 5"
			        ,"XI AP 6"
			        ,"XII AP 1"
			        ,"XII AP 2"
			        ,"XII AP 3"
			        ,"XII AP 4"
			        ,"XII AP 5"
			        ,"XII AP 6"
			        ,"X PEMS 1"
			        ,"X PEMS 2"
			        ,"XI PEMS 1"
			        ,"XII PEMS 1"
			        ,"XII PEMS 2"};
		 Spinner spinner;
		 int randomNo;
		 ProgressDialog progressDialog;
		 Button btndate;
		 
		 //datepicker
		 
		 private DatePicker datePicker;
		   private Calendar calendar;
		   private int year, month, day;
		   
		   SessionManager session;
			HashMap<String, String> user;
	  
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
		setContentView(R.layout.onlinetext);
		
		 session = new SessionManager(getApplicationContext());
	        session.checkLogin();
			 user = session.getUserDetails();
				ActionBar actionBar = getActionBar();

				// Enabling Up / Back navigation
				actionBar.setDisplayHomeAsUpEnabled(true);
				actionBar.setTitle("Online Tugas");
		
		judul = (EditText) findViewById(R.id.onlinejudul);
		deskripsi = (EditText) findViewById(R.id.onlinetanya);
		btnupload = (Button) findViewById(R.id.btnonline);
		spinner = (Spinner) findViewById(R.id.spinnerkelas);
		idcourse = (TextView) findViewById(R.id.idcourse);
		btndate = (Button) findViewById(R.id.btndate);
		
		
		   // Creating adapter for spinner
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, kelas);
					
					// Drop down layout style - list view with radio button
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(dataAdapter);
		
		 Random r = new Random();
		    randomNo = r.nextInt(100000000+1);
		    actionBar.setSubtitle("ID Tugas: 1"+randomNo);
		
		btnupload.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(judul.getText().toString().equalsIgnoreCase("")){
					judul.setError("Tidak boleh kosong");
				}
				if(deskripsi.getText().toString().equalsIgnoreCase("")){
					deskripsi.setError("Tidak boleh kosong");
				}if(btndate.getText().toString().equalsIgnoreCase("Atur Tanggal Pengumpulan")){
					 Toast.makeText(OnlineText.this, "Tanggal pengumpulan harus di set", Toast.LENGTH_SHORT).show();
				}
				
				if(!btndate.getText().toString().equalsIgnoreCase("Atur Tanggal Pengumpulan") && !judul.getText().toString().equalsIgnoreCase("") && !deskripsi.getText().toString().equalsIgnoreCase("")){
	
                
                	progressDialog = ProgressDialog.show(OnlineText.this, "", "Proses.....", false);
                     Thread thread=new Thread(new Runnable(){
                            public void run(){
                           	 doinput();
                                runOnUiThread(new Runnable(){
                                    public void run() {
                                        if(progressDialog.isShowing())
                                            progressDialog.dismiss();
//                                        Intent intent2 = new Intent(up.this, MainActivity.class);
//                            			startActivity(intent2);
                                    }
                                });
                            }
                    });
                    thread.start();
                    Toast.makeText(OnlineText.this, "Berhasil di tambahkan", Toast.LENGTH_SHORT).show();
 				   Intent intent = new Intent(OnlineText.this, listtugas.class);
 			        startActivity(intent);	  
			}
			}
		});
		
		btndate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				calendar = Calendar.getInstance();
			      year = calendar.get(Calendar.YEAR);
			      month = calendar.get(Calendar.MONTH);
			      day = calendar.get(Calendar.DAY_OF_MONTH);
			      showDialog(999);
			}
		});
		
		//date
		
	
	      
	}
	

	   @Override
	   protected Dialog onCreateDialog(int id) {
	   // TODO Auto-generated method stub
	      if (id == 999) {
	         return new DatePickerDialog(this, myDateListener, year, month, day);
	       }
	      return null;
	   }

	   private DatePickerDialog.OnDateSetListener myDateListener
	   = new DatePickerDialog.OnDateSetListener() {

	   @Override
	   public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
	      // TODO Auto-generated method stub
	      // arg1 = year
	      // arg2 = month
	      // arg3 = day
	      showDate(arg1, arg2+1, arg3);
	   }
	   };

	   private void showDate(int year, int month, int day) {
	      btndate.setText(new StringBuilder().append(year).append("-")
	      .append(month).append("-").append(day));
	   }
	
	 public void openGallery(int req_code){
		 
	        Intent intent = new Intent();
	        intent.setType("application/pdf");
	        intent.setAction(Intent.ACTION_GET_CONTENT);
	        startActivityForResult(Intent.createChooser(intent,"Select file to upload "), req_code);
	   }
	 
	 public void onActivityResult(int requestCode, int resultCode, Intent data) {
		 
	        if (resultCode == RESULT_OK) {
	            Uri selectedImageUri = data.getData();
	            if (requestCode == SELECT_FILE1)
	            {
	                selectedPath1 = getPath(selectedImageUri);
//	                fotologo.setImageResource(R.drawable.checklis);
	                fileupload.setImageResource(R.drawable.checklis);
	                System.out.println("selectedPath1 : " + selectedPath1);
	            }
	        }
	        }
	  public String getPath(Uri uri) {
	        String[] projection = { MediaStore.Images.Media.DATA };
	        Cursor cursor = managedQuery(uri, projection, null, null, null);
	        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
	        cursor.moveToFirst();
	        return cursor.getString(column_index);
	    }
	  
	  private void doinput() {
			// TODO Auto-generated method stub

		  	a = "2"+randomNo;
		  	b = ""+user.get(SessionManager.KEY_EMAIL);
		  	c= judul.getText().toString();
		  	d = deskripsi.getText().toString();
		  	StringBuilder time = new StringBuilder().append(year).append("-")
		  	      .append(month).append("-").append(day);
			
			 //String input_data= "http://lms.novalherdinata.net/inputtugasguru.php"; //URL website anda dengan file insert.php yang telah dibuat
		  	String input_data = Config.URL_API + "tugas";

	            
	         HttpClient httpClient = new DefaultHttpClient();  
	         HttpPost httpPost = new HttpPost(input_data);  
	         ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();  
	         param.add(new BasicNameValuePair("idtugas", a));
	         param.add(new BasicNameValuePair("idguru", b));
	        param.add(new BasicNameValuePair("judul", c)); 
	        param.add(new BasicNameValuePair("deskripsi", d)); 
	        param.add(new BasicNameValuePair("kelas", kelas[spinner.getSelectedItemPosition()])); 
	        param.add(new BasicNameValuePair("timestop", btndate.getText().toString()));
	         try {  
	              httpPost.setEntity(new UrlEncodedFormEntity(param));  
	              HttpResponse httpRespose = httpClient.execute(httpPost);  
	             HttpEntity httpEntity = httpRespose.getEntity();  
	             InputStream in = httpEntity.getContent();  
	             BufferedReader read = new BufferedReader(new InputStreamReader(in));  
	            
	             String isi= "";  
	             String baris= "";  
	            
	             while((baris = read.readLine())!=null){  
	                isi+= baris;  
	             }

				 System.out.println(isi);

				 //Jika isi tidak sama dengan "null " maka akan tampil Toast "Berhasil" sebaliknya akan tampil "Gagal"
	             if(!isi.equals("null")){                    
	             }else{  
	             }  
	            
	       } catch (ClientProtocolException e) {  
	          // TODO Auto-generated catch block  
	          e.printStackTrace();  
	       } catch (IOException e) {  
	          // TODO Auto-generated catch block  
	          e.printStackTrace();  
	       }  
	    	
		}
	  
	  private void doFileUpload(){
		  
	        File file1 = new File(selectedPath1);
	        String urlString = "http://lms.novalherdinata.net/uploadtugasguru.php?idiklan="+randomNo;
	        try
	        {
	             HttpClient client = new DefaultHttpClient();
	             HttpPost post = new HttpPost(urlString);
	             FileBody bin1 = new FileBody(file1);
	             MultipartEntity reqEntity = new MultipartEntity();
	             reqEntity.addPart("uploadedfile1", bin1);
	             reqEntity.addPart("user", new StringBody("User"));
	             post.setEntity(reqEntity);
	             HttpResponse response = client.execute(post);
	             resEntity = response.getEntity();
	             final String response_str = EntityUtils.toString(resEntity);
	             if (resEntity != null) {
	                 Log.i("RESPONSE",response_str);
	                 runOnUiThread(new Runnable(){
	                        public void run() {
	                             try {
	                                Toast.makeText(getApplicationContext(),"Upload Complete.", Toast.LENGTH_LONG).show();
	                            } catch (Exception e) {
	                                e.printStackTrace();
	                            }
	                           }
	                    });
	             }
	        }
	        catch (Exception ex){
	             Log.e("Debug", "error: " + ex.getMessage(), ex);
	        }
	      }
	  
	  @Override
		public boolean onKeyDown(int keyCode, KeyEvent event)  {
		    if (keyCode == KeyEvent.KEYCODE_BACK ) {
		        // do something on back.
		    	  Intent intent = new Intent(OnlineText.this, listtugas.class);
		    	  intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			        startActivity(intent);
		        return true;
		    }

		    return super.onKeyDown(keyCode, event);
		}
}
