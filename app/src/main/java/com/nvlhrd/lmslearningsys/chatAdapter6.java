package com.nvlhrd.lmslearningsys;

import java.util.ArrayList;
import java.util.HashMap;

import com.nvlhrd.lmslearningsys.adapter.ImageLoader;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class chatAdapter6 extends BaseAdapter {
    
    private Activity activity;
    private ArrayList<HashMap<String, String>> data;
    private static LayoutInflater inflater=null;
    public ImageLoader imageLoader; 
    
    public chatAdapter6(Activity fragmentActivity,  ArrayList<HashMap<String, String>> d) {
        activity = fragmentActivity;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader=new ImageLoader(activity.getApplicationContext());
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }
    
    public void remove() {
        data.clear();
      }


    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
//        	adapter = new SimpleAdapter(
//					chatutama.this, inboxList,
//					R.layout.inbox_list_item, new String[] { TAG_NAME, TAG_MESS },
//					new int[] { R.id.from, R.id.subject});
        	 vi = inflater.inflate(R.layout.list_item, null);

        TextView name = (TextView)vi.findViewById(R.id.nama);
        TextView email = (TextView)vi.findViewById(R.id.email);
        TextView kelas = (TextView)vi.findViewById(R.id.kelas);
        ImageView thumb_image=(ImageView)vi.findViewById(R.id.foto);
        TextView file = (TextView)vi.findViewById(R.id.file);
        TextView txtfoto = (TextView)vi.findViewById(R.id.txtfoto);
        TextView txtnilai = (TextView)vi.findViewById(R.id.nilai);
        HashMap<String, String> song = new HashMap<String, String>();
        song = data.get(position);
        
        // Setting all values in listview
        name.setText(song.get(list.TAG_NAME));
        email.setText(song.get(list.TAG_EMAIL));
        kelas.setText(song.get(list.TAG_KELAS));
        imageLoader.DisplayImage(song.get(list.TAG_FOTO), thumb_image);
        file.setText(song.get(list.TAG_FILE));
        txtfoto.setText(song.get(list.TAG_FOTO));
        txtnilai.setText(song.get(list.TAG_NILAI));
        	
        return vi;
    }
}