package com.nvlhrd.lmslearningsys;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.ls.LSInput;


import com.nvlhrd.lmslearningsys.adapter.JSONParser;
import com.nvlhrd.lmslearningsys.api.ApiGenerator;
import com.nvlhrd.lmslearningsys.api.ServiceTugas;
import com.nvlhrd.lmslearningsys.api.pojo.NilaiTugas;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class list extends Activity {

	private ProgressDialog pDialog;
	ArrayList<HashMap<String, String>> contactList;
	// url to make request
	static String url;
	static String NAMA;
	static final String JUDUL ="a", JUDUL2="b", JUDUL3="c", JUDUL4="d",JUDUL5="e",JUDUL6="f",JUDUL7="g",JUDUL8="h",JUDUL9="i",JUDUL10="j",JUDUL11="k",JUDUL12="l";
	
	// JSON Node names
	public static final String TAG_CONTACTS = "info";
	public static final String TAG_NAME = "nama";
	public static final String TAG_EMAIL = "email";
	public static final String TAG_KELAS = "kelas";
	public static final String TAG_FOTO = "foto";
	public static final String TAG_FILE = "file";
	public static final String TAG_NILAI = "nilai";
	String link;
	int current_page = 1;
	// contacts JSONArray
	JSONArray contacts = null;
	chatAdapter6 adapter2;
	ListView lv;
	TextView name2;
	LinearLayout linlaHeaderProgress;
	LinearLayout tidakhasil;
	String foto;
	String txtjudul, txtdeskripsi, txtidtugas, txtidguru, txtnama, txtfoto, txtfile, txttglupload;
	char first ;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list);
		
		lv = (ListView) findViewById(R.id.list);
		 ActionBar actionBar = getActionBar();
		 
	        // Enabling Up / Back navigation
	        actionBar.setDisplayHomeAsUpEnabled(true);
	        actionBar.setTitle("");
		contactList = new ArrayList<HashMap<String, String>>();

	        tidakhasil = (LinearLayout) findViewById(R.id.noitem);
	        tidakhasil.setVisibility(View.GONE);
	        
	
		
		Intent in = getIntent();
		 txtjudul = in.getStringExtra(JUDUL);
		 txtdeskripsi = in.getStringExtra(JUDUL2);
		 txtfile = in.getStringExtra(JUDUL3);
		 txtidguru = in.getStringExtra(JUDUL4);
		 txtnama = in.getStringExtra(JUDUL5);
		 txttglupload = in.getStringExtra(JUDUL6);
		 txtidtugas = in.getStringExtra(JUDUL7);
		 txtfoto = in.getStringExtra(JUDUL8);
	
		 first = txtidtugas.charAt(0);
		 actionBar.setTitle(""+listtugas.JUDULTUGAS);
		 
			url = "http://lms.novalherdinata.net/nilaitugasguru.php?nama="+listtugas.JUDULIDT;
		
       // new LoadInbox().execute();
		loadNilaiTugas();
	}

	private void loadNilaiTugas() {

		linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
		linlaHeaderProgress.setVisibility(View.VISIBLE);

		ServiceTugas service = ApiGenerator.createService(ServiceTugas.class);
		Call<List<NilaiTugas>> call = service.getJawabTugas(listtugas.JUDULIDT);
		call.enqueue(new Callback<List<NilaiTugas>>() {
			@Override
			public void onResponse(Call<List<NilaiTugas>> call, Response<List<NilaiTugas>> response) {
				linlaHeaderProgress.setVisibility(View.GONE);

				List<NilaiTugas> nilaiTugasList = response.body();
				for(NilaiTugas nt : nilaiTugasList){
					// Storing each json item in variable
					String name = nt.getPengguna().getName();
					String email = nt.getIdsiswa();
					String kelas = nt.getKelas();
					foto = nt.getPengguna().getFoto();
					String file = nt.getFile();
					String nilai = nt.getNilai();

					//foto = foto.replaceAll(" ", "%20");

					if(first == '1'){

						//file = file.replaceAll(" ", "%20");
					}
					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();

					// adding each child node to HashMap key => value
					// adding each child node to HashMap key => value
					map.put(TAG_NAME, name);
					map.put(TAG_EMAIL, email);
					map.put(TAG_KELAS, kelas);
					map.put(TAG_FOTO, foto);
					map.put(TAG_FILE, file);
					map.put(TAG_NILAI, nilai);

					// adding HashList to ArrayList
					contactList.add(map);
				}

				populateListView();

			}

			@Override
			public void onFailure(Call<List<NilaiTugas>> call, Throwable t) {
				linlaHeaderProgress.setVisibility(View.GONE);
			}
		});

	}

	private void populateListView() {

			adapter2=new chatAdapter6(list.this, contactList);
			adapter2.notifyDataSetChanged();
			lv.setAdapter(adapter2);
			lv.invalidateViews();

			lv.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
										int position, long id) {

					String nama = ((TextView) view.findViewById(R.id.nama)).getText().toString();
					String kelas = ((TextView) view.findViewById(R.id.kelas)).getText().toString();
					String email = ((TextView) view.findViewById(R.id.email)).getText().toString();
					String foto = ((TextView) view.findViewById(R.id.txtfoto)).getText().toString();
					String file = ((TextView) view.findViewById(R.id.file)).getText().toString();
					String nilai = ((TextView) view.findViewById(R.id.nilai)).getText().toString();

					if(!nilai.equalsIgnoreCase("")){
						Toast.makeText(list.this, "Anda sudah memberi nilai", Toast.LENGTH_SHORT).show();
					}else{
						if(first == '1'){
							Intent in = new Intent(list.this, listdetailtugasnilai.class);
							in.putExtra(JUDUL, nama);
							in.putExtra(JUDUL2, kelas);
							in.putExtra(JUDUL3, email);
							in.putExtra(JUDUL4, foto);
							in.putExtra(JUDUL5, file);
							in.putExtra(JUDUL6, listtugas.JUDULIDT);
							startActivity(in);
						}

						if(first == '2'){
							Intent in = new Intent(list.this, listdetailtugasnilaionline.class);
							in.putExtra(JUDUL, nama);
							in.putExtra(JUDUL2, kelas);
							in.putExtra(JUDUL3, email);
							in.putExtra(JUDUL4, foto);
							in.putExtra(JUDUL5, file);
							in.putExtra(JUDUL6, listtugas.JUDULIDT);
							startActivity(in);
						}

					}

				}
			});

	}

	@Override
	protected void onRestart() {
	    super.onRestart();  // Always call the superclass method first
//	    adapter2.remove();
//		url = "http://lms.novalherdinata.net/nilaitugasguru.php?nama="+listtugas.JUDULIDT;
//
//    	new LoadInbox().execute();
	
	}

	class LoadInbox extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
			linlaHeaderProgress.setVisibility(View.VISIBLE);
		}

		/**
		 * getting Inbox JSON
		 * */
		protected String doInBackground(String... args) {
			// Creating JSON Parser instance
			JSONParser jParser = new JSONParser();
	
			// getting JSON string from URL
			JSONObject json = jParser.getJSONFromUrl(url);
	
			try {
				// Getting Array of Contacts
				contacts = json.getJSONArray(TAG_CONTACTS);
				
				// looping through All Contacts
				for(int i = 0; i < contacts.length(); i++){
					JSONObject c = contacts.getJSONObject(i);
					
					// Storing each json item in variable
					String name = c.getString(TAG_NAME);
					String email = c.getString(TAG_EMAIL);
					String kelas = c.getString(TAG_KELAS);
					foto = c.getString(TAG_FOTO);
					String file = c.getString(TAG_FILE);
					String nilai = c.getString(TAG_NILAI);
					
					foto = foto.replaceAll(" ", "%20");
					
					if(first == '1'){
						
					file = file.replaceAll(" ", "%20");
					}
					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();
					
					// adding each child node to HashMap key => value
					// adding each child node to HashMap key => value
					map.put(TAG_NAME, name);
					map.put(TAG_EMAIL, email);
					map.put(TAG_KELAS, kelas);
					map.put(TAG_FOTO, foto);
					map.put(TAG_FILE, file);
					map.put(TAG_NILAI, nilai);
	
					// adding HashList to ArrayList
					contactList.add(map);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			return null;
		}
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			// HIDE THE SPINNER AFTER LOADING FEEDS
		    linlaHeaderProgress.setVisibility(View.GONE);
		    
		    if(contactList.size() <1){
		    	tidakhasil.setVisibility(View.VISIBLE);
		    }
		 // LoadMore button
		    if(contactList.size() >9){
		    	
		    
		    
		
				
		    }
			
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					adapter2=new chatAdapter6(list.this, contactList); 
			        adapter2.notifyDataSetChanged();
			        lv.setAdapter(adapter2);
			        lv.invalidateViews();	
			        
			        lv.setOnItemClickListener(new OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent, View view,
								int position, long id) {
						
							String nama = ((TextView) view.findViewById(R.id.nama)).getText().toString();
							String kelas = ((TextView) view.findViewById(R.id.kelas)).getText().toString();
							String email = ((TextView) view.findViewById(R.id.email)).getText().toString();
							String foto = ((TextView) view.findViewById(R.id.txtfoto)).getText().toString();
							String file = ((TextView) view.findViewById(R.id.file)).getText().toString();
							String nilai = ((TextView) view.findViewById(R.id.nilai)).getText().toString();
							
								if(!nilai.equalsIgnoreCase("")){
									Toast.makeText(list.this, "Anda sudah memberi nilai", Toast.LENGTH_SHORT).show();
							}else{
								if(first == '1'){
									Intent in = new Intent(list.this, listdetailtugasnilai.class);
									in.putExtra(JUDUL, nama);
									in.putExtra(JUDUL2, kelas);
									in.putExtra(JUDUL3, email);
									in.putExtra(JUDUL4, foto);
									in.putExtra(JUDUL5, file);
									in.putExtra(JUDUL6, listtugas.JUDULIDT);
									startActivity(in);
								}
								
								if(first == '2'){
									Intent in = new Intent(list.this, listdetailtugasnilaionline.class);
									in.putExtra(JUDUL, nama);
									in.putExtra(JUDUL2, kelas);
									in.putExtra(JUDUL3, email);
									in.putExtra(JUDUL4, foto);
									in.putExtra(JUDUL5, file);
									in.putExtra(JUDUL6, listtugas.JUDULIDT);
									startActivity(in);
								}
								
							}
							
						}
					});
								}
							});
	}}
	
	private class loadMoreListView extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// Showing progress dialog before sending http request
			pDialog = new ProgressDialog(
					list.this);
			pDialog.setMessage("Please wait..");
			pDialog.setIndeterminate(true);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		protected Void doInBackground(Void... unused) {
			// Creating JSON Parser instance
			current_page += 1;
			
			url = "http://lms.novalherdinata.net/nilaitugasguru.php?nama="+listtugas.JUDULIDT+"&page="+current_page;
						JSONParser jParser = new JSONParser();
				
						// getting JSON string from URL
						JSONObject json = jParser.getJSONFromUrl(url);
				
						try {
							// Getting Array of Contacts
							contacts = json.getJSONArray(TAG_CONTACTS);
							
							// looping through All Contacts
							for(int i = 0; i < contacts.length(); i++){
								JSONObject c = contacts.getJSONObject(i);
								
								
								// Storing each json item in variable
								String name = c.getString(TAG_NAME);
								String email = c.getString(TAG_EMAIL);
								String kelas = c.getString(TAG_KELAS);
								foto = c.getString(TAG_FOTO);
								String file = c.getString(TAG_FILE);
								String nilai = c.getString(TAG_NILAI);
								
								foto = foto.replaceAll(" ", "%20");
								file = file.replaceAll(" ", "%20");
								
								// creating new HashMap
								HashMap<String, String> map = new HashMap<String, String>();
								
								// adding each child node to HashMap key => value
								// adding each child node to HashMap key => value
								map.put(TAG_NAME, name);
								map.put(TAG_EMAIL, email);
								map.put(TAG_KELAS, kelas);
								map.put(TAG_FOTO, foto);
								map.put(TAG_FILE, file);
								map.put(TAG_NILAI, nilai);
				
								// adding HashList to ArrayList
								contactList.add(map);
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}
						
						return null;
		}
		
		
		protected void onPostExecute(Void unused) {
			// closing progress dialog
			pDialog.dismiss();
			
			// updating UI from Background Thread
						runOnUiThread(new Runnable() {
							public void run() {
								int currentPosition = lv.getFirstVisiblePosition();
								adapter2=new chatAdapter6(list.this, contactList); 
						        adapter2.notifyDataSetChanged();
						        lv.setAdapter(adapter2);
						        lv.invalidateViews();	
						        lv.setSelectionFromTop(currentPosition + 1, 0);
						        lv.setOnItemClickListener(new OnItemClickListener() {
									@Override
									public void onItemClick(AdapterView<?> parent, View view,
											int position, long id) {
										String nama = ((TextView) view.findViewById(R.id.nama)).getText().toString();
										String kelas = ((TextView) view.findViewById(R.id.kelas)).getText().toString();
										String email = ((TextView) view.findViewById(R.id.email)).getText().toString();
										String foto = ((TextView) view.findViewById(R.id.txtfoto)).getText().toString();
										String file = ((TextView) view.findViewById(R.id.file)).getText().toString();
										String nilai = ((TextView) view.findViewById(R.id.nilai)).getText().toString();
										
											if(nilai.equalsIgnoreCase("")){
												Toast.makeText(list.this, "Anda sudah memberi nilai", Toast.LENGTH_SHORT).show();
										}else{
											Intent in = new Intent(list.this, listdetailtugasnilai.class);
											in.putExtra(JUDUL, nama);
											in.putExtra(JUDUL2, kelas);
											in.putExtra(JUDUL3, email);
											in.putExtra(JUDUL4, foto);
											in.putExtra(JUDUL5, file);
											in.putExtra(JUDUL6, listtugas.JUDULIDT);
											startActivity(in);
										}
									}
								});
											}
										});
		}
	}
	
	
	
	  @Override
		public boolean onKeyDown(int keyCode, KeyEvent event)  {
		    if (keyCode == KeyEvent.KEYCODE_BACK ) {
		        // do something on back.
		    	  Intent intent = new Intent(list.this, listtugas.class);
		    	  intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			        startActivity(intent);
		        return true;
		    }

		    return super.onKeyDown(keyCode, event);
		}

}