package com.nvlhrd.lmslearningsys;

import java.util.ArrayList;
import java.util.HashMap;


import com.nvlhrd.lmslearningsys.adapter.ImageLoader;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class chatAdapterForum extends BaseAdapter {
    
    private Activity activity;
    private ArrayList<HashMap<String, String>> data;
    private static LayoutInflater inflater=null;
    public ImageLoader imageLoader; 
    
    public chatAdapterForum(Activity a,  ArrayList<HashMap<String, String>> d) {
        activity = a;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader=new ImageLoader(activity.getApplicationContext());
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
//        	adapter = new SimpleAdapter(
//					chatutama.this, inboxList,
//					R.layout.inbox_list_item, new String[] { TAG_NAME, TAG_MESS },
//					new int[] { R.id.from, R.id.subject});
        	 vi = inflater.inflate(R.layout.list_rowforum, null);

        TextView judul = (TextView)vi.findViewById(R.id.title);
        TextView deskripsi = (TextView)vi.findViewById(R.id.deskripsi);
        TextView idforum = (TextView)vi.findViewById(R.id.idforum);
        TextView iduser = (TextView)vi.findViewById(R.id.iduser);
        TextView nama = (TextView)vi.findViewById(R.id.nama);
        TextView foto = (TextView)vi.findViewById(R.id.foto);
        TextView kelas = (TextView)vi.findViewById(R.id.kelas);
        TextView tgl = (TextView)vi.findViewById(R.id.tglupload);

        
        //hasmap
        HashMap<String, String> song = new HashMap<String, String>();
        song = data.get(position);
        
        // Setting all values in listview
        judul.setText(song.get(forum.TAG_JUDUL));
        deskripsi.setText(song.get(forum.TAG_DESKRIPSI));
        idforum.setText(song.get(forum.TAG_FORUM));
        iduser.setText(song.get(forum.TAG_IDUSER));
        nama.setText(song.get(forum.TAG_NAMA));
        foto.setText(song.get(forum.TAG_FOTO));
        kelas.setText(song.get(forum.TAG_KELAS));
        tgl.setText(song.get(forum.TAG_TGL));
        	
        return vi;
    }
}