package com.nvlhrd.lmslearningsys;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.nvlhrd.lmslearningsys.adapter.JSONParser;
import com.nvlhrd.lmslearningsys.api.ApiGenerator;
import com.nvlhrd.lmslearningsys.api.ServiceTugas;
import com.nvlhrd.lmslearningsys.api.pojo.NilaiTugas;
import com.nvlhrd.lmslearningsys.library.SessionManager;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class jawabonlinetext extends Activity{

	  SessionManager session;
		HashMap<String, String> user;
		EditText jawab;
		Button submit;
		  HttpEntity resEntity;
		  String a,b,c,d,e,f,g,h,i;
		  int randomNo;
		  ProgressDialog progressDialog;
			String txtjudul, txtdeskripsi, txtidtugas, txtidguru, txtnama, txtfoto, txtfile, txttglupload;
			static final String JUDUL ="a", JUDUL2="b", JUDUL3="c", JUDUL4="d",JUDUL5="e",JUDUL6="f",JUDUL7="g",JUDUL8="h",JUDUL9="i",JUDUL10="j",JUDUL11="k",JUDUL12="l";
			private ProgressDialog pDialog;
			String url;
			public static final String TAG_CONTACTS = "info";
			public static final String TAG_NAMA = "nama";
			JSONArray contacts = null;
			String id;
			
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
		setContentView(R.layout.jawabonlinetext);
		
		 session = new SessionManager(getApplicationContext());
	        session.checkLogin();
			 user = session.getUserDetails();
				ActionBar actionBar = getActionBar();

				// Enabling Up / Back navigation
				actionBar.setDisplayHomeAsUpEnabled(true);
				actionBar.setTitle("Tambah Tugas");
				
				Intent in = getIntent();
				 txtjudul = in.getStringExtra(JUDUL);
				 txtdeskripsi = in.getStringExtra(JUDUL2);
				 txtfile = in.getStringExtra(JUDUL3);
				 txtidguru = in.getStringExtra(JUDUL4);
				 txtnama = in.getStringExtra(JUDUL5);
				 txttglupload = in.getStringExtra(JUDUL6);
				 txtidtugas = in.getStringExtra(JUDUL7);
				 txtfoto = in.getStringExtra(JUDUL8);
				 
				 url = "http://lms.novalherdinata.net/nilaitugascek.php?nama="+txtidtugas+"&idsiswa="+user.get(SessionManager.KEY_NAME);	
				 //new LoadInbox2().execute();
		         checkTugas(txtidtugas, user.get(SessionManager.KEY_EMAIL));
				 
				 Random r = new Random();
				    randomNo = r.nextInt(100000000+1);
				    actionBar.setSubtitle("ID Jawab: 2"+randomNo);
				
				jawab = (EditText) findViewById(R.id.komenonline);
				submit = (Button) findViewById(R.id.btnkomenonline);
				
				submit.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						
						if(jawab.getText().toString().equalsIgnoreCase("")){
							jawab.setError("Tidak boleh kosong");
						}
						
						if(!jawab.getText().toString().equalsIgnoreCase("")){
						progressDialog = ProgressDialog.show(jawabonlinetext.this, "", "Proses.....", false);
		                Thread thread=new Thread(new Runnable(){
		                       public void run(){
		                 doinput();
		                           runOnUiThread(new Runnable(){
		                               public void run() {
		                                   if(progressDialog.isShowing())
		                                       progressDialog.dismiss();
//		                                   Intent intent2 = new Intent(up.this, MainActivity.class);
//		                       			startActivity(intent2);
		                               }
		                           });
		                       }
		               });
		               thread.start();
		               Toast.makeText(jawabonlinetext.this, "Berhasil di tambahkan", Toast.LENGTH_SHORT).show();
						   Intent intent = new Intent(jawabonlinetext.this, listtugas.class);
					        startActivity(intent);	 
					}
					}
				});
	}

	private void checkTugas(String txtidtugas, String s) {

		pDialog = new ProgressDialog(
				jawabonlinetext.this);
		pDialog.setMessage("Proses..");
		pDialog.setIndeterminate(true);
		pDialog.setCancelable(false);
		pDialog.show();

		ServiceTugas service = ApiGenerator.createService(ServiceTugas.class);
		Call<NilaiTugas> call = service.checkTugas(txtidtugas, s);
		call.enqueue(new Callback<NilaiTugas>() {
			@Override
			public void onResponse(Call<NilaiTugas> call, Response<NilaiTugas> response) {
				pDialog.dismiss();
				if(response.isSuccessful()){
					if(response.body() != null){
						Toast.makeText(jawabonlinetext.this, "Anda sudah mengumpulkan", Toast.LENGTH_LONG).show();
						Intent intent = new Intent(jawabonlinetext.this, listtugas.class);
						startActivity(intent);
					}
				}
			}

			@Override
			public void onFailure(Call<NilaiTugas> call, Throwable t) {
				pDialog.dismiss();
			}
		});

	}

	class LoadInbox2 extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(
					jawabonlinetext.this);
			pDialog.setMessage("Proses..");
			pDialog.setIndeterminate(true);
			pDialog.setCancelable(false);
			pDialog.show();
		
		}

		/**
		 * getting Inbox JSON
		 * */
		protected String doInBackground(String... args) {
			// Creating JSON Parser instance
			JSONParser jParser = new JSONParser();
	
			// getting JSON string from URL
			JSONObject json = jParser.getJSONFromUrl(url);
	
			try {
				// Getting Array of Contacts
				contacts = json.getJSONArray(TAG_CONTACTS);
				
				// looping through All Contacts
				for(int i = 0; i < contacts.length(); i++){
					JSONObject c = contacts.getJSONObject(i);
					
//					// Storing each json item in variable
					id = c.getString(TAG_NAMA);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			
			return null;
		}
		protected void onPostExecute(String file_url) {
		pDialog.dismiss();
		if(!(id == null)){
			  Toast.makeText(jawabonlinetext.this, "Anda sudah mengumpulkan", Toast.LENGTH_LONG).show();
			   Intent intent = new Intent(jawabonlinetext.this, listtugas.class);
		        startActivity(intent);	
		}
	
	}}
	
	 private void doinput() {
			// TODO Auto-generated method stub

		  String idtugas = txtidtugas.toString();
			String idguru = txtidguru.toString();
			String siswa =  user.get(SessionManager.KEY_EMAIL);
			String kelas = dashboard.JUDULKElas;
//			 String input_data= "http://lms.novalherdinata.net/inputtugasonline.php"; //URL website anda dengan file insert.php yang telah dibuat
	         String input_data = Config.URL_API + "tugas/jawab";
	         HttpClient httpClient = new DefaultHttpClient();  
	         HttpPost httpPost = new HttpPost(input_data);  
	         ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();  
	         param.add(new BasicNameValuePair("idtugas", idtugas)); 
	         param.add(new BasicNameValuePair("idguru", idguru)); 
	        param.add(new BasicNameValuePair("idsiswa", siswa));
	        param.add(new BasicNameValuePair("kelas", kelas)); 
	        param.add(new BasicNameValuePair("file", jawab.getText().toString())); 
	         try {  
	              httpPost.setEntity(new UrlEncodedFormEntity(param));  
	              HttpResponse httpRespose = httpClient.execute(httpPost);  
	             HttpEntity httpEntity = httpRespose.getEntity();  
	             InputStream in = httpEntity.getContent();  
	             BufferedReader read = new BufferedReader(new InputStreamReader(in));  
	            
	             String isi= "";  
	             String baris= "";  
	            
	             while((baris = read.readLine())!=null){  
	                isi+= baris;  
	             }  
	               
	             //Jika isi tidak sama dengan "null " maka akan tampil Toast "Berhasil" sebaliknya akan tampil "Gagal"  
	             if(!isi.equals("null")){                    
	             }else{  
	             }  
	            
	       } catch (ClientProtocolException e) {  
	          // TODO Auto-generated catch block  
	          e.printStackTrace();  
	       } catch (IOException e) {  
	          // TODO Auto-generated catch block  
	          e.printStackTrace();  
	       }  
	    	
		}
}
