package com.nvlhrd.lmslearningsys;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;


import com.nvlhrd.lmslearningsys.library.SessionManager;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class inputberita extends Activity {

    EditText txtjudul, txtdeskripsi;
    Button ok;
    ProgressDialog progressDialog;
    int randomNo;
    SessionManager session;
    HashMap<String, String> user;
    String[] kelas = {
            "X ANKIM 1"
            , "X ANKIM 2"
            , "XI ANKIM 1"
            , "XI ANKIM 2"
            , "XII ANKIM 1"
            , "XII ANKIM 2"
            , "X TKJ 1"
            , "X TKJ 2"
            , "X TKJ 3"
            , "XI TKJ 1"
            , "XI TKJ 2"
            , "XII TKJ 1"
            , "XII TKJ 2"
            , "XII TKJ 3"
            , "X RPL 1"
            , "X RPL 2"
            , "XI RPL 1"
            , "XI RPL 2"
            , "XII RPL 1"
            , "XII RPL 2"
            , "X MM 1"
            , "X MM 2"
            , "XI MM 1"
            , "XI MM 2"
            , "XII MM 1"
            , "XII MM 2"
            , "X TP3R 1"
            , "XI TP3R 1"
            , "XII TP3R 1"
            , "X UPW 1"
            , "XI UPW 1"
            , "XI UPW 2"
            , "XII UPW 1"
            , "XII UPW 2"
            , "X AK 1"
            , "X AK 2"
            , "X AK 3"
            , "X AK 4"
            , "X AK 5"
            , "XI AK 1"
            , "XI AK 2"
            , "XI AK 3"
            , "XI AK 4"
            , "XI AK 5"
            , "XII AK 1"
            , "XII AK 2"
            , "XII AK 3"
            , "XII AK 4"
            , "XII AK 5"
            , "XII AK 6"
            , "X AP 1"
            , "X AP 2"
            , "X AP 3"
            , "X AP 4"
            , "X AP 5"
            , "X AP 6"
            , "X AP 7"
            , "XI AP 1"
            , "XI AP 2"
            , "XI AP 3"
            , "XI AP 4"
            , "XI AP 5"
            , "XI AP 6"
            , "XII AP 1"
            , "XII AP 2"
            , "XII AP 3"
            , "XII AP 4"
            , "XII AP 5"
            , "XII AP 6"
            , "X PEMS 1"
            , "X PEMS 2"
            , "XI PEMS 1"
            , "XII PEMS 1"
            , "XII PEMS 2"};
    Spinner spinner;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.tambahberita);
        session = new SessionManager(getApplicationContext());
        session.checkLogin();
        user = session.getUserDetails();

        txtjudul = (EditText) findViewById(R.id.beritajudul);
        txtdeskripsi = (EditText) findViewById(R.id.beritades);
        spinner = (Spinner) findViewById(R.id.spinnerkelas);
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, kelas);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);

        ok = (Button) findViewById(R.id.btnberita);

        Random r = new Random();
        randomNo = r.nextInt(100000000 + 4);

        ok.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (txtdeskripsi.getText().toString().equalsIgnoreCase("")) {
                    txtdeskripsi.setError("Harus diisi");
                }
                if (txtjudul.getText().toString().equalsIgnoreCase("")) {
                    txtjudul.setError("Harus diisi");
                }
                if (!txtdeskripsi.getText().toString().equalsIgnoreCase("") && !txtjudul.getText().toString().equalsIgnoreCase("")) {
                    progressDialog = ProgressDialog.show(inputberita.this, "", "proses.....", false);
                    Thread thread = new Thread(new Runnable() {
                        public void run() {
                            doinput();
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    if (progressDialog.isShowing())
                                        progressDialog.dismiss();
                                    txtjudul.setText("");
                                    txtdeskripsi.setText("");
//                                   Intent intent2 = new Intent(up.this, MainActivity.class);
//                       			startActivity(intent2);
                                }
                            });
                        }
                    });
                    thread.start();
                    Toast.makeText(inputberita.this, "Berhasil di tambahkan", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(inputberita.this, listberita.class);
                    startActivity(intent);
                }
            }
        });

    }

    private void doinput() {
        // TODO Auto-generated method stub

        String a = "" + randomNo;
        String b = user.get(SessionManager.KEY_EMAIL);
        String c = txtjudul.getText().toString();
        String d = txtdeskripsi.getText().toString();

        //String input_data = "http://lms.novalherdinata.net/inputberita.php"; //URL website anda dengan file insert.php yang telah dibuat
        String input_data = Config.URL_API + "berita";

        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(input_data);
        ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();
        param.add(new BasicNameValuePair("idberita", a));
        param.add(new BasicNameValuePair("kelas", kelas[spinner.getSelectedItemPosition()]));
        param.add(new BasicNameValuePair("idguru", b));
        param.add(new BasicNameValuePair("judul", c));
        param.add(new BasicNameValuePair("deskripsi", d));
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(param));
            HttpResponse httpRespose = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpRespose.getEntity();
            InputStream in = httpEntity.getContent();
            BufferedReader read = new BufferedReader(new InputStreamReader(in));

            String isi = "";
            String baris = "";

            while ((baris = read.readLine()) != null) {
                isi += baris;
            }

            //Jika isi tidak sama dengan "null " maka akan tampil Toast "Berhasil" sebaliknya akan tampil "Gagal"
            if (!isi.equals("null")) {
            } else {
            }

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // do something on back.
            Intent intent = new Intent(inputberita.this, dashboard.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

}
