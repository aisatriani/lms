package com.nvlhrd.lmslearningsys;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import com.nvlhrd.lmslearningsys.adapter.JSONParser;
import com.nvlhrd.lmslearningsys.api.ApiGenerator;
import com.nvlhrd.lmslearningsys.api.ServiceTugas;
import com.nvlhrd.lmslearningsys.api.pojo.Message;
import com.nvlhrd.lmslearningsys.api.pojo.Tugas;
import com.nvlhrd.lmslearningsys.library.DialogInfo;
import com.nvlhrd.lmslearningsys.library.SessionManager;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class listtugas extends Activity {

	ArrayList<HashMap<String, String>> contactList;
	// url to make request
	static String url;
	static String NAMA;
	static final String JUDUL = "a", JUDUL2 = "b", JUDUL3 = "c", JUDUL4 = "d",
			JUDUL5 = "e", JUDUL6 = "f", JUDUL7 = "g", JUDUL8 = "h",
			JUDUL9 = "i", JUDUL10 = "j", JUDUL11 = "k", JUDUL12 = "l";

	// JSON Node names
	public static final String TAG_CONTACTS = "info";
	public static final String TAG_NAME = "judul";
	public static final String TAG_DESKRIPSI = "deskripsi";
	public static final String TAG_FILE = "file";
	public static final String TAG_TGLUPLOAD = "tgl";
	public static final String TAG_IDGURU = "idguru";
	public static final String TAG_NAMA = "nama";
	public static final String TAG_IDTUGAS = "idtugas";
	public static final String TAG_FOTO = "foto";
	
	String link;
	int current_page = 1;
	// contacts JSONArray
	JSONArray contacts = null;
	chatAdapterTugas adapter2;
	ListView lv;
	TextView name2;
	LinearLayout linlaHeaderProgress;
	LinearLayout tidakhasil;
	String file;

	// Progress Dialog
	private ProgressDialog pDialog;

	// Progress dialog type (0 - for Horizontal progress bar)
	public static final int progress_bar_type = 0;

	// File url to download
	private static String file_url = "http://api.androidhive.info/progressdialog/hive.jpg";
	File folder, folderpict;
	String txtjudul, txtfile;
	String email, nama, kelas, status;
	SessionManager session;
	HashMap<String, String> user;
	String jkelas;
	 static String JUDULTUGAS, JUDULIDT;
	 
	 String judul;
		String deskripsi;
		String tfile;
		String idguru;
		String tnama;
		String tgl ;
		String idtugas ;
		String foto;
		
		public static final String TAG_NAMEcek = "nama";
		String idce;
		static String detailid;
		String url2;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list);
		 session = new SessionManager(getApplicationContext());
	        session.checkLogin();
			 user = session.getUserDetails();

		lv = (ListView) findViewById(R.id.list);
		ActionBar actionBar = getActionBar();
		
		//intent
		Intent in = getIntent();
        nama = in.getStringExtra(JUDUL);
        kelas = in.getStringExtra(JUDUL2);
        status = in.getStringExtra(JUDUL3);

		// Enabling Up / Back navigation
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setTitle("Tugas");
		contactList = new ArrayList<HashMap<String, String>>();
		// Loading INBOX in Background Thread
		tidakhasil = (LinearLayout) findViewById(R.id.noitem);
		tidakhasil.setVisibility(View.GONE);
		
		
		if(dashboard.JUDULSTATUS.equalsIgnoreCase("s")){
			//jkelas = dashboard.JUDULKElas.replaceAll(" ", "%20");
			jkelas = dashboard.JUDULKElas;
			url = "http://lms.novalherdinata.net/tugasparsingmurid.php?nama="+jkelas;
			loadTugasSiswa();
		}
		
		if(dashboard.JUDULSTATUS.equalsIgnoreCase("g")){
			url = "http://lms.novalherdinata.net/tugasparsing.php?nama="+user.get(SessionManager.KEY_NAME);
			loadTugasGuru();
		}

		//new LoadInbox().execute();
	}

	private void loadTugasSiswa() {

		linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
		linlaHeaderProgress.setVisibility(View.VISIBLE);

		ServiceTugas service = ApiGenerator.createService(ServiceTugas.class);
		Call<List<Tugas>> call = service.getTugasByKelas(jkelas);
		call.enqueue(new Callback<List<Tugas>>() {
			@Override
			public void onResponse(Call<List<Tugas>> call, Response<List<Tugas>> response) {
				linlaHeaderProgress.setVisibility(View.GONE);
				if(response.isSuccessful()){

					for(Tugas tugas : response.body()){
						// Storing each json item in variable
						String judul = tugas.getJudul();
						String deskripsi = tugas.getDeskripsi();
						file = tugas.getFile();
						String tglupload = tugas.getTimestop();
						String idguru = tugas.getIdguru();
						String idtugas = tugas.getIdtugas();
						String foto = tugas.getPengguna().getFoto();
						String nama = tugas.getPengguna().getName();

						//file = file.replaceAll(" ", "%20");
						//foto = foto.replaceAll(" ", "%20");

						// creating new HashMap
						HashMap<String, String> map = new HashMap<String, String>();

						// adding each child node to HashMap key => value
						// adding each child node to HashMap key => value
						map.put(TAG_NAME, judul);
						map.put(TAG_DESKRIPSI, deskripsi);
						map.put(TAG_FILE, file);
						map.put(TAG_TGLUPLOAD, tglupload);
						map.put(TAG_IDGURU, idguru);
						map.put(TAG_IDTUGAS, idtugas);
						map.put(TAG_FOTO, foto);
						map.put(TAG_NAMA, nama);


						// adding HashList to ArrayList
						contactList.add(map);

					}

					populateTolist();

				}
			}



			@Override
			public void onFailure(Call<List<Tugas>> call, Throwable throwable) {
				linlaHeaderProgress.setVisibility(View.GONE);
				DialogInfo.showErrorDialog(listtugas.this, getString(R.string.error_connection));
			}
		});
	}

	private void populateTolist() {
		adapter2 = new chatAdapterTugas(listtugas.this, contactList);
		adapter2.notifyDataSetChanged();
		lv.setAdapter(adapter2);
		lv.invalidateViews();

		lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent,
									View view, int position, long id) {

				judul = ((TextView) view.findViewById(R.id.title)).getText().toString();
				deskripsi = ((TextView) view.findViewById(R.id.deskripsi)).getText().toString();
				tfile = ((TextView) view.findViewById(R.id.downloadfile)).getText().toString();
				idguru = ((TextView) view.findViewById(R.id.idguru)).getText().toString();
				tnama = ((TextView) view.findViewById(R.id.namagurutugas)).getText().toString();
				tgl = ((TextView) view.findViewById(R.id.tglupload)).getText().toString();
				idtugas = ((TextView) view.findViewById(R.id.idtugas)).getText().toString();
				foto = ((TextView) view.findViewById(R.id.foto)).getText().toString();

				String authtgl = tgl.replaceAll("-", "");

				int numtgl = 0;
				try {
					numtgl = Integer.parseInt(authtgl);
				} catch(NumberFormatException nfe) {
					// Handle parse error.
				}
				Calendar c = Calendar.getInstance();
				System.out.println("Current time => "+c.getTime());

				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				String formattedDate = df.format(c.getTime());

				String authtgl3 = formattedDate.replaceAll("-", "");
				int androtgl = 0;
				try {
					androtgl = Integer.parseInt(authtgl3);
				} catch(NumberFormatException nfe) {
					// Handle parse error.
				}
				JUDULTUGAS = judul;
				JUDULIDT = idtugas;
				if(dashboard.JUDULSTATUS.equalsIgnoreCase("g")){

					AlertDialog.Builder alertDialog = new AlertDialog.Builder(listtugas.this);

					// Setting Dialog Title
					alertDialog.setTitle("Pilih");

					// Setting Dialog Message
					alertDialog.setMessage("Apa yang akan anda lakukan?");

					// Setting Positive "Yes" Button
					alertDialog.setPositiveButton("Beri Nilai Siswa", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int which) {

							// Write your code here to invoke YES event
							Intent in = new Intent(listtugas.this, list.class);
							in.putExtra(JUDUL, judul);
							in.putExtra(JUDUL2, deskripsi);
							in.putExtra(JUDUL3, tfile);
							in.putExtra(JUDUL4, idguru);
							in.putExtra(JUDUL5, tnama);
							in.putExtra(JUDUL6, tgl);
							in.putExtra(JUDUL7, idtugas);
							in.putExtra(JUDUL8, foto);
							startActivity(in);
						}
					});

					// Setting Negative "NO" Button
					alertDialog.setNegativeButton("Hapus", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							// Write your code here to invoke NO event
							ServiceTugas service = ApiGenerator.createService(ServiceTugas.class);
							Call<Message> call = service.deleteTugasById(idtugas);
							call.enqueue(new Callback<Message>() {
								@Override
								public void onResponse(Call<Message> call, Response<Message> response) {
									adapter2.remove();
									if(dashboard.JUDULSTATUS.equalsIgnoreCase("s")){
										//jkelas = dashboard.JUDULKElas.replaceAll(" ", "%20");
										jkelas = dashboard.JUDULKElas;
										//url = "http://lms.novalherdinata.net/tugasparsingmurid.php?nama="+jkelas;
										loadTugasSiswa();
									}

									if(dashboard.JUDULSTATUS.equalsIgnoreCase("g")){
										//url = "http://lms.novalherdinata.net/tugasparsing.php?nama="+user.get(SessionManager.KEY_NAME);
										loadTugasGuru();
									}
								}

								@Override
								public void onFailure(Call<Message> call, Throwable throwable) {

								}
							});



							//new LoadInbox().execute();
						}
					});

					// Showing Alert Message
					alertDialog.show();


				}

				if(dashboard.JUDULSTATUS.equalsIgnoreCase("s")){
					char first = idtugas.charAt(0);

					if(first == '1'){
						if(numtgl >= androtgl){
							Intent in = new Intent(listtugas.this, listdetailtugas.class);

							in.putExtra(JUDUL, judul);
							in.putExtra(JUDUL2, deskripsi);
							in.putExtra(JUDUL3, tfile);
							in.putExtra(JUDUL4, idguru);
							in.putExtra(JUDUL5, tnama);
							in.putExtra(JUDUL6, tgl);
							in.putExtra(JUDUL7, idtugas);
							in.putExtra(JUDUL8, foto);
							startActivity(in);
						}
						if(numtgl < androtgl){
							Toast.makeText(listtugas.this, "Anda terlambat mengumpulkan", Toast.LENGTH_SHORT).show();
						}
					}
					if(first == '2'){
						if(numtgl >= androtgl){
							Intent in = new Intent(listtugas.this, jawabonlinetext.class);
							in.putExtra(JUDUL, judul);
							in.putExtra(JUDUL2, deskripsi);
							in.putExtra(JUDUL3, tfile);
							in.putExtra(JUDUL4, idguru);
							in.putExtra(JUDUL5, tnama);
							in.putExtra(JUDUL6, tgl);
							in.putExtra(JUDUL7, idtugas);
							in.putExtra(JUDUL8, foto);
							startActivity(in);
						}
						if(numtgl < androtgl){
							Toast.makeText(listtugas.this, "Anda terlambat mengumpulkan", Toast.LENGTH_SHORT).show();
						}
					}
				}


			}


		});
	}


	private void loadTugasGuru() {

		linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
		linlaHeaderProgress.setVisibility(View.VISIBLE);

		ServiceTugas service = ApiGenerator.createService(ServiceTugas.class);
		Call<List<Tugas>> call = service.getTugasByName(user.get(SessionManager.KEY_EMAIL));
		call.enqueue(new Callback<List<Tugas>>() {
			@Override
			public void onResponse(Call<List<Tugas>> call, Response<List<Tugas>> response) {
				linlaHeaderProgress.setVisibility(View.GONE);
				if(response.isSuccessful()){

					for(Tugas tugas : response.body()){
						// Storing each json item in variable
						String judul = tugas.getJudul();
						String deskripsi = tugas.getDeskripsi();
						file = tugas.getFile();
						String tglupload = tugas.getTimestop();
						String idguru = tugas.getIdguru();
						String idtugas = tugas.getIdtugas();
						String foto = tugas.getPengguna().getFoto();
						String nama = tugas.getPengguna().getName();

						//file = file.replaceAll(" ", "%20");
						//foto = foto.replaceAll(" ", "%20");

						// creating new HashMap
						HashMap<String, String> map = new HashMap<String, String>();

						// adding each child node to HashMap key => value
						// adding each child node to HashMap key => value
						map.put(TAG_NAME, judul);
						map.put(TAG_DESKRIPSI, deskripsi);
						map.put(TAG_FILE, file);
						map.put(TAG_TGLUPLOAD, tglupload);
						map.put(TAG_IDGURU, idguru);
						map.put(TAG_IDTUGAS, idtugas);
						map.put(TAG_FOTO, foto);
						map.put(TAG_NAMA, nama);


						// adding HashList to ArrayList
						contactList.add(map);

					}

					populateTolist();

				}
			}

			@Override
			public void onFailure(Call<List<Tugas>> call, Throwable throwable) {
				linlaHeaderProgress.setVisibility(View.GONE);
				DialogInfo.showErrorDialog(listtugas.this, getString(R.string.error_connection));
			}
		});

	}


	class LoadInbox extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
			linlaHeaderProgress.setVisibility(View.VISIBLE);
		}

		/**
		 * getting Inbox JSON
		 * */
		protected String doInBackground(String... args) {
			// Creating JSON Parser instance
			JSONParser jParser = new JSONParser();

			// getting JSON string from URL
			JSONObject json = jParser.getJSONFromUrl(url);

			try {
				// Getting Array of Contacts
				contacts = json.getJSONArray(TAG_CONTACTS);

				// looping through All Contacts
				for (int i = 0; i < contacts.length(); i++) {
					JSONObject c = contacts.getJSONObject(i);

					// Storing each json item in variable
					String judul = c.getString(TAG_NAME);
					String deskripsi = c.getString(TAG_DESKRIPSI);
					file = c.getString(TAG_FILE);
					String tglupload = c.getString(TAG_TGLUPLOAD);
					String idguru = c.getString(TAG_IDGURU);
					String idtugas = c.getString(TAG_IDTUGAS);
					String foto = c.getString(TAG_FOTO);
					String nama = c.getString(TAG_NAMA);

					file = file.replaceAll(" ", "%20");
					foto = foto.replaceAll(" ", "%20");

					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();

					// adding each child node to HashMap key => value
					// adding each child node to HashMap key => value
					map.put(TAG_NAME, judul);
					map.put(TAG_DESKRIPSI, deskripsi);
					map.put(TAG_FILE, file);
					map.put(TAG_TGLUPLOAD, tglupload);
					map.put(TAG_IDGURU, idguru);
					map.put(TAG_IDTUGAS, idtugas);
					map.put(TAG_FOTO, foto);
					map.put(TAG_NAMA, nama);
					

					// adding HashList to ArrayList
					contactList.add(map);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			// HIDE THE SPINNER AFTER LOADING FEEDS
			linlaHeaderProgress.setVisibility(View.GONE);

			if (contactList.size() < 1) {
				tidakhasil.setVisibility(View.VISIBLE);
			}
			// LoadMore button
			if (contactList.size() > 9) {

				Button btnLoadMore = new Button(listtugas.this);
			
			}

			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					adapter2 = new chatAdapterTugas(listtugas.this, contactList);
					adapter2.notifyDataSetChanged();
					lv.setAdapter(adapter2);
					lv.invalidateViews();

					lv.setOnItemClickListener(new OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							
						judul = ((TextView) view.findViewById(R.id.title)).getText().toString();
						deskripsi = ((TextView) view.findViewById(R.id.deskripsi)).getText().toString();
							tfile = ((TextView) view.findViewById(R.id.downloadfile)).getText().toString();
							idguru = ((TextView) view.findViewById(R.id.idguru)).getText().toString();
							tnama = ((TextView) view.findViewById(R.id.namagurutugas)).getText().toString();
						tgl = ((TextView) view.findViewById(R.id.tglupload)).getText().toString();
							 idtugas = ((TextView) view.findViewById(R.id.idtugas)).getText().toString();
							foto = ((TextView) view.findViewById(R.id.foto)).getText().toString();
							
							String authtgl = tgl.replaceAll("-", "");
							
							int numtgl = 0;
							try {
								numtgl = Integer.parseInt(authtgl);
							} catch(NumberFormatException nfe) {
							  // Handle parse error.
							}
							 Calendar c = Calendar.getInstance();
						        System.out.println("Current time => "+c.getTime());

						        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
						        String formattedDate = df.format(c.getTime());
							
						        String authtgl3 = formattedDate.replaceAll("-", "");
							int androtgl = 0;
							try {
								androtgl = Integer.parseInt(authtgl3);
							} catch(NumberFormatException nfe) {
							  // Handle parse error.
							}
							JUDULTUGAS = judul;
							JUDULIDT = idtugas;
							if(dashboard.JUDULSTATUS.equalsIgnoreCase("g")){
								
								AlertDialog.Builder alertDialog = new AlertDialog.Builder(listtugas.this);
								 
						        // Setting Dialog Title
						        alertDialog.setTitle("Pilih");
						 
						        // Setting Dialog Message
						        alertDialog.setMessage("Apa yang akan anda lakukan?");
						 
						        // Setting Positive "Yes" Button
						        alertDialog.setPositiveButton("Beri Nilai Siswa", new DialogInterface.OnClickListener() {
						            public void onClick(DialogInterface dialog,int which) {
						 
						            // Write your code here to invoke YES event
						            	Intent in = new Intent(listtugas.this, list.class);
										in.putExtra(JUDUL, judul);
										in.putExtra(JUDUL2, deskripsi);
										in.putExtra(JUDUL3, tfile);
										in.putExtra(JUDUL4, idguru);
										in.putExtra(JUDUL5, tnama);
										in.putExtra(JUDUL6, tgl);
										in.putExtra(JUDUL7, idtugas);
										in.putExtra(JUDUL8, foto);
										startActivity(in);
						            }
						        });
						 
						        // Setting Negative "NO" Button
						        alertDialog.setNegativeButton("Hapus", new DialogInterface.OnClickListener() {
						            public void onClick(DialogInterface dialog, int which) {
						            // Write your code here to invoke NO event
										ServiceTugas service = ApiGenerator.createService(ServiceTugas.class);
										Call<Message> call = service.deleteTugasById(idtugas);
										call.enqueue(new Callback<Message>() {
											@Override
											public void onResponse(Call<Message> call, Response<Message> response) {

											}

											@Override
											public void onFailure(Call<Message> call, Throwable throwable) {

											}
										});

										adapter2.remove();
						            	if(dashboard.JUDULSTATUS.equalsIgnoreCase("s")){
						        			//jkelas = dashboard.JUDULKElas.replaceAll(" ", "%20");
						        			jkelas = dashboard.JUDULKElas;
						        			//url = "http://lms.novalherdinata.net/tugasparsingmurid.php?nama="+jkelas;
											loadTugasSiswa();
						        		}
						        		
						        		if(dashboard.JUDULSTATUS.equalsIgnoreCase("g")){
						        			//url = "http://lms.novalherdinata.net/tugasparsing.php?nama="+user.get(SessionManager.KEY_NAME);
											loadTugasGuru();
						        		}

						        		//new LoadInbox().execute();
						            }
						        });
						 
						        // Showing Alert Message
						        alertDialog.show();
								
								
							}
							
							if(dashboard.JUDULSTATUS.equalsIgnoreCase("s")){
								char first = idtugas.charAt(0);
								
								if(first == '1'){
									if(numtgl >= androtgl){
									Intent in = new Intent(listtugas.this, listdetailtugas.class);
									
									in.putExtra(JUDUL, judul);
									in.putExtra(JUDUL2, deskripsi);
									in.putExtra(JUDUL3, tfile);
									in.putExtra(JUDUL4, idguru);
									in.putExtra(JUDUL5, tnama);
									in.putExtra(JUDUL6, tgl);
									in.putExtra(JUDUL7, idtugas);
									in.putExtra(JUDUL8, foto);
									startActivity(in);
									}
									if(numtgl <= androtgl){	 
										Toast.makeText(listtugas.this, "Anda terlambat mengumpulkan", Toast.LENGTH_SHORT).show();
									}
								}
								if(first == '2'){
									if(numtgl >= androtgl){
									Intent in = new Intent(listtugas.this, jawabonlinetext.class);
									in.putExtra(JUDUL, judul);
									in.putExtra(JUDUL2, deskripsi);
									in.putExtra(JUDUL3, tfile);
									in.putExtra(JUDUL4, idguru);
									in.putExtra(JUDUL5, tnama);
									in.putExtra(JUDUL6, tgl);
									in.putExtra(JUDUL7, idtugas);
									in.putExtra(JUDUL8, foto);
									startActivity(in);
									}
									if(numtgl <= androtgl){
										  Toast.makeText(listtugas.this, "Anda terlambat mengumpulkan", Toast.LENGTH_SHORT).show();
									}
								}
								}	
								
							
							}
							
						
					});
				}
			});
		}
	}




	/**
	 * Showing Dialog
	 * */
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case progress_bar_type:
			pDialog = new ProgressDialog(this);
			pDialog.setMessage("Downloading file. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setMax(100);
			pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			pDialog.setCancelable(true);
			pDialog.show();
			return pDialog;
		default:
			return null;
		}
	}


	
	 public void showPdf()
     {
		 
		 String extStorageDirectory = Environment
					.getExternalStorageDirectory().toString();
         File file = new File( extStorageDirectory
					+ "/MobileLearning/Course/" + txtjudul+".pdf" );
         PackageManager packageManager = getPackageManager();
         Intent testIntent = new Intent(Intent.ACTION_VIEW);
         testIntent.setType("application/pdf");
         List list = packageManager.queryIntentActivities(testIntent, PackageManager.MATCH_DEFAULT_ONLY);
         Intent intent = new Intent();
         intent.setAction(Intent.ACTION_VIEW);
         Uri uri = Uri.fromFile(file);
         intent.setDataAndType(uri, "application/pdf");
         startActivity(intent);
     }
	 

		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.main, menu);
			
			MenuItem tambah = (MenuItem) menu.findItem(R.id.action_tambah);
			
			if(dashboard.JUDULSTATUS.equalsIgnoreCase("g")){
				tambah.setVisible(true);
			}
			if(dashboard.JUDULSTATUS.equalsIgnoreCase("s")){
				tambah.setVisible(false);
			}
			
			return super.onCreateOptionsMenu(menu);
			
		}
		
		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			// toggle nav drawer on selecting action bar app icon/title
		
			// Handle action bar actions click
			switch (item.getItemId()) {
			case R.id.action_tambah:
				
				AlertDialog.Builder alertDialog = new AlertDialog.Builder(listtugas.this);
				 
		        // Setting Dialog Title
		        alertDialog.setTitle("Pilih");
		 
		        // Setting Dialog Message
		        alertDialog.setMessage("Pilih model tugas?");
		 
		        // Setting Icon to Dialog
		 
		        // Setting Positive "Yes" Button
		        alertDialog.setPositiveButton("FILE", new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface dialog,int which) {
		 
		            // Write your code here to invoke YES event
		            	Intent intent = new Intent(listtugas.this, uploadtugas.class);
						startActivity(intent);
		            }
		        });
		 
		        // Setting Negative "NO" Button
		        alertDialog.setNegativeButton("TEXT", new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface dialog, int which) {
		            // Write your code here to invoke NO event
		            	Intent intent = new Intent(listtugas.this, OnlineText.class);
						startActivity(intent);
		            }
		        });
		 
		        // Showing Alert Message
		        alertDialog.show();
				
				
				break;
			default:
				return super.onOptionsItemSelected(item);
			}
			return true;
		}
		
		  @Override
			public boolean onKeyDown(int keyCode, KeyEvent event)  {
			    if (keyCode == KeyEvent.KEYCODE_BACK ) {
			        // do something on back.
			    	  Intent intent = new Intent(listtugas.this, dashboard.class);
			    	  intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				        startActivity(intent);
			        return true;
			    }

			    return super.onKeyDown(keyCode, event);
			}

}