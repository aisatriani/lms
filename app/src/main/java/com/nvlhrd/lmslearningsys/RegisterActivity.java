/**
 * Author: Ravi Tamada
 * URL: www.androidhive.info
 * twitter: http://twitter.com/ravitamada
 * */
package com.nvlhrd.lmslearningsys;




import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;


import com.nvlhrd.lmslearningsys.library.DatabaseHandler;
import com.nvlhrd.lmslearningsys.library.UserFunctions;



import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RegisterActivity extends Activity {
	Button btnRegister;
	Button btnLinkToLogin;
	EditText inputFullName;
	EditText inputEmail;
	EditText inputPassword;
	EditText inputPassword2;
	TextView registerErrorMsg;
	
	// JSON Response node names
	 ProgressDialog progressDialog;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		 StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	        StrictMode.setThreadPolicy(policy);
		setContentView(R.layout.activity_registerguru);

		// Importing all assets like buttons, text fields
		inputFullName = (EditText) findViewById(R.id.registerName);
		inputEmail = (EditText) findViewById(R.id.registerEmail);
		inputPassword = (EditText) findViewById(R.id.registerPassword);
		inputPassword2 = (EditText) findViewById(R.id.registerPasswordconf);
		btnRegister = (Button) findViewById(R.id.btnRegister);
		btnLinkToLogin = (Button) findViewById(R.id.btnLinkToLoginScreen);
		registerErrorMsg = (TextView) findViewById(R.id.register_error);
		
		// Register Button Click event
		btnRegister.setOnClickListener(new View.OnClickListener() {			
			public void onClick(View view) {
				
				
				String emailvalidate = inputEmail.getText().toString().trim();

				String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

				if(inputFullName.getText().toString().equalsIgnoreCase("")){
					registerErrorMsg.setText("Nama tidak boleh kosong!");
		}else if(inputEmail.getText().toString().equalsIgnoreCase("")){
			registerErrorMsg.setText("Email tidak boleh kosong!");
		}else if(inputPassword.getText().toString().equalsIgnoreCase("")){
			registerErrorMsg.setText("Password tidak boleh kosong!");
		}else if(!emailvalidate.matches(emailPattern)){
			registerErrorMsg.setText("Format email salah");
		}else if(inputPassword.getText().toString().length() <6){
			registerErrorMsg.setText("Password harus lebih dari 6 karakter");
		}
		else if(!(inputPassword.getText().toString().equals(inputPassword2.getText().toString()))){
			registerErrorMsg.setText("Password harus tidak sama");
		}
				
				if(inputPassword.getText().toString().equals(inputPassword2.getText().toString()) && !inputFullName.getText().toString().equalsIgnoreCase("") &&!inputEmail.getText().toString().equalsIgnoreCase("")&&!inputPassword.getText().toString().equalsIgnoreCase("")&& !(inputPassword.getText().toString().length() <6) && emailvalidate.matches(emailPattern)){
				
					progressDialog = ProgressDialog.show(RegisterActivity.this, "", "Proses.....", false);
                    Thread thread=new Thread(new Runnable(){
                           public void run(){
                        		doinput();
                               runOnUiThread(new Runnable(){
                                   public void run() {
                                       if(progressDialog.isShowing())
                                           progressDialog.dismiss();
                                       Toast.makeText(RegisterActivity.this, "Berhasil di tambahkan", Toast.LENGTH_SHORT).show();
//                                       Intent intent2 = new Intent(up.this, MainActivity.class);
//                           			startActivity(intent2);
                                   }
                               });
                           }
                   });
                   thread.start();
                   Intent in = new Intent(RegisterActivity.this, login.class);
      				startActivity(in);
				}

			}
		});

		// Link to Login Screen
		btnLinkToLogin.setOnClickListener(new View.OnClickListener() {

			public void onClick(View view) {
				Intent i = new Intent(getApplicationContext(),
						login.class);
				startActivity(i);
				// Close Registration View
				finish();
			}
		});
	}
	
	private void doinput() {
		// TODO Auto-generated method stub
		String name = inputFullName.getText().toString();
		String email = inputEmail.getText().toString();
		String password = inputPassword.getText().toString();
	  String a = "Guru";
	  	String b = "g";
	  	
		
		 //String input_data= "http://lms.novalherdinata.net/registerguru.php"; //URL website anda dengan file insert.php yang telah dibuat
		String input_data = Config.URL_API + "register";

		 HttpClient httpClient = new DefaultHttpClient();
         HttpPost httpPost = new HttpPost(input_data);  
         ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();  
         param.add(new BasicNameValuePair("name", name));
         param.add(new BasicNameValuePair("email", email)); 
         param.add(new BasicNameValuePair("password", password)); 
         param.add(new BasicNameValuePair("kelas", a)); 
         param.add(new BasicNameValuePair("status", b)); 
         try {  
              httpPost.setEntity(new UrlEncodedFormEntity(param));  
              HttpResponse httpRespose = httpClient.execute(httpPost);  
             HttpEntity httpEntity = httpRespose.getEntity();  
             InputStream in = httpEntity.getContent();  
             BufferedReader read = new BufferedReader(new InputStreamReader(in));  
            
             String isi= "";  
             String baris= "";  
            
             while((baris = read.readLine())!=null){  
                isi+= baris;  
             }

			 System.out.println(isi);
               
             //Jika isi tidak sama dengan "null " maka akan tampil Toast "Berhasil" sebaliknya akan tampil "Gagal"  
             if(!isi.equals("null")){                    
             }else{  
             }  
            
       } catch (ClientProtocolException e) {  
          // TODO Auto-generated catch block  
          e.printStackTrace();  
       } catch (IOException e) {  
          // TODO Auto-generated catch block  
          e.printStackTrace();  
       }  
    	
	}
}
