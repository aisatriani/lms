package com.nvlhrd.lmslearningsys;


import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import com.nvlhrd.lmslearningsys.adapter.ImageLoader;
import com.nvlhrd.lmslearningsys.adapter.JSONParser;
import com.nvlhrd.lmslearningsys.library.SessionManager;
import com.nvlhrd.lmslearningsys.listcourse.DownloadFileFromURL;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Typeface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.opengl.Visibility;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Toast;

public class listdetailtugasnilaionline extends Activity {

	private static final String TAG = "nilai_tugas";
	private ProgressDialog pDialog;
	// url to make request
	static String url;
	JSONArray contacts = null;

	TextView judul, ket;
	static final String JUDUL ="a", JUDUL2="b", JUDUL3="c", JUDUL4="d",JUDUL5="e",JUDUL6="f",JUDUL7="g",JUDUL8="h",JUDUL9="i",JUDUL10="j",JUDUL11="k",JUDUL12="l";
	static String foto;
	String namenya, des, fotodes;
	String PLACE_LATITUDE, PLACE_LONGITUDE;
	ImageView map,telp;
	LinearLayout aa;
	 LinearLayout count_layout;
	 int count = 0;
	 String deskrip, fotdet1, fotdet2, fotdet3, klan;
	 TextView txtdes, txtfotdet1, txtfotdet2;
	 ArrayList<String> list = new ArrayList<String>();
	 double screenInches;
	 ScrollView scroll;
	 
	 TextView namaprofile, kelasprofile,desktugas;
	 TextView  txtask, txtdeskask;
	String txtnama, txtkelas, txtemail, txtfoto, txtfile, txtidtugas;
	Button  upload;
	public static final int progress_bar_type = 0;
	File folder, folderpict;
	
	 private static final int SELECT_FILE1 = 1;
	  String selectedPath1 = "NONE";
	  HttpEntity resEntity;
	  ImageView fileupload;
	  ProgressDialog progressDialog;
	  SessionManager session;
		HashMap<String, String> user;
		
		TextView nama, kelas, txttfoto, txtjawaban;
		EditText nilaiform;
		Button btnnilai;
		ImageView imgfoto;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
		setContentView(R.layout.list_itemgurunilaionline);
		session = new SessionManager(getApplicationContext());
        session.checkLogin();
		 user = session.getUserDetails();
		
		nama = (TextView) findViewById(R.id.nama);
		kelas = (TextView) findViewById(R.id.kelas);
		txttfoto = (TextView) findViewById(R.id.txtfoto);
		btnnilai = (Button) findViewById(R.id.btnnilai);
		imgfoto = (ImageView) findViewById(R.id.foto);
		nilaiform = (EditText) findViewById(R.id.nilaiform);
		txtjawaban = (TextView) findViewById(R.id.jawaban);
		
		Intent in = getIntent();
		 txtnama = in.getStringExtra(JUDUL);
		 txtkelas = in.getStringExtra(JUDUL2);
		 txtemail = in.getStringExtra(JUDUL3);
		 txtfoto = in.getStringExtra(JUDUL4);
		 txtfile = in.getStringExtra(JUDUL5);
		 txtidtugas = in.getStringExtra(JUDUL6);
		 
		 txtjawaban.setText(""+txtfile);
		 
		 nama.setText(""+txtnama);
		 kelas.setText(""+txtkelas);
        
        runOnUiThread(new Runnable() {
			public void run() {
				 ImageView image = (ImageView) findViewById(R.id.foto);
			        ImageLoader imgLoader = new ImageLoader(getApplicationContext());
			        imgLoader.DisplayImage(txtfoto, image);
							}
						});
        
  
        
        btnnilai.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				int numnilai = 0;
				try {
					numnilai = Integer.parseInt(nilaiform.getText().toString());
				} catch(NumberFormatException nfe) {
				  // Handle parse error.
				}
				if(numnilai > 100){
					nilaiform.setError("Tidak boleh memasukan nilai lebih dari 100");
				}
				if(nilaiform.getText().toString().equalsIgnoreCase("")){
					nilaiform.setError("Nilai tidak boleh kosong");
				}
				
				if(!nilaiform.getText().toString().equalsIgnoreCase("") && numnilai <= 100){
				
				progressDialog = ProgressDialog.show(listdetailtugasnilaionline.this, "", "Loading.....", false);
                Thread thread=new Thread(new Runnable(){
                       public void run(){
                    	   doinput();
                           runOnUiThread(new Runnable(){
                               public void run() {
                                   if(progressDialog.isShowing())
                                       progressDialog.dismiss();
//                                   Intent intent2 = new Intent(up.this, MainActivity.class);
//                       			startActivity(intent2);
                                   Toast.makeText(listdetailtugasnilaionline.this, "Berhasil di tambahkan", Toast.LENGTH_SHORT).show();
                                   nilaiform.setFocusable(false);
                                   btnnilai.setVisibility(View.GONE); 
                               }
                           });
                       }
               });
               thread.start();
            
             
				}
			}
		});
	}
	
	
	  private void doinput() {
			// TODO Auto-generated method stub

//		  String idtugas = txtidtugas.toString();
//			String idguru = user.get(SessionManager.KEY_NAME);
//			String idsiswa =  txtemail;
//			String kelas = dashboard.JUDULKElas;
//			
			
			 //String input_data= "http://lms.novalherdinata.net/uploadnilaitugas.php?idtugas="+txtidtugas+"&idsiswa="+txtemail; //URL website anda dengan file insert.php yang telah dibuat
		  	String input_data = Config.URL_API + "tugas/"+txtidtugas+"/"+txtemail+"/nilai";
		  Log.d(TAG, "doinput: "+ input_data);

		  HttpClient httpClient = new DefaultHttpClient();
	         HttpPost httpPost = new HttpPost(input_data);  
	         ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();  
	         param.add(new BasicNameValuePair("nilai", nilaiform.getText().toString())); 
	         try {  
	              httpPost.setEntity(new UrlEncodedFormEntity(param));  
	              HttpResponse httpRespose = httpClient.execute(httpPost);  
	             HttpEntity httpEntity = httpRespose.getEntity();  
	             InputStream in = httpEntity.getContent();  
	             BufferedReader read = new BufferedReader(new InputStreamReader(in));  
	            
	             String isi= "";  
	             String baris= "";  
	            
	             while((baris = read.readLine())!=null){  
	                isi+= baris;  
	             }  
	               
	             //Jika isi tidak sama dengan "null " maka akan tampil Toast "Berhasil" sebaliknya akan tampil "Gagal"  
	             if(!isi.equals("null")){                    
	             }else{  
	             }  
	            
	       } catch (ClientProtocolException e) {  
	          // TODO Auto-generated catch block  
	          e.printStackTrace();  
	       } catch (IOException e) {  
	          // TODO Auto-generated catch block  
	          e.printStackTrace();  
	       }  
	    	
		}
	
	/**
	 * Showing Dialog
	 * */
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case progress_bar_type:
			pDialog = new ProgressDialog(this);
			pDialog.setMessage("Downloading file. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setMax(100);
			pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			pDialog.setCancelable(true);
			pDialog.show();
			return pDialog;
		default:
			return null;
		}
	}


}
