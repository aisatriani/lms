package com.nvlhrd.lmslearningsys;


import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import com.nvlhrd.lmslearningsys.adapter.JSONParser;
import com.nvlhrd.lmslearningsys.api.ApiGenerator;
import com.nvlhrd.lmslearningsys.api.ServicePengguna;
import com.nvlhrd.lmslearningsys.api.pojo.Pengguna;
import com.nvlhrd.lmslearningsys.library.DialogInfo;
import com.nvlhrd.lmslearningsys.library.SessionManager;
import com.nvlhrd.lmslearningsys.services.NotificationService;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class dashboard extends Activity {

    private static final String TAG = dashboard.class.getCanonicalName();
    TextView txtemailuser, txtnamauser, txtkelasuser, txtstatususer;
    LinearLayout linlaHeaderProgress;

    JSONArray contacts = null;
    // JSON Node names
    public static final String TAG_CONTACTS = "info";
    public static final String TAG_EMAIL = "email";
    public static final String TAG_NAMA = "nama";
    public static final String TAG_KELAS = "kelas";
    public static final String TAG_STATUS = "status";


    ArrayList<HashMap<String, String>> contactList;
    // url to make request
    static String url;
    String email, nama, kelas, status;
    SessionManager session;
    HashMap<String, String> user;
    static final String JUDUL = "a", JUDUL2 = "b", JUDUL3 = "c", JUDUL4 = "d", JUDUL5 = "e", JUDUL6 = "f", JUDUL7 = "g", JUDUL8 = "h", JUDUL9 = "i", JUDUL10 = "j", JUDUL11 = "k", JUDUL12 = "l", JUDUL13 = "m", JUDUL14 = "n";

    Button course;
    Button btn_friends;
    static String JUDULKElas, JUDULNAMA, JUDULSTATUS;
    Button btn_messages;
    Button btn_places;
    Button btn_events;
    Button btn_photos;
    MenuItem tambah;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.dashboard_layout);
        session = new SessionManager(getApplicationContext());
        session.checkLogin();
        user = session.getUserDetails();

        String deviceID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        session.storeDeviceId(deviceID);
        /**
         * Creating all buttons instances
         * */
        // Dashboard News feed button
        course = (Button) findViewById(R.id.btn_news_feed);

        // Dashboard Friends button
        btn_friends = (Button) findViewById(R.id.btn_friends);

        // Dashboard Messages button
        btn_messages = (Button) findViewById(R.id.btn_messages);

        // Dashboard Places button
        btn_places = (Button) findViewById(R.id.btn_places);

        // Dashboard Events button
        btn_events = (Button) findViewById(R.id.btn_events);

        // Dashboard Photos button
        btn_photos = (Button) findViewById(R.id.btn_photos);


        /**
         * Handling all button click events
         * */
        ActionBar actionBar = getActionBar();

        // Enabling Up / Back navigation
        actionBar.setDisplayHomeAsUpEnabled(false);
        contactList = new ArrayList<HashMap<String, String>>();
        // Loading INBOX in Background Thread

        //url = "http://lms.novalherdinata.net/getuser.php?nama="+ user.get(SessionManager.KEY_NAME);
        //url = Config.URL_API + "pengguna/"+user.get(SessionManager.KEY_NAME);
        //new LoadInbox().execute();

        Log.d(TAG, "onCreate: " + user.get(SessionManager.KEY_NAME));

        LoadInboxPengguna();

        //startService(new Intent(this, NotificationService.class));


    }

    private void LoadInboxPengguna() {

        linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
        linlaHeaderProgress.setVisibility(View.VISIBLE);

        ServicePengguna service = ApiGenerator.createService(ServicePengguna.class);
        Call<Pengguna> call = service.getPengunaByName(user.get(SessionManager.KEY_EMAIL));
        call.enqueue(new Callback<Pengguna>() {
            @Override
            public void onResponse(Call<Pengguna> call, Response<Pengguna> response) {
                linlaHeaderProgress.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    email = response.body().getEmail();
                    nama = response.body().getName();
                    kelas = response.body().getKelas();
                    status = response.body().getStatus();

                    registerClickHandler();
                }
            }

            @Override
            public void onFailure(Call<Pengguna> call, Throwable throwable) {
                linlaHeaderProgress.setVisibility(View.GONE);
                DialogInfo.showErrorDialog(dashboard.this, getString(R.string.error_connection));
            }
        });


    }

    private void registerClickHandler() {

        JUDULKElas = kelas;
        JUDULNAMA = nama;
        JUDULSTATUS = status;

        //Toast.makeText(dashboard.this, "" + JUDULNAMA + " " + JUDULKElas + " " + JUDULSTATUS, Toast.LENGTH_SHORT).show();

        course.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent in = new Intent(dashboard.this, listcourse.class);
                in.putExtra(JUDUL, nama);
                in.putExtra(JUDUL2, kelas);
                in.putExtra(JUDUL3, status);
                startActivity(in);
            }
        });

        btn_friends.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent in = new Intent(dashboard.this, listtugas.class);
                in.putExtra(JUDUL, nama);
                in.putExtra(JUDUL2, kelas);
                in.putExtra(JUDUL3, status);
                startActivity(in);
            }
        });

        btn_messages.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent in = new Intent(dashboard.this, forumactivity.class);
                in.putExtra(JUDUL, nama);
                in.putExtra(JUDUL2, kelas);
                in.putExtra(JUDUL3, status);
                startActivity(in);
            }
        });

        btn_places.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent in = new Intent(dashboard.this, raporlist.class);
                in.putExtra(JUDUL, nama);
                in.putExtra(JUDUL2, kelas);
                in.putExtra(JUDUL3, status);
                startActivity(in);
            }
        });


        btn_events.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent in = new Intent(dashboard.this, editprofile.class);
                in.putExtra(JUDUL, nama);
                in.putExtra(JUDUL2, kelas);
                in.putExtra(JUDUL3, status);
                startActivity(in);
            }
        });

        btn_photos.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (session.isLoggedIn()) {
                    session.logoutUser();
                    //stopService(new Intent(dashboard.this, NotificationService.class));
                }
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.maindashboard, menu);

        tambah = (MenuItem) menu.findItem(R.id.action_notif);

        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title

        // Handle action bar actions click
        switch (item.getItemId()) {

            case R.id.action_notif:
                Intent inn = new Intent(dashboard.this, listberita.class);
                startActivity(inn);

                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}