package com.nvlhrd.lmslearningsys;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;


import com.nvlhrd.lmslearningsys.library.SessionManager;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class inputforum extends Activity {

    EditText txtjudul, txtdeskripsi;
    Button ok;
    ProgressDialog progressDialog;
    int randomNo;
    SessionManager session;
    HashMap<String, String> user;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.tambahforum);
        session = new SessionManager(getApplicationContext());
        session.checkLogin();
        user = session.getUserDetails();

        txtjudul = (EditText) findViewById(R.id.forumjudul);
        txtdeskripsi = (EditText) findViewById(R.id.forumdeskripsi);
        ok = (Button) findViewById(R.id.btnforum);

        Random r = new Random();
        randomNo = r.nextInt(100000000 + 4);

        ok.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (txtdeskripsi.getText().toString().equalsIgnoreCase("")) {
                    txtdeskripsi.setError("Harus diisi");
                }
                if (txtjudul.getText().toString().equalsIgnoreCase("")) {
                    txtjudul.setError("Harus diisi");
                }
                if (!txtdeskripsi.getText().toString().equalsIgnoreCase("") && !txtjudul.getText().toString().equalsIgnoreCase("")) {
                    progressDialog = ProgressDialog.show(inputforum.this, "", "Loading.....", false);
                    Thread thread = new Thread(new Runnable() {
                        public void run() {
                            doinput();
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    if (progressDialog.isShowing())
                                        progressDialog.dismiss();
                                    txtjudul.setText("");
                                    txtdeskripsi.setText("");
//                                   Intent intent2 = new Intent(up.this, MainActivity.class);
//                       			startActivity(intent2);
                                }
                            });
                        }
                    });
                    thread.start();
                    Toast.makeText(inputforum.this, "Berhasil di tambahkan", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(inputforum.this, forumactivity.class);
                    startActivity(intent);
                }
            }
        });

    }

    private void doinput() {
        // TODO Auto-generated method stub

        String a = "" + randomNo;
        String b = user.get(SessionManager.KEY_EMAIL);
        String c = txtjudul.getText().toString();
        String d = txtdeskripsi.getText().toString();

        //String input_data= "http://lms.novalherdinata.net/inputforum.php"; //URL website anda dengan file insert.php yang telah dibuat
        String input_data = Config.URL_API + "forum";

        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(input_data);
        ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();
        param.add(new BasicNameValuePair("idforum", a));
        param.add(new BasicNameValuePair("iduser", b));
        param.add(new BasicNameValuePair("judul", c));
        param.add(new BasicNameValuePair("deskripsi", d));
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(param));
            HttpResponse httpRespose = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpRespose.getEntity();
            InputStream in = httpEntity.getContent();
            BufferedReader read = new BufferedReader(new InputStreamReader(in));

            String isi = "";
            String baris = "";

            while ((baris = read.readLine()) != null) {
                isi += baris;
            }

            //Jika isi tidak sama dengan "null " maka akan tampil Toast "Berhasil" sebaliknya akan tampil "Gagal"
            if (!isi.equals("null")) {
            } else {
            }

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // do something on back.
            Intent intent = new Intent(inputforum.this, forum.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

}
