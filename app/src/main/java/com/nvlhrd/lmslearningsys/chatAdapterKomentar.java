package com.nvlhrd.lmslearningsys;


import java.util.ArrayList;
import java.util.HashMap;


import com.nvlhrd.lmslearningsys.adapter.ImageLoader;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class chatAdapterKomentar extends BaseAdapter {
    
    private Activity activity;
    private ArrayList<HashMap<String, String>> data;
    private static LayoutInflater inflater=null;
    public ImageLoader imageLoader; 
    
    public chatAdapterKomentar(Activity a,  ArrayList<HashMap<String, String>> d) {
        activity = a;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader=new ImageLoader(activity.getApplicationContext());
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
//        	adapter = new SimpleAdapter(
//					chatutama.this, inboxList,
//					R.layout.inbox_list_item, new String[] { TAG_NAME, TAG_MESS },
//					new int[] { R.id.from, R.id.subject});
        	 vi = inflater.inflate(R.layout.list_rowkomentar, null);

        TextView nama = (TextView)vi.findViewById(R.id.namaprofile);
        TextView kelas = (TextView)vi.findViewById(R.id.kelasprofile);
        ImageView foto = (ImageView)vi.findViewById(R.id.foto);
        TextView komentar = (TextView)vi.findViewById(R.id.komentar);

        
        //hasmap
        HashMap<String, String> song = new HashMap<String, String>();
        song = data.get(position);
        
        // Setting all values in listview
        nama.setText(song.get(listdetailkomentar.TAG_NAME));
        kelas.setText(song.get(listdetailkomentar.TAG_KELAS));
        imageLoader.DisplayImage(song.get(listdetailkomentar.TAG_FOTO), foto);
        komentar.setText(song.get(listdetailkomentar.TAG_KOMENTAR));
        
        return vi;
    }
}