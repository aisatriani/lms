package com.nvlhrd.lmslearningsys;


import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import com.nvlhrd.lmslearningsys.adapter.ImageLoader;
import com.nvlhrd.lmslearningsys.library.DatabaseHandler;
import com.nvlhrd.lmslearningsys.library.SessionManager;
import com.nvlhrd.lmslearningsys.library.UserFunctions;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class LayoutDetailforum extends Activity {


//
//	
	// Session Manager Class
			SessionManager session;
			 TextView namaprofile, kelasprofile;
			 TextView  txtask, txtdeskask;
			 Button komentar;
			 
			String txtjudul, txtdeskripsi, txtidforum, txtiduser;
				String txtnama;
			 String txtfoto;
			String txtkelas;
				String txttglupload;
				static final String JUDUL = "a", JUDUL2 = "b", JUDUL3 = "c", JUDUL4 = "d",
						JUDUL5 = "e", JUDUL6 = "f", JUDUL7 = "g", JUDUL8 = "h",
						JUDUL9 = "i", JUDUL10 = "j", JUDUL11 = "k", JUDUL12 = "l";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.forumdetail);	
		
		
		
		 StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	        StrictMode.setThreadPolicy(policy);
	        
	    	Intent in = getIntent();
			 txtjudul = in.getStringExtra(JUDUL);
			 txtdeskripsi = in.getStringExtra(JUDUL2);
			 txtidforum = in.getStringExtra(JUDUL3);
			 txtiduser = in.getStringExtra(JUDUL4);
			 txtnama = in.getStringExtra(JUDUL5);
			 txtfoto = in.getStringExtra(JUDUL6);
			 txtkelas = in.getStringExtra(JUDUL7);
			 txttglupload = in.getStringExtra(JUDUL8);
	        session = new SessionManager(LayoutDetailforum.this.getApplicationContext());
	    	
			namaprofile = (TextView) findViewById(R.id.namaprofile);
			kelasprofile = (TextView) findViewById(R.id.kelasprofile);
//			komentar = (Button) findViewById(R.id.komenlihat);
			
			 namaprofile.setText(""+txtnama);
			 kelasprofile.setText(""+txtkelas);
			
			txtask = (TextView) findViewById(R.id.txtask);
			txtdeskask = (TextView) findViewById(R.id.desask);
			
			 txtask.setText(""+txtjudul);
			 txtdeskask.setText(""+txtdeskripsi);
	        
	        LayoutDetailforum.this.runOnUiThread(new Runnable() {
				public void run() {
					 ImageView image = (ImageView) findViewById(R.id.foto);
				        ImageLoader imgLoader = new ImageLoader(LayoutDetailforum.this.getApplicationContext());
				        imgLoader.DisplayImage(txtfoto, image);
								}
							});
	        
	        komentar.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent in = new Intent(LayoutDetailforum.this, LayoutDetailforumKomentar.class);
					in.putExtra(JUDUL, txtjudul);
					in.putExtra(JUDUL2, txtdeskripsi);
					in.putExtra(JUDUL3, txtidforum);
					in.putExtra(JUDUL4, txtiduser);
					in.putExtra(JUDUL5, txtnama);
					in.putExtra(JUDUL6, txtfoto);
					in.putExtra(JUDUL7, txtkelas);
					in.putExtra(JUDUL8, txttglupload);
					startActivity(in);
				}
			});
	        
	        
	        
	
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)  {
	    if (keyCode == KeyEvent.KEYCODE_BACK ) {
	        // do something on back.
	    	  Intent in = new Intent(LayoutDetailforum.this, forumactivity.class);
	    	  in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    	
		        startActivity(in);
	        return true;
	    }

	    return super.onKeyDown(keyCode, event);
	}
	
  

	
}
