package com.nvlhrd.lmslearningsys;


import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import com.nvlhrd.lmslearningsys.adapter.ImageLoader;
import com.nvlhrd.lmslearningsys.adapter.JSONParser;


import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.net.Uri;
import android.opengl.Visibility;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Toast;

public class listdetailkomentar extends Activity {
	
	private ProgressDialog pDialog;
	// url to make request
	static String url;
	JSONArray contacts = null;

	TextView judul, ket;
	static final String JUDUL ="a", JUDUL2="b", JUDUL3="c", JUDUL4="d",JUDUL5="e",JUDUL6="f",JUDUL7="g",JUDUL8="h",JUDUL9="i",JUDUL10="j",JUDUL11="k",JUDUL12="l";
	static String foto;
	String namenya, des, fotodes;
	String PLACE_LATITUDE, PLACE_LONGITUDE;
	ImageView map,telp;
	LinearLayout aa;
	 LinearLayout count_layout;
	 int count = 0;
	 String deskrip, fotdet1, fotdet2, fotdet3, klan;
	 TextView txtdes, txtfotdet1, txtfotdet2;
	 ArrayList<String> list = new ArrayList<String>();
	 double screenInches;
	 ScrollView scroll;
	 
	 TextView namaprofile, kelasprofile;
	 TextView  txtask, txtdeskask;
	String txtjudul, txtdeskripsi, txtidforum, txtiduser, txtnama, txtfoto, txtkelas, txttglupload;
	
	// JSON Node names
	public static final String TAG_CONTACTS = "info";
	public static final String TAG_NAME = "nama";
	public static final String TAG_KELAS= "kelas";
	public static final String TAG_FOTO = "foto";
	public static final String TAG_KOMENTAR = "komentar";

	String link;
	int current_page = 1;
	// contacts JSONArray
	chatAdapterKomentar adapter2;
	ListView lv;
	TextView name2;
	LinearLayout linlaHeaderProgress;
	LinearLayout tidakhasil;
	ArrayList<HashMap<String, String>> contactList;
	LinearLayout atas;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list_itemforum);
		
		namaprofile = (TextView) findViewById(R.id.namaprofile);
		kelasprofile = (TextView) findViewById(R.id.kelasprofile);
		
		txtask = (TextView) findViewById(R.id.txtask);
		txtdeskask = (TextView) findViewById(R.id.desask);
		
		ActionBar actionBar = getActionBar();

		// Enabling Up / Back navigation
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setTitle("Komentar");
		
		Intent in = getIntent();
		 txtjudul = in.getStringExtra(JUDUL);
		 txtdeskripsi = in.getStringExtra(JUDUL2);
		 txtidforum = in.getStringExtra(JUDUL3);
		 txtiduser = in.getStringExtra(JUDUL4);
		 txtnama = in.getStringExtra(JUDUL5);
		 txtfoto = in.getStringExtra(JUDUL6);
		 txtkelas = in.getStringExtra(JUDUL7);
		 txttglupload = in.getStringExtra(JUDUL8);
		 
		 namaprofile.setText(""+txtnama);
		 kelasprofile.setText(""+txtkelas);
		 
		 txtask.setText(""+txtjudul);
		 txtdeskask.setText(""+txtdeskripsi);
        
        runOnUiThread(new Runnable() {
			public void run() {
				 ImageView image = (ImageView) findViewById(R.id.foto);
			        ImageLoader imgLoader = new ImageLoader(getApplicationContext());
			        imgLoader.DisplayImage(txtfoto, image);
							}
						});
        
        lv = (ListView) findViewById(R.id.list);
        atas = (LinearLayout) findViewById(R.id.atasatas);
        
        lv.setOnScrollListener(new OnScrollListener() {
            private int mLastFirstVisibleItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            public void onScroll(AbsListView view, int firstVisibleItem,
                    int visibleItemCount, int totalItemCount) {

                if(mLastFirstVisibleItem<firstVisibleItem)
                {
                    Log.i("SCROLLING DOWN","TRUE");
                    atas.setVisibility(View.GONE);
                    
                }
                if(mLastFirstVisibleItem>firstVisibleItem)
                {
                    Log.i("SCROLLING UP","TRUE");
                    atas.setVisibility(View.VISIBLE);
                }
                mLastFirstVisibleItem=firstVisibleItem;

            }
        });
        contactList = new ArrayList<HashMap<String, String>>();
		// Loading INBOX in Background Thread
		tidakhasil = (LinearLayout) findViewById(R.id.noitem);
		tidakhasil.setVisibility(View.GONE);

		url = "http://lms.novalherdinata.net/komentarparsing.php?nama="+txtidforum+"&page=1";

		new LoadInbox().execute();
        
	}
	
	class LoadInbox extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
			linlaHeaderProgress.setVisibility(View.VISIBLE);
		}

		/**
		 * getting Inbox JSON
		 * */
		protected String doInBackground(String... args) {
			// Creating JSON Parser instance
			JSONParser jParser = new JSONParser();

			// getting JSON string from URL
			JSONObject json = jParser.getJSONFromUrl(url);

			try {
				// Getting Array of Contacts
				contacts = json.getJSONArray(TAG_CONTACTS);

				// looping through All Contacts
				for (int i = 0; i < contacts.length(); i++) {
					JSONObject c = contacts.getJSONObject(i);

					// Storing each json item in variable
					String nama = c.getString(TAG_NAME);
					String kelas = c.getString(TAG_KELAS);
					String foto = c.getString(TAG_FOTO);
					String komentar = c.getString(TAG_KOMENTAR);

					foto = foto.replaceAll(" ", "%20");
					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();

					// adding each child node to HashMap key => value
					// adding each child node to HashMap key => value
					map.put(TAG_NAME, nama);
					map.put(TAG_KELAS, kelas);
					map.put(TAG_FOTO, foto);
					map.put(TAG_KOMENTAR, komentar);
				

					// adding HashList to ArrayList
					contactList.add(map);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			// HIDE THE SPINNER AFTER LOADING FEEDS
			linlaHeaderProgress.setVisibility(View.GONE);

			if (contactList.size() < 1) {
				tidakhasil.setVisibility(View.VISIBLE);
			}
			// LoadMore button
			if (contactList.size() > 9) {

			
			}

			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					adapter2 = new chatAdapterKomentar(listdetailkomentar.this, contactList);
					adapter2.notifyDataSetChanged();
					lv.setAdapter(adapter2);
					lv.invalidateViews();

					lv.setOnItemClickListener(new OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
						
						}
					});
				}
			});
		}
	}

	private class loadMoreListView extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// Showing progress dialog before sending http request
			pDialog = new ProgressDialog(listdetailkomentar.this);
			pDialog.setMessage("Please wait..");
			pDialog.setIndeterminate(true);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		protected Void doInBackground(Void... unused) {
			// Creating JSON Parser instance
			current_page += 1;

			url = "http://lms.novalherdinata.net/komentarparsing.php?nama="+txtidforum+"&page="
					+ current_page;
			JSONParser jParser = new JSONParser();

			// getting JSON string from URL
			JSONObject json = jParser.getJSONFromUrl(url);

			try {
				// Getting Array of Contacts
				contacts = json.getJSONArray(TAG_CONTACTS);

				// looping through All Contacts
				for (int i = 0; i < contacts.length(); i++) {
					JSONObject c = contacts.getJSONObject(i);
					// Storing each json item in variable
					// Storing each json item in variable
					String nama = c.getString(TAG_NAME);
					String kelas = c.getString(TAG_KELAS);
					String foto = c.getString(TAG_FOTO);
					String komentar = c.getString(TAG_KOMENTAR);

					foto = foto.replaceAll(" ", "%20");
					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();

					// adding each child node to HashMap key => value
					// adding each child node to HashMap key => value
					map.put(TAG_NAME, nama);
					map.put(TAG_KELAS, kelas);
					map.put(TAG_FOTO, foto);
					map.put(TAG_KOMENTAR, komentar);
				
					contactList.add(map);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		protected void onPostExecute(Void unused) {
			// closing progress dialog
			pDialog.dismiss();

			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					int currentPosition = lv.getFirstVisiblePosition();
					adapter2 = new chatAdapterKomentar(listdetailkomentar.this, contactList);
					adapter2.notifyDataSetChanged();
					lv.setAdapter(adapter2);
					lv.invalidateViews();
					lv.setSelectionFromTop(currentPosition + 1, 0);
					lv.setOnItemClickListener(new OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
						
						}
					});
				}
			});
		}
	}

}
