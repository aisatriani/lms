package com.nvlhrd.lmslearningsys.api;

import com.nvlhrd.lmslearningsys.api.pojo.Course;
import com.nvlhrd.lmslearningsys.api.pojo.Message;
import com.nvlhrd.lmslearningsys.api.pojo.Pengguna;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by aisatriani on 27/07/16.
 */
public interface ServiceCourse {

    @GET("course/name/{id}")
    Call<List<Course>> getCourseByName(@Path("id") String name);

    @GET("course/kelas/{id}")
    Call<List<Course>> getCourseByKelas(@Path("id") String kelas);

    @DELETE("course/{id}")
    Call<Message> deleteCourseById(@Path("id") String id);

}
