package com.nvlhrd.lmslearningsys.api;

import com.nvlhrd.lmslearningsys.api.pojo.Course;
import com.nvlhrd.lmslearningsys.api.pojo.Message;
import com.nvlhrd.lmslearningsys.api.pojo.NilaiTugas;
import com.nvlhrd.lmslearningsys.api.pojo.Tugas;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by aisatriani on 27/07/16.
 */
public interface ServiceTugas {

    @GET("tugas/name/{id}")
    Call<List<Tugas>> getTugasByName(@Path("id") String name);

    @GET("tugas/kelas/{id}")
    Call<List<Tugas>> getTugasByKelas(@Path("id") String kelas);

    @FormUrlEncoded
    @POST("tugas")
    Call<Message> addTugas(
            @Field("idtugas") String idtugas,
            @Field("idguru") String idguru,
            @Field("judul") String judul,
            @Field("deskripsi") String deskripsi,
            @Field("kelas") String kelas,
            @Field("timestop") String waktu
    );

    @DELETE("tugas/{id}")
    Call<Message> deleteTugasById(@Path("id") String id);

    @GET("tugas/{idtugas}/{idsiswa}/check")
    Call<NilaiTugas> checkTugas(@Path("idtugas") String idtugas, @Path("idsiswa") String idsiswa);

    @GET("jawabtugas/{id}")
    Call<List<NilaiTugas>> getJawabTugas(@Path("id") String idtugas);

    @POST("tugas/{idtugas}/{idsiswa}/nilai")
    Call<Message> beriNilai(@Path("idtugas") String idtugas, @Path("idsiswa") String idsiswa);

}
