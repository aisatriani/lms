package com.nvlhrd.lmslearningsys.api;

import com.nvlhrd.lmslearningsys.api.pojo.Forum;
import com.nvlhrd.lmslearningsys.api.pojo.Komentar;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by aisatriani on 05/08/16.
 */
public interface ServiceForum {

    @GET("forum")
    Call<List<Forum>> getAllForum();

    @GET("forum/{iduser}/thread")
    Call<List<Forum>> getMyForum(@Path("iduser") String iduser);

    @GET("forum/{idforum}/komentar")
    Call<List<Komentar>> getKomentar(@Path("idforum") String idforum);
}
