package com.nvlhrd.lmslearningsys.api;

import com.nvlhrd.lmslearningsys.api.pojo.Berita;
import com.nvlhrd.lmslearningsys.api.pojo.Message;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by aisatriani on 06/08/16.
 */
public interface ServiceBerita {

//    $app->get('/berita/{email}/guru','BeritaController@getBeritaByUserEmail');
//    $app->get('/berita/{kelas}/kelas','BeritaController@getBeritaByKelas');
//    $app->post('/berita','BeritaController@store');

    @GET("berita/{email}/guru")
    Call<List<Berita>> getBeritaByEmail(@Path("email") String email);

    @GET("berita/{kelas}/siswa")
    Call<List<Berita>> getBeritaByKelas(@Path("kelas") String kelas);


}
