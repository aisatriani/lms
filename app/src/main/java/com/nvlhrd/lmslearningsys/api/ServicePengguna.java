package com.nvlhrd.lmslearningsys.api;

import com.nvlhrd.lmslearningsys.api.pojo.Pengguna;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by aisatriani on 04/07/16.
 */
public interface ServicePengguna {

    @FormUrlEncoded
    @POST("login")
    Call<Pengguna> login(@Field("user") String name, @Field("pass") String pass);

    @GET("pengguna/{id}")
    Call<Pengguna> getPengunaByName(@Path("id") String name);

    @GET("pengguna/{email}/profile")
    Call<Pengguna> getPenggunaByEmail(@Path("email") String email);

}
