package com.nvlhrd.lmslearningsys.api.pojo;

/**
 * Created by aisatriani on 06/08/16.
 */
public class Komentar {
    private int no;
    private String idkomen;
    private String idforum;
    private String iduser;
    private String komentar;
    private String tgl;
    private Pengguna pengguna;
    private Forum forum;

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getIdkomen() {
        return idkomen;
    }

    public void setIdkomen(String idkomen) {
        this.idkomen = idkomen;
    }

    public String getIdforum() {
        return idforum;
    }

    public void setIdforum(String idforum) {
        this.idforum = idforum;
    }

    public String getIduser() {
        return iduser;
    }

    public void setIduser(String iduser) {
        this.iduser = iduser;
    }

    public String getKomentar() {
        return komentar;
    }

    public void setKomentar(String komentar) {
        this.komentar = komentar;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }

    public Pengguna getPengguna() {
        return pengguna;
    }

    public void setPengguna(Pengguna pengguna) {
        this.pengguna = pengguna;
    }

    public Forum getForum() {
        return forum;
    }

    public void setForum(Forum forum) {
        this.forum = forum;
    }
}
