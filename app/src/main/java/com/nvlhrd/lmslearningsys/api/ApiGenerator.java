package com.nvlhrd.lmslearningsys.api;

import com.nvlhrd.lmslearningsys.Config;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiGenerator {

    public static final String API_BASE_URL = Config.URL_API;

    private static OkHttpClient.Builder httpClient = null;

    private static Retrofit.Builder builder = new Retrofit.Builder();
    private static Retrofit retrofit = null;


    private static HttpLoggingInterceptor loggingInterceptor(){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }

    public static <S> S createService(Class<S> serviceClass) {

        if(httpClient == null){
            httpClient = new OkHttpClient.Builder();
            httpClient.readTimeout(30,TimeUnit.SECONDS);
            httpClient.connectTimeout(30, TimeUnit.SECONDS);
        }

        httpClient.addInterceptor(loggingInterceptor());


        builder.baseUrl(API_BASE_URL);
        builder.addConverterFactory(GsonConverterFactory.create());
        builder.client(httpClient.build());

        if(retrofit == null){
            retrofit = builder.build();
        }

        return retrofit.create(serviceClass);
    }

    public Retrofit getRetrofit(){
        return retrofit;
    }

}
