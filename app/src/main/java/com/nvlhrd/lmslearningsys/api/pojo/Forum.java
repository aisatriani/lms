package com.nvlhrd.lmslearningsys.api.pojo;

/**
 * Created by aisatriani on 05/08/16.
 */
public class Forum {
    private int no;
    private String idforum;
    private String iduser;
    private String judul;
    private String deskripsi;
    private String tgl;
    private Pengguna pengguna;

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getIdforum() {
        return idforum;
    }

    public void setIdforum(String idforum) {
        this.idforum = idforum;
    }

    public String getIduser() {
        return iduser;
    }

    public void setIduser(String iduser) {
        this.iduser = iduser;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }

    public Pengguna getPengguna() {
        return pengguna;
    }

    public void setPengguna(Pengguna pengguna) {
        this.pengguna = pengguna;
    }
}
