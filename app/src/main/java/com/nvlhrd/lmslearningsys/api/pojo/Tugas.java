package com.nvlhrd.lmslearningsys.api.pojo;

import java.util.List;

/**
 * Created by aisatriani on 27/07/16.
 */
public class Tugas {

    private String
            idtugas,
            idguru,
            judul,
            deskripsi,
            file,
            kelas,
            timestop;

    private List<NilaiTugas> nilai;
    private Pengguna pengguna;


    public String getIdtugas() {
        return idtugas;
    }

    public void setIdtugas(String idtugas) {
        this.idtugas = idtugas;
    }

    public String getIdguru() {
        return idguru;
    }

    public void setIdguru(String idguru) {
        this.idguru = idguru;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getTimestop() {
        return timestop;
    }

    public void setTimestop(String timestop) {
        this.timestop = timestop;
    }

    public List<NilaiTugas> getNilai() {
        return nilai;
    }

    public void setNilai(List<NilaiTugas> nilai) {
        this.nilai = nilai;
    }

    public Pengguna getPengguna() {
        return pengguna;
    }

    public void setPengguna(Pengguna pengguna) {
        this.pengguna = pengguna;
    }
}
