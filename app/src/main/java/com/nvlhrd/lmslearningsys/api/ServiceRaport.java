package com.nvlhrd.lmslearningsys.api;

import com.nvlhrd.lmslearningsys.api.pojo.NilaiTugas;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by aisatriani on 06/08/16.
 */
public interface ServiceRaport {

    @GET("raport/{idsiswa}/siswa")
    Call<List<NilaiTugas>> getRaportBySiswa(@Path("idsiswa") String idsiswa);

    @GET("raport/{idguru}/guru")
    Call<List<NilaiTugas>> getRaportByGuru(@Path("idguru") String idguru);
}
