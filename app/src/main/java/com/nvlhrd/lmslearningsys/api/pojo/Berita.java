package com.nvlhrd.lmslearningsys.api.pojo;

/**
 * Created by aisatriani on 06/08/16.
 */
public class Berita {
    private int id;
    private String idberita;
    private String idguru;
    private String judul;
    private String deskripsi;
    private String tgl;
    private String kelas;
    private Pengguna pengguna;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdberita() {
        return idberita;
    }

    public void setIdberita(String idberita) {
        this.idberita = idberita;
    }

    public String getIdguru() {
        return idguru;
    }

    public void setIdguru(String idguru) {
        this.idguru = idguru;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }

    public Pengguna getPengguna() {
        return pengguna;
    }

    public void setPengguna(Pengguna pengguna) {
        this.pengguna = pengguna;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }
}
