package com.nvlhrd.lmslearningsys;

import java.util.HashMap;


import com.nvlhrd.lmslearningsys.library.SessionManager;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ListView;
import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;

public class forumkomentar extends FragmentActivity{

	private ViewPager _mViewPager;
	private ViewPagerAdapterForum _adapter;
	SessionManager session;
	HashMap<String, String> user;
	 String txtjudul, txtdeskripsi, txtidforum, txtiduser;
	String txtnama;
	 String txtfoto;
	String txtkelas;
	String txttglupload;
	
	static String unama, ufoto, ukelas, fjudul, fdeskripsi, fid;
	
	static final String JUDUL = "a", JUDUL2 = "b", JUDUL3 = "c", JUDUL4 = "d",
			JUDUL5 = "e", JUDUL6 = "f", JUDUL7 = "g", JUDUL8 = "h",
			JUDUL9 = "i", JUDUL10 = "j", JUDUL11 = "k", JUDUL12 = "l";
	String email, nama, kelas, status;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.forumhome);
		session = new SessionManager(getApplicationContext());
        session.checkLogin();
        
        //intent
        
    	Intent in = getIntent();
		 txtjudul = in.getStringExtra(JUDUL);
		 txtdeskripsi = in.getStringExtra(JUDUL2);
		 txtidforum = in.getStringExtra(JUDUL3);
		 txtiduser = in.getStringExtra(JUDUL4);
		 txtnama = in.getStringExtra(JUDUL5);
		 txtfoto = in.getStringExtra(JUDUL6);
		 txtkelas = in.getStringExtra(JUDUL7);
		 txttglupload = in.getStringExtra(JUDUL8);
		 
		 unama = txtnama;
		 ufoto = txtfoto;
		 ukelas = txtkelas;
		 
		 //forum
		 fjudul = txtjudul;
		 fdeskripsi = txtdeskripsi;
		 fid = txtidforum;
		 
		 ActionBar actionBar = getActionBar();

			// Enabling Up / Back navigation
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setTitle("Forum");
		 
		setUpView();
        setTab();
	}
	
	 private void setUpView(){    	
	   	 _mViewPager = (ViewPager) findViewById(R.id.viewPager);
	     _adapter = new ViewPagerAdapterForum(getApplicationContext(),getSupportFragmentManager());
	     _mViewPager.setAdapter(_adapter);
		 _mViewPager.setCurrentItem(0);
	    }
	    private void setTab(){
				_mViewPager.setOnPageChangeListener(new OnPageChangeListener(){
				    		
							@Override
							public void onPageScrollStateChanged(int position) {}
							@Override
							public void onPageScrolled(int arg0, float arg1, int arg2) {}
							@Override
							public void onPageSelected(int position) {
								// TODO Auto-generated method stub
								switch(position){
								case 0:
									findViewById(R.id.first_tab).setVisibility(View.VISIBLE);
									findViewById(R.id.second_tab).setVisibility(View.INVISIBLE);
									break;
									
								case 1:
									findViewById(R.id.first_tab).setVisibility(View.INVISIBLE);
									findViewById(R.id.second_tab).setVisibility(View.VISIBLE);
									break;
									
									
								}
							}
							
						});
				
//				tab1.setOnClickListener(new OnClickListener() {
//		            @Override
//		            public void onClick(View v) {
//		            	 _mViewPager.setCurrentItem(0);
//		            }
//		        });
//				
//				tab2.setOnClickListener(new OnClickListener() {
//		            @Override
//		            public void onClick(View v) {
//		            	 _mViewPager.setCurrentItem(1);
//		            }
//		        });
//				
//				tab3.setOnClickListener(new OnClickListener() {
//		            @Override
//		            public void onClick(View v) {
//		            	 _mViewPager.setCurrentItem(2);
//		            }
//		        });

	    }
	    
	    @Override
		public boolean onCreateOptionsMenu(Menu menu) {
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.forumkomentar, menu);
			
			MenuItem tambah = (MenuItem) menu.findItem(R.id.action_tambah);
			
			return super.onCreateOptionsMenu(menu);
			
		}
		
		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			// toggle nav drawer on selecting action bar app icon/title
		
			// Handle action bar actions click
			switch (item.getItemId()) {
			case R.id.action_tambah:
					Intent intent = new Intent(this, inputforumkomentar.class);
					startActivity(intent);
				
				break;
			default:
				return super.onOptionsItemSelected(item);
			}
			return true;
		}

	    
	    

}
