package com.nvlhrd.lmslearningsys;


import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import com.nvlhrd.lmslearningsys.adapter.JSONParser;
import com.nvlhrd.lmslearningsys.api.ApiGenerator;
import com.nvlhrd.lmslearningsys.api.ServiceCourse;
import com.nvlhrd.lmslearningsys.api.pojo.Course;
import com.nvlhrd.lmslearningsys.api.pojo.Message;
import com.nvlhrd.lmslearningsys.library.DialogInfo;
import com.nvlhrd.lmslearningsys.library.SessionManager;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class listcourse extends Activity {

	private static final String TAG = listcourse.class.getCanonicalName();
	ArrayList<HashMap<String, String>> contactList;
	// url to make request
	static String url;
	static String NAMA;
	static final String JUDUL = "a", JUDUL2 = "b", JUDUL3 = "c", JUDUL4 = "d",
			JUDUL5 = "e", JUDUL6 = "f", JUDUL7 = "g", JUDUL8 = "h",
			JUDUL9 = "i", JUDUL10 = "j", JUDUL11 = "k", JUDUL12 = "l";

	// JSON Node names
	public static final String TAG_CONTACTS = "info";
	public static final String TAG_NAME = "judul";
	public static final String TAG_DESKRIPSI = "deskripsi";
	public static final String TAG_FILE = "file";
	public static final String TAG_TGLUPLOAD = "tgl";
	public static final String TAG_IDGURU = "idguru";
	public static final String TAG_ID= "idcourse";
	String link;
	int current_page = 1;
	// contacts JSONArray
	JSONArray contacts = null;
	chatAdapter adapter2;
	ListView lv;
	TextView name2;
	LinearLayout linlaHeaderProgress;
	LinearLayout tidakhasil;
	String file;

	// Progress Dialog
	private ProgressDialog pDialog;

	// Progress dialog type (0 - for Horizontal progress bar)
	public static final int progress_bar_type = 0;

	// File url to download
	File folder, folderpict;
	String txtjudul, txtfile, txtid;
	String email, nama, kelas, status;
	SessionManager session;
	HashMap<String, String> user;
	String jkelas;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list);
		 session = new SessionManager(getApplicationContext());
	        session.checkLogin();
			 user = session.getUserDetails();
			 
				


		lv = (ListView) findViewById(R.id.list);
		ActionBar actionBar = getActionBar();

		// Enabling Up / Back navigation
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setTitle("Materi");
		
		//intent
		
		Intent in = getIntent();
       nama = in.getStringExtra(JUDUL);
       kelas = in.getStringExtra(JUDUL2);
       status = in.getStringExtra(JUDUL3);
		
		contactList = new ArrayList<HashMap<String, String>>();
		// Loading INBOX in Background Thread
		tidakhasil = (LinearLayout) findViewById(R.id.noitem);
		tidakhasil.setVisibility(View.GONE);

		//jkelas = dashboard.JUDULKElas.replaceAll(" ", "%20");
		jkelas = dashboard.JUDULKElas;
		if(dashboard.JUDULSTATUS.equalsIgnoreCase("s")){
			loadCurseMurid();
		}
		
		if(dashboard.JUDULSTATUS.equalsIgnoreCase("g")){

			loadCurseGuru();
		}
	

		
	}

	private void loadCurseGuru() {
		linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
		linlaHeaderProgress.setVisibility(View.VISIBLE);

		ServiceCourse service = ApiGenerator.createService(ServiceCourse.class);
		Call<List<Course>> call = service.getCourseByName(user.get(SessionManager.KEY_EMAIL));
		call.enqueue(new Callback<List<Course>>() {
			@Override
			public void onResponse(Call<List<Course>> call, Response<List<Course>> response) {
				linlaHeaderProgress.setVisibility(View.GONE);
				if (response.isSuccessful()) {
					for (Course course : response.body()) {

						// creating new HashMap
						HashMap<String, String> map = new HashMap<String, String>();

						// adding each child node to HashMap key => value
						// adding each child node to HashMap key => value
						map.put(TAG_NAME, course.getJudul());
						map.put(TAG_DESKRIPSI, course.getDeskripsi());
						map.put(TAG_FILE, course.getFile());
						map.put(TAG_TGLUPLOAD, course.getTgl());
						map.put(TAG_IDGURU, course.getIdguru());
						map.put(TAG_ID, course.getIdcourse());

						// adding HashList to ArrayList
						contactList.add(map);

					}

					populateTolist();

				}
			}

			@Override
			public void onFailure(Call<List<Course>> call, Throwable throwable) {
				linlaHeaderProgress.setVisibility(View.GONE);
				DialogInfo.showErrorDialog(listcourse.this, getString(R.string.error_connection));
			}
		});

	}

	private void loadCurseMurid() {

		linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
		linlaHeaderProgress.setVisibility(View.VISIBLE);

		ServiceCourse service = ApiGenerator.createService(ServiceCourse.class);
		Call<List<Course>> call = service.getCourseByKelas(jkelas);
		call.enqueue(new Callback<List<Course>>() {
			@Override
			public void onResponse(Call<List<Course>> call, Response<List<Course>> response) {
				linlaHeaderProgress.setVisibility(View.GONE);
				if(response.isSuccessful()){
					for(Course course : response.body()){

						// creating new HashMap
						HashMap<String, String> map = new HashMap<String, String>();

						// adding each child node to HashMap key => value
						// adding each child node to HashMap key => value
						map.put(TAG_NAME, course.getJudul());
						map.put(TAG_DESKRIPSI, course.getDeskripsi());
						map.put(TAG_FILE, course.getFile());
						map.put(TAG_TGLUPLOAD, course.getTgl());
						map.put(TAG_IDGURU, course.getIdguru());
						map.put(TAG_ID, course.getIdcourse());

						// adding HashList to ArrayList
						contactList.add(map);

					}

					populateTolist();
				}
			}

			@Override
			public void onFailure(Call<List<Course>> call, Throwable throwable) {
				linlaHeaderProgress.setVisibility(View.GONE);
				DialogInfo.showErrorDialog(listcourse.this, getString(R.string.error_connection));
			}
		});

	}

	private void populateTolist() {
		adapter2 = new chatAdapter(listcourse.this, contactList);
		adapter2.notifyDataSetChanged();
		lv.setAdapter(adapter2);
		lv.invalidateViews();

		lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent,
									View view, int position, long id) {

				txtjudul = ((TextView) view
						.findViewById(R.id.title)).getText()
						.toString();
				txtfile = ((TextView) view
						.findViewById(R.id.downloadfile)).getText()
						.toString();
				txtid = ((TextView) view
						.findViewById(R.id.idcourse)).getText()
						.toString();
				if(dashboard.JUDULSTATUS.equalsIgnoreCase("g")){
					AlertDialog.Builder alertDialog = new AlertDialog.Builder(listcourse.this);

					// Setting Dialog Title
					alertDialog.setTitle("Pilih");

					// Setting Dialog Message
					alertDialog.setMessage("Apa yang akan anda lakukan?");

					// Setting Positive "Yes" Button
					alertDialog.setPositiveButton("Download", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int which) {

							if(txtfile.equalsIgnoreCase("")){
								Toast.makeText(listcourse.this, "tidak ada file terlampir", Toast.LENGTH_SHORT).show();
							}else{
								new DownloadFileFromURL().execute(txtfile);
							}
						}
					});

					// Setting Negative "NO" Button
					alertDialog.setNegativeButton("Hapus", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							//delete
							ServiceCourse service = ApiGenerator.createService(ServiceCourse.class);
							Call<Message> call = service.deleteCourseById(txtid);
							call.enqueue(new Callback<Message>() {
								@Override
								public void onResponse(Call<Message> call, Response<Message> response) {
									adapter2.remove();
									if(dashboard.JUDULSTATUS.equalsIgnoreCase("s")){
										url = "http://lms.novalherdinata.net/coursemurid.php?nama="+jkelas;
										loadCurseMurid();
									}

									if(dashboard.JUDULSTATUS.equalsIgnoreCase("g")){
										url = "http://lms.novalherdinata.net/courseparsing.php?nama="+user.get(SessionManager.KEY_NAME);
										loadCurseGuru();
									}
								}

								@Override
								public void onFailure(Call<Message> call, Throwable throwable) {
									DialogInfo.showErrorDialog(listcourse.this, getString(R.string.error_connection));
								}
							});


						}
					});

					// Showing Alert Message
					alertDialog.show();
				}else{

					txtjudul = ((TextView) view
							.findViewById(R.id.title)).getText()
							.toString();
					txtfile = ((TextView) view
							.findViewById(R.id.downloadfile)).getText()
							.toString();

					if(txtfile.equalsIgnoreCase("")){
						Toast.makeText(listcourse.this, "tidak ada file terlampir", Toast.LENGTH_SHORT).show();
					}else{
						new DownloadFileFromURL().execute(txtfile);
						Toast.makeText(listcourse.this, "Download file terlampir", Toast.LENGTH_SHORT).show();
					}
				}
			}
		});
	}


	class LoadInbox extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
			linlaHeaderProgress.setVisibility(View.VISIBLE);
		}

		/**
		 * getting Inbox JSON
		 * */
		protected String doInBackground(String... args) {
			// Creating JSON Parser instance
			JSONParser jParser = new JSONParser();

			// getting JSON string from URL
			JSONObject json = jParser.getJSONFromUrl(url);

			try {
				// Getting Array of Contacts
				contacts = json.getJSONArray(TAG_CONTACTS);

				// looping through All Contacts
				for (int i = 0; i < contacts.length(); i++) {
					JSONObject c = contacts.getJSONObject(i);

					// Storing each json item in variable
					String judul = c.getString(TAG_NAME);
					String deskripsi = c.getString(TAG_DESKRIPSI);
					file = c.getString(TAG_FILE);
					String tglupload = c.getString(TAG_TGLUPLOAD);
					String idguru = c.getString(TAG_IDGURU);
					String id = c.getString(TAG_ID);

					file = file.replaceAll(" ", "%20");

					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();

					// adding each child node to HashMap key => value
					// adding each child node to HashMap key => value
					map.put(TAG_NAME, judul);
					map.put(TAG_DESKRIPSI, deskripsi);
					map.put(TAG_FILE, file);
					map.put(TAG_TGLUPLOAD, tglupload);
					map.put(TAG_IDGURU, idguru);
					map.put(TAG_ID, id);

					// adding HashList to ArrayList
					contactList.add(map);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			// HIDE THE SPINNER AFTER LOADING FEEDS
			linlaHeaderProgress.setVisibility(View.GONE);

			if (contactList.size() < 1) {
				tidakhasil.setVisibility(View.VISIBLE);
			}
			// LoadMore button
			if (contactList.size() > 9) {


			}

			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					adapter2 = new chatAdapter(listcourse.this, contactList);
					adapter2.notifyDataSetChanged();
					lv.setAdapter(adapter2);
					lv.invalidateViews();

					lv.setOnItemClickListener(new OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							
							txtjudul = ((TextView) view
									.findViewById(R.id.title)).getText()
									.toString();
							txtfile = ((TextView) view
									.findViewById(R.id.downloadfile)).getText()
									.toString();
							txtid = ((TextView) view
									.findViewById(R.id.idcourse)).getText()
									.toString();
							if(dashboard.JUDULSTATUS.equalsIgnoreCase("g")){
								AlertDialog.Builder alertDialog = new AlertDialog.Builder(listcourse.this);
								 
						        // Setting Dialog Title
						        alertDialog.setTitle("Pilih");
						 
						        // Setting Dialog Message
						        alertDialog.setMessage("Apa yang akan anda lakukan?");
						 
						        // Setting Positive "Yes" Button
						        alertDialog.setPositiveButton("Download", new DialogInterface.OnClickListener() {
						            public void onClick(DialogInterface dialog,int which) {
									
										if(txtfile.equalsIgnoreCase("")){
											Toast.makeText(listcourse.this, "tidak ada file terlampir", Toast.LENGTH_SHORT).show();
										}else{
										new DownloadFileFromURL().execute(txtfile);
										}
						            }
						        });
						 
						        // Setting Negative "NO" Button
						        alertDialog.setNegativeButton("Hapus", new DialogInterface.OnClickListener() {
						            public void onClick(DialogInterface dialog, int which) {
						         //delete
						            	String input_data= "http://lms.novalherdinata.net/deletemateri.php?idcourse="+txtid; //URL website anda dengan file insert.php yang telah dibuat  
							            
								         HttpClient httpClient = new DefaultHttpClient();  
								         HttpPost httpPost = new HttpPost(input_data);  
								         ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();  
								         param.add(new BasicNameValuePair("id_rack", "sa")); 
								         try {  
								              httpPost.setEntity(new UrlEncodedFormEntity(param));  
								              HttpResponse httpRespose = httpClient.execute(httpPost);  
								             HttpEntity httpEntity = httpRespose.getEntity();  
								             InputStream in = httpEntity.getContent();  
								             BufferedReader read = new BufferedReader(new InputStreamReader(in));  
								            
								             String isi= "";  
								             String baris= "";  
								            
								             while((baris = read.readLine())!=null){  
								                isi+= baris;  
								             }  
								               
								             //Jika isi tidak sama dengan "null " maka akan tampil Toast "Berhasil" sebaliknya akan tampil "Gagal"  
								             if(!isi.equals("null")){                    
								             }else{  
								             }  
								            
								       } catch (ClientProtocolException e) {  
								          // TODO Auto-generated catch block  
								          e.printStackTrace();  
								       } catch (IOException e) {  
								          // TODO Auto-generated catch block  
								          e.printStackTrace();  
								       } 
								         
						            	adapter2.remove();
						            	if(dashboard.JUDULSTATUS.equalsIgnoreCase("s")){
						        			url = "http://lms.novalherdinata.net/coursemurid.php?nama="+jkelas;
						        		}
						        		
						        		if(dashboard.JUDULSTATUS.equalsIgnoreCase("g")){
						        			url = "http://lms.novalherdinata.net/courseparsing.php?nama="+user.get(SessionManager.KEY_NAME);
						        		}
						        	

						        		new LoadInbox().execute();
						            }
						        });
						 
						        // Showing Alert Message
						        alertDialog.show();
							}else{
							
							txtjudul = ((TextView) view
									.findViewById(R.id.title)).getText()
									.toString();
							txtfile = ((TextView) view
									.findViewById(R.id.downloadfile)).getText()
									.toString();
						
							if(txtfile.equalsIgnoreCase("")){
								Toast.makeText(listcourse.this, "tidak ada file terlampir", Toast.LENGTH_SHORT).show();
							}else{
							new DownloadFileFromURL().execute(txtfile);
							}
							}
						}
					});
				}
			});
		
		}
	}

	private class loadMoreListView extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// Showing progress dialog before sending http request
			pDialog = new ProgressDialog(listcourse.this);
			pDialog.setMessage("Please wait..");
			pDialog.setIndeterminate(true);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		protected Void doInBackground(Void... unused) {
			// Creating JSON Parser instance
			current_page += 1;
			
			if(dashboard.JUDULSTATUS.equalsIgnoreCase("s")){

				url = "http://lms.novalherdinata.net/coursemurid.php?nama="+jkelas+"&page="
					+ current_page;
			}else if(dashboard.JUDULSTATUS.equalsIgnoreCase("g")){
				url = "http://lms.novalherdinata.net/courseparsing.php?nama="+user.get(SessionManager.KEY_NAME)+"&page="
						+ current_page;
			}
			JSONParser jParser = new JSONParser();

			// getting JSON string from URL
			JSONObject json = jParser.getJSONFromUrl(url);

			try {
				// Getting Array of Contacts
				contacts = json.getJSONArray(TAG_CONTACTS);

				// looping through All Contacts
				for (int i = 0; i < contacts.length(); i++) {
					JSONObject c = contacts.getJSONObject(i);

					// Storing each json item in variable
					String judul = c.getString(TAG_NAME);
					String deskripsi = c.getString(TAG_DESKRIPSI);
					file = c.getString(TAG_FILE);
					String tglupload = c.getString(TAG_TGLUPLOAD);
					String idguru = c.getString(TAG_IDGURU);
					String id = c.getString(TAG_ID);

					file = file.replaceAll(" ", "%20");

					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();

					// adding each child node to HashMap key => value
					// adding each child node to HashMap key => value
					map.put(TAG_NAME, judul);
					map.put(TAG_DESKRIPSI, deskripsi);
					map.put(TAG_FILE, file);
					map.put(TAG_TGLUPLOAD, tglupload);
					map.put(TAG_IDGURU, idguru);
					map.put(TAG_ID, id);
					// adding HashList to ArrayList
					contactList.add(map);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		protected void onPostExecute(Void unused) {
			// closing progress dialog
			pDialog.dismiss();

			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					int currentPosition = lv.getFirstVisiblePosition();
					adapter2 = new chatAdapter(listcourse.this, contactList);
					adapter2.notifyDataSetChanged();
					lv.setAdapter(adapter2);
					lv.invalidateViews();
					lv.setSelectionFromTop(currentPosition + 1, 0);
					lv.setOnItemClickListener(new OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							txtjudul = ((TextView) view
									.findViewById(R.id.title)).getText()
									.toString();
							txtfile = ((TextView) view
									.findViewById(R.id.downloadfile)).getText()
									.toString();
							if(txtfile.equalsIgnoreCase("")){
								Toast.makeText(listcourse.this, "tidak ada file terlampir", Toast.LENGTH_SHORT).show();
							}else{
							new DownloadFileFromURL().execute(txtfile);
							}
						}
					});
				}
			});
		}
	}

	/**
	 * Showing Dialog
	 * */
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case progress_bar_type:
			pDialog = new ProgressDialog(this);
			pDialog.setMessage("Download file. Mohon Tunggu...");
			pDialog.setIndeterminate(false);
			pDialog.setMax(100);
			pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			pDialog.setCancelable(true);
			pDialog.show();
			return pDialog;
		default:
			return null;
		}
	}

	/**
	 * Background Async Task to download file
	 * */
	class DownloadFileFromURL extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Bar Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showDialog(progress_bar_type);
		}

		/**
		 * Downloading file in background thread
		 * */
		@Override
		protected String doInBackground(String... f_url) {
			int count;
			try {
				String extStorageDirectory = Environment
						.getExternalStorageDirectory().toString();
				folderpict = new File(extStorageDirectory, "/MobileLearning");
				if (!folderpict.exists()) {
					folderpict.mkdir();
				} else {
					folder = new File(extStorageDirectory,
							"/MobileLearning/Course");
					if (!folder.exists()) {
						folder.mkdir();
					}

				}

				URL url = new URL(f_url[0]);
				URLConnection conection = url.openConnection();
				conection.connect();
				// getting file length
				int lenghtOfFile = conection.getContentLength();

				// input stream to read file - with 8k buffer
				InputStream input = new BufferedInputStream(url.openStream(),
						8192);

				// Output stream to write file
				OutputStream output = new FileOutputStream(extStorageDirectory
						+ "/MobileLearning/Course/" + txtjudul+".pdf");

				byte data[] = new byte[1024];

				long total = 0;

				while ((count = input.read(data)) != -1) {
					total += count;
					// publishing the progress....
					// After this onProgressUpdate will be called
					publishProgress("" + (int) ((total * 100) / lenghtOfFile));

					// writing data to file
					output.write(data, 0, count);
				}

				// flushing output
				output.flush();
				MediaScannerConnection
						.scanFile(listcourse.this,
								new String[] { extStorageDirectory
										+ "/MobileLearning/Course/" + txtjudul+".pdf" },
								new String[] { "application/pdf" }, null);
				// closing streams
				output.close();
				input.close();

			} catch (Exception e) {
				Log.e("Error: ", e.getMessage());
			}

			return null;
		}

		/**
		 * Updating progress bar
		 * */
		protected void onProgressUpdate(String... progress) {
			// setting progress percentage
			pDialog.setProgress(Integer.parseInt(progress[0]));
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		@Override
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after the file was downloaded
			dismissDialog(progress_bar_type);

			showPdf();

			// Displaying downloaded image into image view
			// Reading image path from sdcard
		}

	}
	
	 public void showPdf()
     {
		 
		 String extStorageDirectory = Environment
					.getExternalStorageDirectory().toString();
         File file = new File( extStorageDirectory
					+ "/MobileLearning/Course/" + txtjudul+".pdf" );
         PackageManager packageManager = getPackageManager();
         Intent testIntent = new Intent(Intent.ACTION_VIEW);
         testIntent.setType("application/pdf");
         List list = packageManager.queryIntentActivities(testIntent, PackageManager.MATCH_DEFAULT_ONLY);
         Intent intent = new Intent();
         intent.setAction(Intent.ACTION_VIEW);
         Uri uri = Uri.fromFile(file);
         intent.setDataAndType(uri, "application/pdf");
		 //Log.d(TAG, "showPdf: "+ intent.getPackage());
		 try{
			 startActivity(intent);
		 }catch (ActivityNotFoundException e){
			e.printStackTrace();
			 DialogInfo.showErrorDialog(this,"File tidak bisa dibuka. Harap install aplikasi pembaca file pdf");
		 }

   }
	 

		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.main, menu);
			
			MenuItem tambah = (MenuItem) menu.findItem(R.id.action_tambah);
			
			if(dashboard.JUDULSTATUS.equalsIgnoreCase("g")){
				tambah.setVisible(true);
			}
			if(dashboard.JUDULSTATUS.equalsIgnoreCase("s")){
				tambah.setVisible(false);
			}
			
			return super.onCreateOptionsMenu(menu);
			
		}
		
		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			// toggle nav drawer on selecting action bar app icon/title
		
			// Handle action bar actions click
			switch (item.getItemId()) {
			case R.id.action_tambah:
					Intent intent = new Intent(this, uploadcourse.class);
					startActivity(intent);
				break;
			default:
				return super.onOptionsItemSelected(item);
			}
			return true;
		}
		
		@Override
		public boolean onKeyDown(int keyCode, KeyEvent event)  {
		    if (keyCode == KeyEvent.KEYCODE_BACK ) {
		        // do something on back.
		    	  Intent intent = new Intent(listcourse.this, dashboard.class);
		    	  intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			        startActivity(intent);
		        return true;
		    }

		    return super.onKeyDown(keyCode, event);
		}

}