package com.nvlhrd.lmslearningsys;


import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import com.nvlhrd.lmslearningsys.adapter.ImageLoader;
import com.nvlhrd.lmslearningsys.adapter.JSONParser;
import com.nvlhrd.lmslearningsys.api.ApiGenerator;
import com.nvlhrd.lmslearningsys.api.ServiceForum;
import com.nvlhrd.lmslearningsys.api.pojo.Forum;
import com.nvlhrd.lmslearningsys.forum.LoadInbox;
import com.nvlhrd.lmslearningsys.library.DatabaseHandler;
import com.nvlhrd.lmslearningsys.library.DialogInfo;
import com.nvlhrd.lmslearningsys.library.SessionManager;
import com.nvlhrd.lmslearningsys.library.UserFunctions;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LayoutForumGlobal extends Fragment {


	
	
	public static Fragment newInstance(Context context) {
		LayoutForumGlobal f = new LayoutForumGlobal();	
		
		return f;
	}
//
//	ArrayList<HashMap<String, String>> contactList;
	// url to make request
	ArrayList<HashMap<String, String>> contactList;
	static String url;
	static String NAMA;
	static final String JUDUL = "a", JUDUL2 = "b", JUDUL3 = "c", JUDUL4 = "d",
			JUDUL5 = "e", JUDUL6 = "f", JUDUL7 = "g", JUDUL8 = "h",
			JUDUL9 = "i", JUDUL10 = "j", JUDUL11 = "k", JUDUL12 = "l";

	// JSON Node names
	public static final String TAG_CONTACTS = "info";
	public static final String TAG_JUDUL = "judul";
	public static final String TAG_DESKRIPSI = "deskripsi";
	public static final String TAG_FORUM = "idforum";
	public static final String TAG_IDUSER = "iduser";
	public static final String TAG_NAMA = "nama";
	public static final String TAG_TGL = "tgl";
	public static final String TAG_FOTO = "foto";
	public static final String TAG_KELAS = "kelas";
	String link;
	int current_page = 1;
	// contacts JSONArray
	JSONArray contacts = null;
	chatAdapterForum adapter2;
	ListView lv;
	TextView name2;
	LinearLayout linlaHeaderProgress;
	LinearLayout tidakhasil;
	String foto;

	// Progress Dialog
	private ProgressDialog pDialog;

	// Progress dialog type (0 - for Horizontal progress bar)
	public static final int progress_bar_type = 0;

	// File url to download
	File folder, folderpict;
	String txtjudul, txtfile;
	ViewGroup root;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		root = (ViewGroup) inflater.inflate(R.layout.list, null);		
		
		 StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	        StrictMode.setThreadPolicy(policy);
	    	lv = (ListView) root.findViewById(R.id.list);

			// Enabling Up / Back navigation
			contactList = new ArrayList<HashMap<String, String>>();
			// Loading INBOX in Background Thread
			tidakhasil = (LinearLayout) root.findViewById(R.id.noitem);
			tidakhasil.setVisibility(View.GONE);

			//url = "http://lms.novalherdinata.net/forum.php?page=1";

			//new LoadInbox().execute();
			loadAllForum();
	
        return root;
	}

	private void loadAllForum() {
		linlaHeaderProgress = (LinearLayout) root.findViewById(R.id.linlaHeaderProgress);
		linlaHeaderProgress.setVisibility(View.VISIBLE);

		ServiceForum service = ApiGenerator.createService(ServiceForum.class);
		Call<List<Forum>> call = service.getAllForum();
		call.enqueue(new Callback<List<Forum>>() {
			@Override
			public void onResponse(Call<List<Forum>> call, Response<List<Forum>> response) {
				linlaHeaderProgress.setVisibility(View.GONE);
				List<Forum> list = response.body();
				for(Forum forum : list){

					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();

					// adding each child node to HashMap key => value
					// adding each child node to HashMap key => value
					map.put(TAG_JUDUL, forum.getJudul());
					map.put(TAG_DESKRIPSI, forum.getDeskripsi());
					map.put(TAG_FORUM, forum.getIdforum());
					map.put(TAG_IDUSER, forum.getIduser());
					map.put(TAG_NAMA, forum.getPengguna().getName());
					map.put(TAG_TGL, forum.getTgl());
					map.put(TAG_FOTO, forum.getPengguna().getFoto());
					map.put(TAG_KELAS, forum.getPengguna().getKelas());


					// adding HashList to ArrayList
					contactList.add(map);

				}

				populateList();


			}

			@Override
			public void onFailure(Call<List<Forum>> call, Throwable t) {
				linlaHeaderProgress.setVisibility(View.GONE);
				DialogInfo.showErrorDialog(getActivity(),"Gagal Terhubung ke server. Harap ulangi kembali");
			}
		});
	}

	private void populateList() {
		adapter2 = new chatAdapterForum(getActivity(), contactList);
		adapter2.notifyDataSetChanged();
		lv.setAdapter(adapter2);
		lv.invalidateViews();

		lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent,
									View view, int position, long id) {
				String judul = ((TextView) view.findViewById(R.id.title)).getText().toString();
				String deskripsi = ((TextView) view.findViewById(R.id.deskripsi)).getText().toString();
				String idforum = ((TextView) view.findViewById(R.id.idforum)).getText().toString();
				String iduser = ((TextView) view.findViewById(R.id.iduser)).getText().toString();
				String nama = ((TextView) view.findViewById(R.id.nama)).getText().toString();
				String foto = ((TextView) view.findViewById(R.id.foto)).getText().toString();
				String kelas = ((TextView) view.findViewById(R.id.kelas)).getText().toString();
				String tgl = ((TextView) view.findViewById(R.id.tglupload)).getText().toString();
				Intent in = new Intent(getActivity(), LayoutDetailforumKomentar.class);
				in.putExtra(JUDUL, judul);
				in.putExtra(JUDUL2, deskripsi);
				in.putExtra(JUDUL3, idforum);
				in.putExtra(JUDUL4, iduser);
				in.putExtra(JUDUL5, nama);
				in.putExtra(JUDUL6, foto);
				in.putExtra(JUDUL7, kelas);
				in.putExtra(JUDUL8, tgl);
				startActivity(in);
			}
		});
	}

	class LoadInbox extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			linlaHeaderProgress = (LinearLayout) root.findViewById(R.id.linlaHeaderProgress);
			linlaHeaderProgress.setVisibility(View.VISIBLE);
		}

		/**
		 * getting Inbox JSON
		 * */
		protected String doInBackground(String... args) {
			// Creating JSON Parser instance
			JSONParser jParser = new JSONParser();

			// getting JSON string from URL
			JSONObject json = jParser.getJSONFromUrl(url);

			try {
				// Getting Array of Contacts
				contacts = json.getJSONArray(TAG_CONTACTS);

				// looping through All Contacts
				for (int i = 0; i < contacts.length(); i++) {
					JSONObject c = contacts.getJSONObject(i);

					// Storing each json item in variable
					String judul = c.getString(TAG_JUDUL);
					String deskripsi = c.getString(TAG_DESKRIPSI);
					String idforum = c.getString(TAG_FORUM);
					String iduser = c.getString(TAG_IDUSER);
					String nama = c.getString(TAG_NAMA);
					String tgl = c.getString(TAG_TGL);
					String foto = c.getString(TAG_FOTO);
					String kelas = c.getString(TAG_KELAS);
				

					foto = foto.replaceAll(" ", "%20");

					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();

					// adding each child node to HashMap key => value
					// adding each child node to HashMap key => value
					map.put(TAG_JUDUL, judul);
					map.put(TAG_DESKRIPSI, deskripsi);
					map.put(TAG_FORUM, idforum);
					map.put(TAG_IDUSER, iduser);
					map.put(TAG_NAMA, nama);
					map.put(TAG_TGL, tgl);
					map.put(TAG_FOTO, foto);
					map.put(TAG_KELAS, kelas);
				

					// adding HashList to ArrayList
					contactList.add(map);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			// HIDE THE SPINNER AFTER LOADING FEEDS
			linlaHeaderProgress.setVisibility(View.GONE);

			if (contactList.size() < 1) {
				tidakhasil.setVisibility(View.VISIBLE);
			}
			// LoadMore button
			if (contactList.size() > 9) {


			}

			// updating UI from Background Thread
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					adapter2 = new chatAdapterForum(getActivity(), contactList);
					adapter2.notifyDataSetChanged();
					lv.setAdapter(adapter2);
					lv.invalidateViews();

					lv.setOnItemClickListener(new OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							String judul = ((TextView) view.findViewById(R.id.title)).getText().toString();
							String deskripsi = ((TextView) view.findViewById(R.id.deskripsi)).getText().toString();
							String idforum = ((TextView) view.findViewById(R.id.idforum)).getText().toString();
							String iduser = ((TextView) view.findViewById(R.id.iduser)).getText().toString();
							String nama = ((TextView) view.findViewById(R.id.nama)).getText().toString();
							String foto = ((TextView) view.findViewById(R.id.foto)).getText().toString();
							String kelas = ((TextView) view.findViewById(R.id.kelas)).getText().toString();
							String tgl = ((TextView) view.findViewById(R.id.tglupload)).getText().toString();
							Intent in = new Intent(getActivity(), LayoutDetailforumKomentar.class);
							in.putExtra(JUDUL, judul);
							in.putExtra(JUDUL2, deskripsi);
							in.putExtra(JUDUL3, idforum);
							in.putExtra(JUDUL4, iduser);
							in.putExtra(JUDUL5, nama);
							in.putExtra(JUDUL6, foto);
							in.putExtra(JUDUL7, kelas);
							in.putExtra(JUDUL8, tgl);
							startActivity(in);
						}
					});
				}
			});
		}
	}

	private class loadMoreListView extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// Showing progress dialog before sending http request
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Please wait..");
			pDialog.setIndeterminate(true);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		protected Void doInBackground(Void... unused) {
			// Creating JSON Parser instance
			current_page += 1;

			url = "http://lms.novalherdinata.net/forum.php?page="
					+ current_page;
			JSONParser jParser = new JSONParser();

			// getting JSON string from URL
			JSONObject json = jParser.getJSONFromUrl(url);

			try {
				// Getting Array of Contacts
				contacts = json.getJSONArray(TAG_CONTACTS);

				// looping through All Contacts
				for (int i = 0; i < contacts.length(); i++) {
					JSONObject c = contacts.getJSONObject(i);

					// Storing each json item in variable
					String judul = c.getString(TAG_JUDUL);
					String deskripsi = c.getString(TAG_DESKRIPSI);
					String idforum = c.getString(TAG_FORUM);
					String iduser = c.getString(TAG_IDUSER);
					String nama = c.getString(TAG_NAMA);
					String tgl = c.getString(TAG_TGL);
					String foto = c.getString(TAG_FOTO);
					String kelas = c.getString(TAG_KELAS);
				

					foto = foto.replaceAll(" ", "%20");

					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();

					// adding each child node to HashMap key => value
					// adding each child node to HashMap key => value
					map.put(TAG_JUDUL, judul);
					map.put(TAG_DESKRIPSI, deskripsi);
					map.put(TAG_FORUM, idforum);
					map.put(TAG_IDUSER, iduser);
					map.put(TAG_NAMA, nama);
					map.put(TAG_TGL, tgl);
					map.put(TAG_FOTO, foto);
					map.put(TAG_KELAS, kelas);
					contactList.add(map);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		protected void onPostExecute(Void unused) {
			// closing progress dialog
			pDialog.dismiss();

			// updating UI from Background Thread
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					int currentPosition = lv.getFirstVisiblePosition();
					adapter2 = new chatAdapterForum(getActivity(), contactList);
					adapter2.notifyDataSetChanged();
					lv.setAdapter(adapter2);
					lv.invalidateViews();
					lv.setSelectionFromTop(currentPosition + 1, 0);
					lv.setOnItemClickListener(new OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							String judul = ((TextView) view.findViewById(R.id.title)).getText().toString();
							String deskripsi = ((TextView) view.findViewById(R.id.deskripsi)).getText().toString();
							String idforum = ((TextView) view.findViewById(R.id.idforum)).getText().toString();
							String iduser = ((TextView) view.findViewById(R.id.iduser)).getText().toString();
							String nama = ((TextView) view.findViewById(R.id.nama)).getText().toString();
							String foto = ((TextView) view.findViewById(R.id.foto)).getText().toString();
							String kelas = ((TextView) view.findViewById(R.id.kelas)).getText().toString();
							String tgl = ((TextView) view.findViewById(R.id.tglupload)).getText().toString();
							Intent in = new Intent(getActivity(), LayoutDetailforum.class);
							in.putExtra(JUDUL, judul);
							in.putExtra(JUDUL2, deskripsi);
							in.putExtra(JUDUL3, idforum);
							in.putExtra(JUDUL4, iduser);
							in.putExtra(JUDUL5, nama);
							in.putExtra(JUDUL6, foto);
							in.putExtra(JUDUL7, kelas);
							in.putExtra(JUDUL8, tgl);
							startActivity(in);
						}
					});
				}
			});
		}
	}
	
}
