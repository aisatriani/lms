package com.nvlhrd.lmslearningsys;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;


import com.nvlhrd.lmslearningsys.library.SessionManager;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class inputforumkomentar extends Activity {

    EditText txtdeskripsi;
    String txtjudul, eddeskripsi, txtidforum, txtiduser;
    String txtnama;
    String txtfoto;
    String txtkelas;
    String txttglupload;
    Button ok;
    ProgressDialog progressDialog;
    int randomNo;
    SessionManager session;
    HashMap<String, String> user;
    static final String JUDUL = "a", JUDUL2 = "b", JUDUL3 = "c", JUDUL4 = "d",
            JUDUL5 = "e", JUDUL6 = "f", JUDUL7 = "g", JUDUL8 = "h",
            JUDUL9 = "i", JUDUL10 = "j", JUDUL11 = "k", JUDUL12 = "l";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.tambahkomentar);
        session = new SessionManager(getApplicationContext());
        session.checkLogin();
        user = session.getUserDetails();

        Intent in = getIntent();
        txtjudul = in.getStringExtra(JUDUL);
        eddeskripsi = in.getStringExtra(JUDUL2);
        txtidforum = in.getStringExtra(JUDUL3);
        txtiduser = in.getStringExtra(JUDUL4);
        txtnama = in.getStringExtra(JUDUL5);
        txtfoto = in.getStringExtra(JUDUL6);
        txtkelas = in.getStringExtra(JUDUL7);
        txttglupload = in.getStringExtra(JUDUL8);

        txtdeskripsi = (EditText) findViewById(R.id.komentardeskripsi);
        ok = (Button) findViewById(R.id.btnkomentar);

        Random r = new Random();
        randomNo = r.nextInt(100000000 + 1);

        ok.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (txtdeskripsi.getText().toString().equalsIgnoreCase("")) {
                    txtdeskripsi.setError("Harus diisi");
                }
                if (!txtdeskripsi.getText().toString().equalsIgnoreCase("")) {
                    progressDialog = ProgressDialog.show(inputforumkomentar.this, "", "Loading.....", false);
                    Thread thread = new Thread(new Runnable() {
                        public void run() {
                            doinput();
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    if (progressDialog.isShowing())
                                        progressDialog.dismiss();
                                    txtdeskripsi.setText("");
//                                   Intent intent2 = new Intent(up.this, MainActivity.class);
//                       			startActivity(intent2);
                                }
                            });
                        }
                    });
                    thread.start();
                    Toast.makeText(inputforumkomentar.this, "Berhasil di tambahkan", Toast.LENGTH_SHORT).show();
                    Intent in = new Intent(inputforumkomentar.this, LayoutDetailforumKomentar.class);
                    in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    in.putExtra(JUDUL, txtjudul);
                    in.putExtra(JUDUL2, eddeskripsi);
                    in.putExtra(JUDUL3, txtidforum);
                    in.putExtra(JUDUL4, txtiduser);
                    in.putExtra(JUDUL5, txtnama);
                    in.putExtra(JUDUL6, txtfoto);
                    in.putExtra(JUDUL7, txtkelas);
                    in.putExtra(JUDUL8, txttglupload);
                    startActivity(in);
                }
            }
        });

    }

    private void doinput() {
        // TODO Auto-generated method stub

        String a = "" + randomNo;
        String b = user.get(SessionManager.KEY_EMAIL);
        ;
        String c = txtidforum.toString();
        String d = txtdeskripsi.getText().toString();

        //String input_data = "http://lms.novalherdinata.net/inputkomentar.php";
        String input_data = Config.URL_API + "forum/komentar";

        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(input_data);
        ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();
        param.add(new BasicNameValuePair("idkomen", a));
        param.add(new BasicNameValuePair("idforum", c));
        param.add(new BasicNameValuePair("iduser", b));
        param.add(new BasicNameValuePair("komentar", d));
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(param));
            HttpResponse httpRespose = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpRespose.getEntity();
            InputStream in = httpEntity.getContent();
            BufferedReader read = new BufferedReader(new InputStreamReader(in));

            String isi = "";
            String baris = "";

            while ((baris = read.readLine()) != null) {
                isi += baris;
            }

            //Jika isi tidak sama dengan "null " maka akan tampil Toast "Berhasil" sebaliknya akan tampil "Gagal"
            if (!isi.equals("null")) {
            } else {
            }

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}
