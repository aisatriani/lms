package com.nvlhrd.lmslearningsys.services;

/**
 * Created by aisatriani on 07/08/16.
 */
public class MessageData {
    // action : berita, course, tugas, nilai
    private String action;
    private String message;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
