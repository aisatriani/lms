package com.nvlhrd.lmslearningsys.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.google.gson.Gson;
import com.nvlhrd.lmslearningsys.Config;
import com.nvlhrd.lmslearningsys.R;
import com.nvlhrd.lmslearningsys.api.pojo.Course;
import com.nvlhrd.lmslearningsys.api.pojo.Pengguna;
import com.nvlhrd.lmslearningsys.library.SessionManager;
import com.nvlhrd.lmslearningsys.listberita;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.android.service.MqttService;
import org.eclipse.paho.client.mqttv3.DisconnectedBufferOptions;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.MqttPingSender;
import org.eclipse.paho.client.mqttv3.internal.wire.MqttDisconnect;

public class NotificationService extends Service implements MqttCallbackExtended {

    private static final String TAG = NotificationService.class.getSimpleName();
    private static final String ACTION_BERITA = "berita";
    private MqttAndroidClient mqttAndroidClient;
    private SessionManager session;
    private Pengguna pengguna;
    private int notificationID = 100;
    private MqttConnectOptions mqttConnectOption;
    private boolean connected;

    public NotificationService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.d(TAG, "onStartCommand: service started");
        session = new SessionManager(getApplicationContext());
        pengguna = session.getDataPengguna();
        Log.d(TAG, "onStartCommand: device id" + session.getDeviceId());

        if(!isAlreadyConnected() && !isServiceConnected()) {
            try {
                connectToBroker();
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }else{
            Log.d(TAG, "onStartCommand: service already running and registered");
        }

        return super.onStartCommand(intent,flags,startId);
    }

    private void handleTopikAndMessage(String topic, MqttMessage message) {
        if (topic.equals(String.valueOf(pengguna.getId()))) {
            onPrivateMessage(message);
        }

        if (topic.equals(String.valueOf(pengguna.getKelas()))) {
            onKelasMessage(message);
        }
    }

    private void onKelasMessage(MqttMessage message) {

        MessageData messageData = ParseMessageData(message);
        if (messageData.getAction().equals(ACTION_BERITA)) {
            showNotification("PENGUMUMAN", messageData.getMessage(), listberita.class);
        }

    }

    private MessageData ParseMessageData(MqttMessage message) {
        Gson gson = new Gson();
        MessageData messageData = gson.fromJson(message.toString(), MessageData.class);
        return messageData;
    }

    private void onPrivateMessage(MqttMessage message) {

    }

    private void subscribeTopik() throws MqttException {

        if (!isAlreadyConnected()) {
            // quick sanity check - don't try and subscribe if we
            //  don't have a connection

            Log.e("mqtt", "Unable to subscribe as we are not connected");

        } else {

            // subscrite all topic
            mqttAndroidClient.subscribe(Config.MQTT_DEFAULT_TOPIC, 0, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    Log.d(TAG, "onSuccess: success subscribe");
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Log.d(TAG, "subscribe onFailure: " + exception.getMessage());
                }
            });

            //subscribe self
            mqttAndroidClient.subscribe(Config.MQTT_ROOT_TOPIC + pengguna.getEmail(), 0);
            if (pengguna.getStatus().equals("s"))
                mqttAndroidClient.subscribe(Config.MQTT_ROOT_TOPIC + pengguna.getKelas(), 0);


        }

    }

    private void showNotification(String contentTitle, String contentText, Class action) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setAutoCancel(true);
        builder.setContentTitle(contentTitle);
        builder.setContentText(contentText);
        builder.setSmallIcon(R.drawable.logo);

        Intent intentAction = new Intent(this, action);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(listberita.class);

   /* Adds the Intent that starts the Activity to the top of the stack */
        stackBuilder.addNextIntent(intentAction);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

   /* notificationID allows you to update the notification later on. */
        mNotificationManager.notify(++notificationID, builder.build());

    }

    private boolean isAlreadyConnected() {
        return getMqttClient().isConnected();
    }

    private boolean isOnline()
    {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
        if(cm.getActiveNetworkInfo() != null &&
                cm.getActiveNetworkInfo().isAvailable() &&
                cm.getActiveNetworkInfo().isConnected())
        {
            return true;
        }

        return false;
    }

    private MqttAndroidClient getMqttClient() {
        if(mqttAndroidClient == null){
            mqttAndroidClient = new MqttAndroidClient(getApplicationContext(),Config.MQTT_SERVER,session.getDeviceId());
        }
        return mqttAndroidClient;
    }

    private synchronized boolean connectToBroker() throws MqttException {

        connected = false;



        getMqttClient().setCallback(this);
        getMqttClient().setTraceEnabled(true);
        getMqttClient().connect(getMqttConnectOption(), null, new IMqttActionListener() {
            @Override
            public void onSuccess(IMqttToken asyncActionToken) {

                try {

//                    DisconnectedBufferOptions disconnectedBufferOptions = new DisconnectedBufferOptions();
//                    disconnectedBufferOptions.setBufferEnabled(true);
//                    disconnectedBufferOptions.setBufferSize(100);
//                    disconnectedBufferOptions.setPersistBuffer(false);
//                    disconnectedBufferOptions.setDeleteOldestMessages(false);
//                    getMqttClient().setBufferOpts(disconnectedBufferOptions);


                    subscribeTopik();

                    connected = true;

                } catch (MqttException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                //exception.printStackTrace();
                Log.d(TAG, "connect onFailure: " + exception.getMessage());
                try {
                    connectToBroker();
                } catch (MqttException e) {
                    e.printStackTrace();
                }
            }
        });



        return connected;
    }

    @Override
    public void connectComplete(boolean reconnect, String serverURI) {
        if (reconnect) {
            try {
                Log.d(TAG, "connectComplete: reconnect and resubscribe topic");
                connected = true;
                subscribeTopik();
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void connectionLost(Throwable cause) {
        if(cause != null)
            Log.d(TAG, "connectionLost: " + cause.getMessage());

        connected = false;
        Log.d(TAG, "connectionLost: connection close");

    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        Log.d(TAG, "messageArrived: from " + topic + " " + new String(message.getPayload()));
        handleTopikAndMessage(topic, message);
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {

    }

    private MqttConnectOptions getMqttConnectOption() {
        mqttConnectOption= new MqttConnectOptions();
        mqttConnectOption.setUserName(Config.MQTT_USER);
        mqttConnectOption.setPassword(Config.MQTT_PASS.toCharArray());
        mqttConnectOption.setAutomaticReconnect(true);
        mqttConnectOption.setCleanSession(false);
        return mqttConnectOption;
    }

    private synchronized boolean isServiceConnected()
    {
        return connected;
    }

    @Override
    public void onDestroy() {
        disconnectedBroker();
        super.onDestroy();

    }

    private void disconnectedBroker() {


        try
        {
            if (mqttAndroidClient != null)
            {
                if(mqttAndroidClient.isConnected()) {
                    mqttAndroidClient.close();
                    //mqttAndroidClient.disconnect();
                    connected = false;
                }

                mqttAndroidClient.setCallback(null);

            }
        }
//        catch (MqttException e)
//        {
//            Log.e("mqtt", "disconnect failed - persistence exception", e);
//        }
        finally
        {
            mqttAndroidClient = null;
        }
    }
}
