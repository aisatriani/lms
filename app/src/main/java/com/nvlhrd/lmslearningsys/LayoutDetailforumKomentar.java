package com.nvlhrd.lmslearningsys;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import com.nvlhrd.lmslearningsys.adapter.ImageLoader;
import com.nvlhrd.lmslearningsys.adapter.JSONParser;
import com.nvlhrd.lmslearningsys.api.ApiGenerator;
import com.nvlhrd.lmslearningsys.api.ServiceForum;
import com.nvlhrd.lmslearningsys.api.pojo.Komentar;
import com.nvlhrd.lmslearningsys.library.DatabaseHandler;
import com.nvlhrd.lmslearningsys.library.SessionManager;
import com.nvlhrd.lmslearningsys.library.UserFunctions;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LayoutDetailforumKomentar extends Activity {


    private ProgressDialog pDialog;
    // url to make request
    static String url;
    JSONArray contacts = null;

    TextView judul, ket;
    static final String JUDUL = "a", JUDUL2 = "b", JUDUL3 = "c", JUDUL4 = "d", JUDUL5 = "e", JUDUL6 = "f", JUDUL7 = "g", JUDUL8 = "h", JUDUL9 = "i", JUDUL10 = "j", JUDUL11 = "k", JUDUL12 = "l";
    static String foto;
    String namenya, des, fotodes;
    String PLACE_LATITUDE, PLACE_LONGITUDE;
    ImageView map, telp;
    LinearLayout aa;
    LinearLayout count_layout;
    int count = 0;
    String deskrip, fotdet1, fotdet2, fotdet3, klan;
    TextView txtdes, txtfotdet1, txtfotdet2;
    ArrayList<String> list = new ArrayList<String>();
    double screenInches;
    ScrollView scroll;

    TextView namaprofile, kelasprofile;
    TextView txtask, txtdeskask;
    String txtjudul, txtdeskripsi, txtidforum, txtiduser, txtnama, txtfoto, txtkelas, txttglupload;
    ProgressDialog progressDialog;

    // JSON Node names
    public static final String TAG_CONTACTS = "info";
    public static final String TAG_IDUSER = "iduser";
    public static final String TAG_NAME = "nama";
    public static final String TAG_KELAS = "kelas";
    public static final String TAG_FOTO = "foto";
    public static final String TAG_KOMENTAR = "komentar";

    String link;
    int current_page = 1;
    // contacts JSONArray
    chatAdapterKomentark adapter2;
    ListView lv;
    TextView name2;
    LinearLayout linlaHeaderProgress;
    LinearLayout tidakhasil;
    ArrayList<HashMap<String, String>> contactList;
    LinearLayout atas;

    SessionManager session;
    HashMap<String, String> user;

    String iduser, idkelas, nilai;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forumdetail);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        lv = (ListView) findViewById(R.id.list);


        session = new SessionManager(getApplicationContext());
        session.checkLogin();
        user = session.getUserDetails();


        Intent in = getIntent();
        txtjudul = in.getStringExtra(JUDUL);
        txtdeskripsi = in.getStringExtra(JUDUL2);
        txtidforum = in.getStringExtra(JUDUL3);
        txtiduser = in.getStringExtra(JUDUL4);
        txtnama = in.getStringExtra(JUDUL5);
        txtfoto = in.getStringExtra(JUDUL6);
        txtkelas = in.getStringExtra(JUDUL7);
        txttglupload = in.getStringExtra(JUDUL8);

        namaprofile = (TextView) findViewById(R.id.namaprofile);
        kelasprofile = (TextView) findViewById(R.id.kelasprofile);

        namaprofile.setText("" + txtnama);
        kelasprofile.setText("" + txtkelas);

        txtask = (TextView) findViewById(R.id.txtask);
        txtdeskask = (TextView) findViewById(R.id.desask);

        txtask.setText("" + txtjudul);
        txtdeskask.setText("" + txtdeskripsi);

        LayoutDetailforumKomentar.this.runOnUiThread(new Runnable() {
            public void run() {
                ImageView image = (ImageView) findViewById(R.id.foto);
                ImageLoader imgLoader = new ImageLoader(LayoutDetailforumKomentar.this.getApplicationContext());
                imgLoader.DisplayImage(txtfoto, image);
            }
        });

        contactList = new ArrayList<HashMap<String, String>>();
        // Loading INBOX in Background Thread
        tidakhasil = (LinearLayout) findViewById(R.id.noitem);
        tidakhasil.setVisibility(View.GONE);

        //url = "http://lms.novalherdinata.net/komentarparsing.php?nama="+txtidforum+"&page=1";

        //new LoadInbox().execute();
        loadKomentar();

    }

    private void loadKomentar() {
        linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
        linlaHeaderProgress.setVisibility(View.VISIBLE);

        ServiceForum service = ApiGenerator.createService(ServiceForum.class);
        Call<List<Komentar>> call = service.getKomentar(txtidforum);
        call.enqueue(new Callback<List<Komentar>>() {
            @Override
            public void onResponse(Call<List<Komentar>> call, Response<List<Komentar>> response) {
                linlaHeaderProgress.setVisibility(View.GONE);
                List<Komentar> list = response.body();
                for (Komentar komen : list) {
                    HashMap<String, String> map = new HashMap<String, String>();

                    // adding each child node to HashMap key => value
                    // adding each child node to HashMap key => value
                    map.put(TAG_IDUSER, komen.getIduser());
                    map.put(TAG_NAME, komen.getPengguna().getName());
                    map.put(TAG_KELAS, komen.getPengguna().getKelas());
                    map.put(TAG_FOTO, komen.getPengguna().getFoto());
                    map.put(TAG_KOMENTAR, komen.getKomentar());


                    // adding HashList to ArrayList
                    contactList.add(map);
                }

                populateList();
            }

            @Override
            public void onFailure(Call<List<Komentar>> call, Throwable t) {
                linlaHeaderProgress.setVisibility(View.GONE);
            }
        });
    }

    private void populateList() {

        adapter2 = new chatAdapterKomentark(LayoutDetailforumKomentar.this, contactList);
        adapter2.notifyDataSetChanged();
        lv.setAdapter(adapter2);
        lv.invalidateViews();

        lv.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent,
                                    View view, int position, long id) {
                iduser = ((TextView) view.findViewById(R.id.iduser)).getText().toString();
                idkelas = ((TextView) view.findViewById(R.id.namaprofile)).getText().toString();
                if (dashboard.JUDULSTATUS.equalsIgnoreCase("g")) {

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(LayoutDetailforumKomentar.this);

                    // Setting Dialog Title
                    alertDialog.setTitle("Nilai Komentar");

                    // Setting Dialog Message
                    alertDialog.setMessage("Berikan Nilai Sesuai Kualitas Komentar");

                    // Setting Icon to Dialog

                    // Setting Positive "Yes" Button
                    alertDialog.setPositiveButton("BAGUS", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            nilai = "Bagus";
                            // Write your code here to invoke YES event
                            progressDialog = ProgressDialog.show(LayoutDetailforumKomentar.this, "", "Proses.....", false);
                            Thread thread = new Thread(new Runnable() {
                                public void run() {
                                    doinput();
                                    runOnUiThread(new Runnable() {
                                        public void run() {
                                            if (progressDialog.isShowing())
                                                progressDialog.dismiss();
                                            Toast.makeText(LayoutDetailforumKomentar.this, "Berhasil di tambahkan", Toast.LENGTH_SHORT).show();

                                        }
                                    });
                                }
                            });
                            thread.start();
                        }
                    });

                    // Setting Negative "NO" Button
                    alertDialog.setNegativeButton("CUKUP", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Write your code here to invoke NO event

                            nilai = "CUKUP";
                            progressDialog = ProgressDialog.show(LayoutDetailforumKomentar.this, "", "Proses.....", false);
                            Thread thread = new Thread(new Runnable() {
                                public void run() {
                                    doinput();
                                    runOnUiThread(new Runnable() {
                                        public void run() {
                                            if (progressDialog.isShowing())
                                                progressDialog.dismiss();
                                            Toast.makeText(LayoutDetailforumKomentar.this, "Berhasil di tambahkan", Toast.LENGTH_SHORT).show();

                                        }
                                    });
                                }
                            });
                            thread.start();
                        }
                    });

                    // Showing Alert Message
                    alertDialog.show();
                }
            }
        });


    }

    class LoadInbox extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
            linlaHeaderProgress.setVisibility(View.VISIBLE);
        }

        /**
         * getting Inbox JSON
         */
        protected String doInBackground(String... args) {
            // Creating JSON Parser instance
            JSONParser jParser = new JSONParser();

            // getting JSON string from URL
            JSONObject json = jParser.getJSONFromUrl(url);

            try {
                // Getting Array of Contacts
                contacts = json.getJSONArray(TAG_CONTACTS);

                // looping through All Contacts
                for (int i = 0; i < contacts.length(); i++) {
                    JSONObject c = contacts.getJSONObject(i);

                    // Storing each json item in variable
                    String iduser = c.getString(TAG_IDUSER);
                    String nama = c.getString(TAG_NAME);
                    String kelas = c.getString(TAG_KELAS);
                    String foto = c.getString(TAG_FOTO);
                    String komentar = c.getString(TAG_KOMENTAR);

                    foto = foto.replaceAll(" ", "%20");
                    // creating new HashMap
                    HashMap<String, String> map = new HashMap<String, String>();

                    // adding each child node to HashMap key => value
                    // adding each child node to HashMap key => value
                    map.put(TAG_IDUSER, komentar);
                    map.put(TAG_NAME, nama);
                    map.put(TAG_KELAS, kelas);
                    map.put(TAG_FOTO, foto);
                    map.put(TAG_KOMENTAR, komentar);


                    // adding HashList to ArrayList
                    contactList.add(map);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products
            // HIDE THE SPINNER AFTER LOADING FEEDS
            linlaHeaderProgress.setVisibility(View.GONE);

            if (contactList.size() < 1) {
                tidakhasil.setVisibility(View.VISIBLE);
            }
            // LoadMore button
            if (contactList.size() > 9) {

            }

            // updating UI from Background Thread
            LayoutDetailforumKomentar.this.runOnUiThread(new Runnable() {
                public void run() {
                    adapter2 = new chatAdapterKomentark(LayoutDetailforumKomentar.this, contactList);
                    adapter2.notifyDataSetChanged();
                    lv.setAdapter(adapter2);
                    lv.invalidateViews();

                    lv.setOnItemClickListener(new OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent,
                                                View view, int position, long id) {
                            iduser = ((TextView) view.findViewById(R.id.iduser)).getText().toString();
                            idkelas = ((TextView) view.findViewById(R.id.namaprofile)).getText().toString();
                            if (dashboard.JUDULSTATUS.equalsIgnoreCase("g")) {

                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(LayoutDetailforumKomentar.this);

                                // Setting Dialog Title
                                alertDialog.setTitle("Nilai Komentar");

                                // Setting Dialog Message
                                alertDialog.setMessage("Berikan Nilai Sesuai Kualitas Komentar");

                                // Setting Icon to Dialog

                                // Setting Positive "Yes" Button
                                alertDialog.setPositiveButton("BAGUS", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        nilai = "Bagus";
                                        // Write your code here to invoke YES event
                                        progressDialog = ProgressDialog.show(LayoutDetailforumKomentar.this, "", "Proses.....", false);
                                        Thread thread = new Thread(new Runnable() {
                                            public void run() {
                                                doinput();
                                                runOnUiThread(new Runnable() {
                                                    public void run() {
                                                        if (progressDialog.isShowing())
                                                            progressDialog.dismiss();
                                                        Toast.makeText(LayoutDetailforumKomentar.this, "Berhasil di tambahkan", Toast.LENGTH_SHORT).show();

                                                    }
                                                });
                                            }
                                        });
                                        thread.start();
                                    }
                                });

                                // Setting Negative "NO" Button
                                alertDialog.setNegativeButton("CUKUP", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // Write your code here to invoke NO event

                                        nilai = "CUKUP";
                                        progressDialog = ProgressDialog.show(LayoutDetailforumKomentar.this, "", "Proses.....", false);
                                        Thread thread = new Thread(new Runnable() {
                                            public void run() {
                                                doinput();
                                                runOnUiThread(new Runnable() {
                                                    public void run() {
                                                        if (progressDialog.isShowing())
                                                            progressDialog.dismiss();
                                                        Toast.makeText(LayoutDetailforumKomentar.this, "Berhasil di tambahkan", Toast.LENGTH_SHORT).show();

                                                    }
                                                });
                                            }
                                        });
                                        thread.start();
                                    }
                                });

                                // Showing Alert Message
                                alertDialog.show();
                            }
                        }
                    });

                }
            });
        }
    }

    private void doinput() {
        // TODO Auto-generated method stub

        String idguru = user.get(SessionManager.KEY_NAME);

        String input_data = "http://lms.novalherdinata.net/inputnilaikomentar.php"; //URL website anda dengan file insert.php yang telah dibuat

        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(input_data);
        ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();
        param.add(new BasicNameValuePair("idtugas", "Komentar"));
        param.add(new BasicNameValuePair("idguru", idguru));
        param.add(new BasicNameValuePair("siswa", iduser));
        param.add(new BasicNameValuePair("kelas", idkelas));
        param.add(new BasicNameValuePair("nilai", nilai));
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(param));
            HttpResponse httpRespose = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpRespose.getEntity();
            InputStream in = httpEntity.getContent();
            BufferedReader read = new BufferedReader(new InputStreamReader(in));

            String isi = "";
            String baris = "";

            while ((baris = read.readLine()) != null) {
                isi += baris;
            }

            //Jika isi tidak sama dengan "null " maka akan tampil Toast "Berhasil" sebaliknya akan tampil "Gagal"
            if (!isi.equals("null")) {
            } else {
            }

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private class loadMoreListView extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            // Showing progress dialog before sending http request
            pDialog = new ProgressDialog(LayoutDetailforumKomentar.this);
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        protected Void doInBackground(Void... unused) {
            // Creating JSON Parser instance
            current_page += 1;

            url = "http://lms.novalherdinata.net/komentarparsing.php?nama=" + forumkomentar.fid + "&page="
                    + current_page;
            JSONParser jParser = new JSONParser();

            // getting JSON string from URL
            JSONObject json = jParser.getJSONFromUrl(url);

            try {
                // Getting Array of Contacts
                contacts = json.getJSONArray(TAG_CONTACTS);

                // looping through All Contacts
                for (int i = 0; i < contacts.length(); i++) {
                    JSONObject c = contacts.getJSONObject(i);
                    // Storing each json item in variable
                    // Storing each json item in variable
                    String nama = c.getString(TAG_NAME);
                    String kelas = c.getString(TAG_KELAS);
                    String foto = c.getString(TAG_FOTO);
                    String komentar = c.getString(TAG_KOMENTAR);

                    foto = foto.replaceAll(" ", "%20");
                    // creating new HashMap
                    HashMap<String, String> map = new HashMap<String, String>();

                    // adding each child node to HashMap key => value
                    // adding each child node to HashMap key => value
                    map.put(TAG_NAME, nama);
                    map.put(TAG_KELAS, kelas);
                    map.put(TAG_FOTO, foto);
                    map.put(TAG_KOMENTAR, komentar);

                    contactList.add(map);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(Void unused) {
            // closing progress dialog
            pDialog.dismiss();

            // updating UI from Background Thread
            LayoutDetailforumKomentar.this.runOnUiThread(new Runnable() {
                public void run() {
                    int currentPosition = lv.getFirstVisiblePosition();
                    adapter2 = new chatAdapterKomentark(LayoutDetailforumKomentar.this, contactList);
                    adapter2.notifyDataSetChanged();
                    lv.setAdapter(adapter2);
                    lv.invalidateViews();
                    lv.setSelectionFromTop(currentPosition + 1, 0);
                    lv.setOnItemClickListener(new OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent,
                                                View view, int position, long id) {

                        }
                    });
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.forumkomentar, menu);

        MenuItem tambah = (MenuItem) menu.findItem(R.id.action_tambah);

        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title

        // Handle action bar actions click
        switch (item.getItemId()) {
            case R.id.action_tambah:
                Intent in = new Intent(this, inputforumkomentar.class);
                in.putExtra(JUDUL, txtjudul);
                in.putExtra(JUDUL2, txtdeskripsi);
                in.putExtra(JUDUL3, txtidforum);
                in.putExtra(JUDUL4, txtiduser);
                in.putExtra(JUDUL5, txtnama);
                in.putExtra(JUDUL6, txtfoto);
                in.putExtra(JUDUL7, txtkelas);
                in.putExtra(JUDUL8, txttglupload);
                startActivity(in);

                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // do something on back.
            Intent in = new Intent(LayoutDetailforumKomentar.this, forumactivity.class);
            in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            startActivity(in);
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

}
