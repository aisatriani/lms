package com.nvlhrd.lmslearningsys;

import java.util.ArrayList;
import java.util.HashMap;


import com.nvlhrd.lmslearningsys.adapter.ImageLoader;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class chatAdapterTugas extends BaseAdapter {
    
    private Activity activity;
    private ArrayList<HashMap<String, String>> data;
    private static LayoutInflater inflater=null;
    public ImageLoader imageLoader; 
    
    public chatAdapterTugas(Activity a,  ArrayList<HashMap<String, String>> d) {
        activity = a;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader=new ImageLoader(activity.getApplicationContext());
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }
    public void remove() {
        data.clear();
      }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
//        	adapter = new SimpleAdapter(
//					chatutama.this, inboxList,
//					R.layout.inbox_list_item, new String[] { TAG_NAME, TAG_MESS },
//					new int[] { R.id.from, R.id.subject});
        	 vi = inflater.inflate(R.layout.list_rowtugas, null);

        TextView judul = (TextView)vi.findViewById(R.id.title);
        TextView deskripsi = (TextView)vi.findViewById(R.id.deskripsi);
        TextView file = (TextView)vi.findViewById(R.id.downloadfile);
        TextView namaguru = (TextView)vi.findViewById(R.id.namagurutugas);
        TextView tgl = (TextView)vi.findViewById(R.id.tglupload);
        TextView idtugas = (TextView)vi.findViewById(R.id.idtugas);
        TextView foto = (TextView)vi.findViewById(R.id.foto);
        TextView idguru = (TextView)vi.findViewById(R.id.idguru);
        
        
        //hasmap
        HashMap<String, String> song = new HashMap<String, String>();
        song = data.get(position);
        
        // Setting all values in listview
        judul.setText(song.get(listtugas.TAG_NAME));
        deskripsi.setText(song.get(listtugas.TAG_DESKRIPSI));
        file.setText(song.get(listtugas.TAG_FILE));
        namaguru.setText(song.get(listtugas.TAG_NAMA));
        tgl.setText(song.get(listtugas.TAG_TGLUPLOAD));
        idtugas.setText(song.get(listtugas.TAG_IDTUGAS));
        foto.setText(song.get(listtugas.TAG_FOTO));
        idguru.setText(song.get(listtugas.TAG_IDGURU));
        	
        return vi;
    }
}