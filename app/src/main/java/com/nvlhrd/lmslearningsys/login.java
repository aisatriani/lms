package com.nvlhrd.lmslearningsys;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.nvlhrd.lmslearningsys.adapter.JSONParser;
import com.nvlhrd.lmslearningsys.api.ApiGenerator;
import com.nvlhrd.lmslearningsys.api.ServicePengguna;
import com.nvlhrd.lmslearningsys.api.pojo.Pengguna;
import com.nvlhrd.lmslearningsys.library.SessionManager;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class login extends Activity {

    EditText user, pass;
    Button btnlogin;
    SessionManager session;

    private ProgressDialog pDialog;
    ArrayList<HashMap<String, String>> contactList;
    // url to make request
    static String url;
    JSONArray contacts = null;

    //pref
    // JSON Response node names
    String id, nama, level, foto, idserver;

    public static final String TAG_CONTACTS = "info";
    public static final String TAG_IDUSER = "iduser";

    static final String JUDUL11 = "a", JUDUL12 = "b", JUDUL13 = "c", JUDUL14 = "d", JUDUL15 = "e";

    // flag for Internet connection status
    Boolean isInternetPresent = false;

    // Connection detector class
    ConnectionDetector cd;
    Button regisSiswa, regisGuru;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loginac);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        session = new SessionManager(login.this.getApplicationContext());


        cd = new ConnectionDetector(getApplicationContext());

        isInternetPresent = cd.isConnectingToInternet();

        // check for Internet status
        if (isInternetPresent) {
            // Internet Connection is Present

        } else {
            Toast.makeText(login.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

        if (session.isLoggedIn()) {
            Intent in = new Intent(login.this, dashboard.class);
            startActivity(in);
            finish();
        }

        user = (EditText) findViewById(R.id.loginuser);
        pass = (EditText) findViewById(R.id.loginpass);

        btnlogin = (Button) findViewById(R.id.btnlogin);
        regisGuru = (Button) findViewById(R.id.registerguru);
        regisSiswa = (Button) findViewById(R.id.registersiswa);

        regisGuru.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent in = new Intent(login.this, RegisterActivity.class);
                startActivity(in);
            }
        });

        regisSiswa.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent in = new Intent(login.this, RegisterActivityStudent.class);
                startActivity(in);
            }
        });


        btnlogin.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (user.getText().toString().equalsIgnoreCase("")) {
                    user.setError("Field can't be blank.");
                }
                if (pass.getText().toString().equalsIgnoreCase("")) {
                    pass.setError("Field can't be blank.");
                }

                if (!user.getText().toString().equalsIgnoreCase("") && !pass.getText().toString().equalsIgnoreCase("")) {
                    String suser = user.getText().toString();
                    String spass = pass.getText().toString();

                    isInternetPresent = cd.isConnectingToInternet();

                    // check for Internet status
                    if (isInternetPresent) {
                        // Internet Connection is Present

                        submitLogin();
                    } else {
                        Toast.makeText(login.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

    }

    private void submitLogin() {
        pDialog = new ProgressDialog(
                login.this);
        pDialog.setMessage("Please wait..");
        pDialog.setIndeterminate(true);
        pDialog.setCancelable(false);
        pDialog.show();

        ServicePengguna servicePengguna = ApiGenerator.createService(ServicePengguna.class);
        Call<Pengguna> call = servicePengguna.login(user.getText().toString(), pass.getText().toString());
        call.enqueue(new Callback<Pengguna>() {
            @Override
            public void onResponse(Call<Pengguna> call, Response<Pengguna> response) {
                pDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getId() != 0) {
                        session.createLoginSession(response.body().getName(), response.body().getEmail());
                        session.storePengguna(response.body());
                        Intent in = new Intent(login.this, dashboard.class);
                        startActivity(in);
                        login.this.finish();
                    } else {
                        Toast.makeText(login.this, getString(R.string.invalid_login), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(login.this, getString(R.string.invalid_login), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Pengguna> call, Throwable throwable) {
                pDialog.dismiss();
                Toast.makeText(login.this, getString(R.string.error_connection), Toast.LENGTH_SHORT).show();

            }
        });
    }


//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            // do something on back.
//            Intent intent = new Intent(Intent.ACTION_MAIN);
//            intent.addCategory(Intent.CATEGORY_HOME);
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//***Change Here***
//            startActivity(intent);
//            finish();
//            System.exit(0);
//            return true;
//        }
//
//        return super.onKeyDown(keyCode, event);
//    }


}
