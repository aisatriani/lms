package com.nvlhrd.lmslearningsys;

import java.util.ArrayList;
import java.util.HashMap;

import com.nvlhrd.lmslearningsys.adapter.ImageLoader;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class BeritaAdapter extends BaseAdapter {
    
    private Activity activity;
    private ArrayList<HashMap<String, String>> data;
    private static LayoutInflater inflater=null;
    public ImageLoader imageLoader; 
    
    public BeritaAdapter(Activity a,  ArrayList<HashMap<String, String>> d) {
        activity = a;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader=new ImageLoader(activity.getApplicationContext());
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }
    
    public void remove() {
        data.clear();
      }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
//        	adapter = new SimpleAdapter(
//					chatutama.this, inboxList,
//					R.layout.inbox_list_item, new String[] { TAG_NAME, TAG_MESS },
//					new int[] { R.id.from, R.id.subject});
        	 vi = inflater.inflate(R.layout.list_berita, null);

        TextView judul = (TextView)vi.findViewById(R.id.title);
        TextView nama = (TextView)vi.findViewById(R.id.namaguru);
        TextView des = (TextView)vi.findViewById(R.id.deskripsi);
        TextView idberita = (TextView)vi.findViewById(R.id.idberita);
        TextView tgl = (TextView)vi.findViewById(R.id.tglupload);
        
        //hasmap
        HashMap<String, String> song = new HashMap<String, String>();
        song = data.get(position);
        
        // Setting all values in listview
        judul.setText(song.get(listberita.TAG_JUDUL));
        nama.setText(song.get(listberita.TAG_NAMA));
        des.setText(song.get(listberita.TAG_DESKRIPSI));
        idberita.setText(song.get(listberita.TAG_IDBERITA));
        tgl.setText(song.get(listberita.TAG_TGLUPLOAD));
        	
        return vi;
    }
}