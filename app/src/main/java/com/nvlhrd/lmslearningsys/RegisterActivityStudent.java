/**
 * Author: Ravi Tamada
 * URL: www.androidhive.info
 * twitter: http://twitter.com/ravitamada
 * */
package com.nvlhrd.lmslearningsys;




import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;


import com.nvlhrd.lmslearningsys.library.DatabaseHandler;
import com.nvlhrd.lmslearningsys.library.UserFunctions;



import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class RegisterActivityStudent extends Activity {
	Button btnRegister;
	Button btnLinkToLogin;
	EditText inputFullName;
	EditText inputEmail;
	EditText inputPassword;
	EditText inputPassword2;
	TextView registerErrorMsg;
	
	// JSON Response node names
	private static String KEY_SUCCESS = "success";
	private static String KEY_ERROR = "error";
	private static String KEY_ERROR_MSG = "error_msg";
	private static String KEY_UID = "uid";
	private static String KEY_NAME = "name";
	private static String KEY_EMAIL = "email";
	private static String KEY_CREATED_AT = "created_at";

	//spiner
	 
	 String[] kelas = {   
			 	"X ANKIM 1"
		        ,"X ANKIM 2"
		        ,"XI ANKIM 1"
		        ,"XI ANKIM 2"
		        ,"XII ANKIM 1"
		        ,"XII ANKIM 2"
		        ,"X TKJ 1"
		        ,"X TKJ 2"
		        ,"X TKJ 3"
		        ,"XI TKJ 1"
		        ,"XI TKJ 2"
		        ,"XII TKJ 1"
		        ,"XII TKJ 2"
		        ,"XII TKJ 3"
		        ,"X RPL 1"
		        ,"X RPL 2"
		        ,"XI RPL 1"
		        ,"XI RPL 2"
		        ,"XII RPL 1"
		        ,"XII RPL 2"
		        ,"X MM 1"
		        ,"X MM 2"
		        ,"XI MM 1"
		        ,"XI MM 2"
		        ,"XII MM 1"
		        ,"XII MM 2"
		        ,"X TP3R 1"
		        ,"XI TP3R 1"
		        ,"XII TP3R 1"
		        ,"X UPW 1"
		        ,"XI UPW 1"
		        ,"XI UPW 2"
		        ,"XII UPW 1"
		        ,"XII UPW 2"
		        ,"X AK 1"
		        ,"X AK 2"
		        ,"X AK 3"
		        ,"X AK 4"
		        ,"X AK 5"
		        ,"XI AK 1"
		        ,"XI AK 2"
		        ,"XI AK 3"
		        ,"XI AK 4"
		        ,"XI AK 5"
		        ,"XII AK 1"
		        ,"XII AK 2"
		        ,"XII AK 3"
		        ,"XII AK 4"
		        ,"XII AK 5"
		        ,"XII AK 6"
		        ,"X AP 1"
		        ,"X AP 2"
		        ,"X AP 3"
		        ,"X AP 4"
		        ,"X AP 5"
		        ,"X AP 6"
		        ,"X AP 7"
		        ,"XI AP 1"
		        ,"XI AP 2"
		        ,"XI AP 3"
		        ,"XI AP 4"
		        ,"XI AP 5"
		        ,"XI AP 6"
		        ,"XII AP 1"
		        ,"XII AP 2"
		        ,"XII AP 3"
		        ,"XII AP 4"
		        ,"XII AP 5"
		        ,"XII AP 6"
		        ,"X PEMS 1"
		        ,"X PEMS 2"
		        ,"XI PEMS 1"
		        ,"XII PEMS 1"
		        ,"XII PEMS 2"};
	 Spinner spinner;
	 ProgressDialog progressDialog;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		 StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	        StrictMode.setThreadPolicy(policy);
		setContentView(R.layout.activity_registersiswa);

		// Importing all assets like buttons, text fields
		inputFullName = (EditText) findViewById(R.id.registerName);
		inputEmail = (EditText) findViewById(R.id.registerEmail);
		inputPassword = (EditText) findViewById(R.id.registerPassword);
		inputPassword2 = (EditText) findViewById(R.id.registerPasswordconf);
		btnRegister = (Button) findViewById(R.id.btnRegister);
		btnLinkToLogin = (Button) findViewById(R.id.btnLinkToLoginScreen);
		registerErrorMsg = (TextView) findViewById(R.id.register_error);
		spinner = (Spinner) findViewById(R.id.spinnerkelas);
		
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, kelas);
		
		// Drop down layout style - list view with radio button
dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
spinner.setAdapter(dataAdapter);
		
		// Register Button Click event
		btnRegister.setOnClickListener(new View.OnClickListener() {			
			public void onClick(View view) {
				
				
				String emailvalidate = inputEmail.getText().toString().trim();

				String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

				if(inputFullName.getText().toString().equalsIgnoreCase("")){
					registerErrorMsg.setText("Nama tidak boleh kosong!");
		}else if(inputEmail.getText().toString().equalsIgnoreCase("")){
			registerErrorMsg.setText("Email tidak boleh kosong!");
		}else if(inputPassword.getText().toString().equalsIgnoreCase("")){
			registerErrorMsg.setText("Password tidak boleh kosong!");
		}else if(!emailvalidate.matches(emailPattern)){
			registerErrorMsg.setText("Format email salah");
		}else if(inputPassword.getText().toString().length() <6){
			registerErrorMsg.setText("Password harus lebih dari 6 karakter");
		}
		else if(!(inputPassword.getText().toString().equals(inputPassword2.getText().toString()))){
			registerErrorMsg.setText("Password harus sama");
		}
				
				if(inputPassword.getText().toString().equals(inputPassword2.getText().toString()) && !inputFullName.getText().toString().equalsIgnoreCase("") &&!inputEmail.getText().toString().equalsIgnoreCase("")&&!inputPassword.getText().toString().equalsIgnoreCase("")&& !(inputPassword.getText().toString().length() <6) && emailvalidate.matches(emailPattern)){
					progressDialog = ProgressDialog.show(RegisterActivityStudent.this, "", "Proses.....", false);
                    Thread thread=new Thread(new Runnable(){
                           public void run(){
                        		doinput();
                               runOnUiThread(new Runnable(){
                                   public void run() {
                                       if(progressDialog.isShowing())
                                           progressDialog.dismiss();
                                       Toast.makeText(RegisterActivityStudent.this, "Berhasil di tambahkan", Toast.LENGTH_SHORT).show();
//                                       Intent intent2 = new Intent(up.this, MainActivity.class);
//                           			startActivity(intent2);
                                   }
                               });
                           }
                   });
                   thread.start();
                   Intent in = new Intent(RegisterActivityStudent.this, login.class);
   				startActivity(in);
				}

			}
		});

		// Link to Login Screen
		btnLinkToLogin.setOnClickListener(new View.OnClickListener() {

			public void onClick(View view) {
				Intent i = new Intent(getApplicationContext(),
						login.class);
				startActivity(i);
				// Close Registration View
				finish();
			}
		});
	}
	
	private void doinput() {
		// TODO Auto-generated method stub

	  String a = kelas[spinner.getSelectedItemPosition()];
	
		String name = inputFullName.getText().toString();
		String email = inputEmail.getText().toString();
		String password = inputPassword.getText().toString();
	  	String b = "s";
	  	
		
		// String input_data= "http://lms.novalherdinata.net/registerguru.php"; //URL website anda dengan file insert.php yang telah dibuat
         String input_data = Config.URL_API + "register";

         HttpClient httpClient = new DefaultHttpClient();  
         HttpPost httpPost = new HttpPost(input_data);  
         ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();  
         param.add(new BasicNameValuePair("name", name));
         param.add(new BasicNameValuePair("email", email)); 
         param.add(new BasicNameValuePair("password", password)); 
         param.add(new BasicNameValuePair("kelas", a)); 
         param.add(new BasicNameValuePair("status", b)); 
         try {  
              httpPost.setEntity(new UrlEncodedFormEntity(param));  
              HttpResponse httpRespose = httpClient.execute(httpPost);  
             HttpEntity httpEntity = httpRespose.getEntity();  
             InputStream in = httpEntity.getContent();  
             BufferedReader read = new BufferedReader(new InputStreamReader(in));  
            
             String isi= "";  
             String baris= "";  
            
             while((baris = read.readLine())!=null){  
                isi+= baris;  
             }

			 System.out.println(isi);
               
             //Jika isi tidak sama dengan "null " maka akan tampil Toast "Berhasil" sebaliknya akan tampil "Gagal"  
             if(!isi.equals("null")){                    
             }else{  
             }  
            
       } catch (ClientProtocolException e) {  
          // TODO Auto-generated catch block  
          e.printStackTrace();  
       } catch (IOException e) {  
          // TODO Auto-generated catch block  
          e.printStackTrace();  
       }  
	  	
    	
	}
}
