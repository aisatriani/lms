package com.nvlhrd.lmslearningsys;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import com.ipaulpro.afilechooser.utils.FileUtils;
import com.nvlhrd.lmslearningsys.adapter.ImageLoader;
import com.nvlhrd.lmslearningsys.adapter.JSONParser;
import com.nvlhrd.lmslearningsys.api.ApiGenerator;
import com.nvlhrd.lmslearningsys.api.ServicePengguna;
import com.nvlhrd.lmslearningsys.api.pojo.Pengguna;
import com.nvlhrd.lmslearningsys.library.DialogInfo;
import com.nvlhrd.lmslearningsys.library.SessionManager;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class editprofile extends Activity {

    ImageView image;
    TextView nama, email, kelas;
    EditText pass1, pass2;
    Button btnedit;
    SessionManager session;
    HashMap<String, String> user;
    private static final int SELECT_FILE1 = 1;
    String selectedPath1 = "NONE";
    HttpEntity resEntity;

    //json

    public static final String TAG_CONTACTS = "info";
    public static final String TAG_EMAIL = "email";
    public static final String TAG_NAMA = "nama";
    public static final String TAG_KELAS = "kelas";
    public static final String TAG_FOTO = "foto";

    ArrayList<HashMap<String, String>> contactList;
    // url to make request
    static String url;
    LinearLayout linlaHeaderProgress;
    String semail, snama, skelas, sfoto;
    JSONArray contacts = null;
    ImageLoader imageloader;
    ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.layout_profil);
        session = new SessionManager(getApplicationContext());
        session.checkLogin();
        user = session.getUserDetails();

        image = (ImageView) findViewById(R.id.foto);
        nama = (TextView) findViewById(R.id.nama);
        email = (TextView) findViewById(R.id.email);
        kelas = (TextView) findViewById(R.id.kelas);

        pass1 = (EditText) findViewById(R.id.pass1);
        pass2 = (EditText) findViewById(R.id.pass2);

        btnedit = (Button) findViewById(R.id.btneditpass);

        contactList = new ArrayList<HashMap<String, String>>();
        // Loading INBOX in Background Thread

        url = "http://lms.novalherdinata.net/getuser.php?nama=" + user.get(SessionManager.KEY_NAME);

        //new LoadInbox().execute();
        loadUserdata();

        nama.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                final Dialog dialog = new Dialog(editprofile.this);
                dialog.setContentView(R.layout.dialogrubahnama);
                dialog.setCancelable(false);
                // set the custom dialog components - text, image and button
                dialog.setTitle("Edit Nama Anda");

                Button yes = (Button) dialog.findViewById(R.id.btnYes);
                Button no = (Button) dialog.findViewById(R.id.btnNo);
                // if button is clicked, close the custom dialog
                yes.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EditText fnama = (EditText) dialog.findViewById(R.id.namadialog);
                        String snama = fnama.getText().toString();
                        nama.setText("" + snama);
                        //String input_data = "http://lms.novalherdinata.net/editprofilenama.php?idiklan=" + user.get(SessionManager.KEY_NAME); //URL website anda dengan file insert.php yang telah dibuat
                        String input_data = Config.URL_API + "pengguna/" + user.get(SessionManager.KEY_EMAIL) + "/updatename";

                        HttpClient httpClient = new DefaultHttpClient();
                        HttpPost httpPost = new HttpPost(input_data);
                        ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();
                        param.add(new BasicNameValuePair("name", snama));
                        try {
                            httpPost.setEntity(new UrlEncodedFormEntity(param));
                            HttpResponse httpRespose = httpClient.execute(httpPost);
                            HttpEntity httpEntity = httpRespose.getEntity();
                            InputStream in = httpEntity.getContent();
                            BufferedReader read = new BufferedReader(new InputStreamReader(in));

                            String isi = "";
                            String baris = "";

                            while ((baris = read.readLine()) != null) {
                                isi += baris;
                            }

                            //Jika isi tidak sama dengan "null " maka akan tampil Toast "Berhasil" sebaliknya akan tampil "Gagal"
                            if (!isi.equals("null")) {
                            } else {
                            }


                        } catch (ClientProtocolException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                    }
                });
                no.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        image.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                openGallery(SELECT_FILE1);
            }
        });

        btnedit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (pass1.getText().toString().equals("")) {
                    pass1.setText("Tidak boleh kosong");
                }
                if (pass2.getText().toString().equals("")) {
                    pass2.setText("Tidak boleh kosong");
                }
                if (pass1.getText().toString().equals(pass2.getText().toString().equals(""))) {
                    Toast.makeText(getApplicationContext(), "Password Tidak Sama", Toast.LENGTH_LONG).show();
                }
                if (!pass1.getText().toString().equals("") && !pass2.getText().toString().equals("") && !pass1.getText().toString().equals(pass2.getText().toString().equals(""))) {
                    String iduse = user.get(SessionManager.KEY_NAME);

                    //String input_data = "http://lms.novalherdinata.net/updatepassword.php?iduser=" + iduse; //URL website anda dengan file insert.php yang telah dibuat
                    String input_data = Config.URL_API + "pengguna/" + user.get(SessionManager.KEY_EMAIL) + "/updatepass";


                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httpPost = new HttpPost(input_data);
                    ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();
                    param.add(new BasicNameValuePair("pass", pass2.getText().toString()));
                    try {
                        httpPost.setEntity(new UrlEncodedFormEntity(param));
                        HttpResponse httpRespose = httpClient.execute(httpPost);
                        HttpEntity httpEntity = httpRespose.getEntity();
                        InputStream in = httpEntity.getContent();
                        BufferedReader read = new BufferedReader(new InputStreamReader(in));

                        String isi = "";
                        String baris = "";

                        while ((baris = read.readLine()) != null) {
                            isi += baris;
                        }

                        //Jika isi tidak sama dengan "null " maka akan tampil Toast "Berhasil" sebaliknya akan tampil "Gagal"
                        if (!isi.equals("null")) {
                        } else {
                        }

                    } catch (ClientProtocolException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    pass1.setText("");
                    pass2.setText("");
                    Toast.makeText(getApplicationContext(), "Berhasil terupdate", Toast.LENGTH_LONG).show();
                }

            }
        });

    }

    private void loadUserdata() {

        linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
        linlaHeaderProgress.setVisibility(View.VISIBLE);

        ServicePengguna service = ApiGenerator.createService(ServicePengguna.class);
        Call<Pengguna> call = service.getPenggunaByEmail(user.get(SessionManager.KEY_EMAIL));
        call.enqueue(new Callback<Pengguna>() {
            @Override
            public void onResponse(Call<Pengguna> call, Response<Pengguna> response) {
                linlaHeaderProgress.setVisibility(View.GONE);
                Pengguna p = response.body();
                snama = p.getName();
                semail = p.getEmail();
                skelas = p.getKelas();
                sfoto = p.getFoto();

                nama.setText("" + snama);
                email.setText("" + semail);
                kelas.setText("" + skelas);

                runOnUiThread(new Runnable() {
                    public void run() {
                        ImageLoader imgLoader = new ImageLoader(getApplicationContext());
                        imgLoader.DisplayImage(sfoto, image);
                    }
                });
            }

            @Override
            public void onFailure(Call<Pengguna> call, Throwable t) {
                linlaHeaderProgress.setVisibility(View.GONE);
                DialogInfo.showErrorDialog(editprofile.this, getString(R.string.error_connection));
            }
        });

    }

    public void openGallery(int req_code) {

//        Intent intent = new Intent();
//        intent.setType("image/*");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(Intent.createChooser(intent, "Select file to upload "), req_code);

        Intent getContentIntent = FileUtils.createGetContentIntent();
        getContentIntent.setType("image/*");
        Intent intent = Intent.createChooser(getContentIntent, "Select a file");

        startActivityForResult(intent, req_code);
    }


    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void doFileUpload() {

        File file1 = new File(selectedPath1);
        //String urlString = "http://lms.novalherdinata.net/editfotoprofi.php?idiklan=" + user.get(SessionManager.KEY_NAME);
        String urlString = Config.URL_API + "pengguna/"+user.get(SessionManager.KEY_EMAIL)+"/updateprofile";
        try {
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(urlString);
            FileBody bin1 = new FileBody(file1);
            MultipartEntity reqEntity = new MultipartEntity();
            reqEntity.addPart("uploadedfile1", bin1);
            reqEntity.addPart("user", new StringBody("User"));
            post.setEntity(reqEntity);
            HttpResponse response = client.execute(post);
            resEntity = response.getEntity();
            final String response_str = EntityUtils.toString(resEntity);
            if (resEntity != null) {
                Log.i("RESPONSE", response_str);
                runOnUiThread(new Runnable() {
                    public void run() {
                        try {
                            Toast.makeText(getApplicationContext(), "Upload Complete. Check the server uploads directory.", Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        } catch (Exception ex) {
            Log.e("Debug", "error: " + ex.getMessage(), ex);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {
            Uri selectedImageUri = data.getData();
            if (requestCode == SELECT_FILE1) {

                final Uri uri = data.getData();

                String path = FileUtils.getPath(this, uri);
                selectedPath1 = path;

                // Alternatively, use FileUtils.getFile(Context, Uri)
                if (path != null && FileUtils.isLocal(path)) {
                    File file = new File(path);
                }
                //kirimfoto

                progressDialog = ProgressDialog.show(editprofile.this, "", "Uploading files to server.....", false);
                Thread thread = new Thread(new Runnable() {
                    public void run() {
                        doFileUpload();
                        runOnUiThread(new Runnable() {
                            public void run() {
                                if (progressDialog.isShowing())
                                    progressDialog.dismiss();
                                Bitmap bitmap = BitmapFactory.decodeFile(selectedPath1);
                                image.setImageBitmap(bitmap);
                            }
                        });
                    }
                });
                thread.start();
            }
        }
    }

    class LoadInbox extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
            linlaHeaderProgress.setVisibility(View.VISIBLE);
        }

        /**
         * getting Inbox JSON
         */
        protected String doInBackground(String... args) {
            // Creating JSON Parser instance
            JSONParser jParser = new JSONParser();

            // getting JSON string from URL
            JSONObject json = jParser.getJSONFromUrl(url);

            try {
                // Getting Array of Contacts
                contacts = json.getJSONArray(TAG_CONTACTS);

                // looping through All Contacts
                for (int i = 0; i < contacts.length(); i++) {
                    JSONObject c = contacts.getJSONObject(i);

                    // Storing each json item in variable
                    snama = c.getString(TAG_NAMA);
                    semail = c.getString(TAG_EMAIL);
                    skelas = c.getString(TAG_KELAS);
                    sfoto = c.getString(TAG_FOTO);

                    sfoto = sfoto.replaceAll(" ", "%20");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products
            linlaHeaderProgress.setVisibility(View.GONE);

            nama.setText("" + snama);
            email.setText("" + semail);
            kelas.setText("" + skelas);

            runOnUiThread(new Runnable() {
                public void run() {
                    ImageLoader imgLoader = new ImageLoader(getApplicationContext());
                    imgLoader.DisplayImage(sfoto, image);
                }
            });


        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {

        }
        return super.onKeyDown(keyCode, event);
    }


}
