package com.nvlhrd.lmslearningsys;


import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import com.nvlhrd.lmslearningsys.adapter.JSONParser;
import com.nvlhrd.lmslearningsys.api.ApiGenerator;
import com.nvlhrd.lmslearningsys.api.ServiceBerita;
import com.nvlhrd.lmslearningsys.api.pojo.Berita;
import com.nvlhrd.lmslearningsys.library.DialogInfo;
import com.nvlhrd.lmslearningsys.library.SessionManager;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class listberita extends Activity {

    ArrayList<HashMap<String, String>> contactList;
    // url to make request
    static String url;
    static String NAMA;
    static final String JUDUL = "a", JUDUL2 = "b", JUDUL3 = "c", JUDUL4 = "d",
            JUDUL5 = "e", JUDUL6 = "f", JUDUL7 = "g", JUDUL8 = "h",
            JUDUL9 = "i", JUDUL10 = "j", JUDUL11 = "k", JUDUL12 = "l";

    // JSON Node names
    public static final String TAG_CONTACTS = "info";
    public static final String TAG_JUDUL = "judul";
    public static final String TAG_DESKRIPSI = "berita";
    public static final String TAG_NAMA = "idguru";
    public static final String TAG_TGLUPLOAD = "tgl";
    public static final String TAG_IDBERITA = "idberita";
    String link;
    int current_page = 1;
    // contacts JSONArray
    JSONArray contacts = null;
    BeritaAdapter adapter2;
    ListView lv;
    TextView name2;
    LinearLayout linlaHeaderProgress;
    LinearLayout tidakhasil;
    String file;

    // Progress Dialog
    private ProgressDialog pDialog;

    // Progress dialog type (0 - for Horizontal progress bar)
    public static final int progress_bar_type = 0;

    // File url to download
    File folder, folderpict;
    String txtjudul, txtfile, txtid;
    String email, nama, kelas, status;
    SessionManager session;
    HashMap<String, String> user;
    String jkelas;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list);
        session = new SessionManager(getApplicationContext());
        session.checkLogin();
        user = session.getUserDetails();


        lv = (ListView) findViewById(R.id.list);
        ActionBar actionBar = getActionBar();

        // Enabling Up / Back navigation
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Berita");

        //intent

        Intent in = getIntent();
        nama = in.getStringExtra(JUDUL);
        kelas = in.getStringExtra(JUDUL2);
        status = in.getStringExtra(JUDUL3);

        contactList = new ArrayList<HashMap<String, String>>();
        // Loading INBOX in Background Thread
        tidakhasil = (LinearLayout) findViewById(R.id.noitem);
        tidakhasil.setVisibility(View.GONE);

        //jkelas = dashboard.JUDULKElas.replaceAll(" ", "%20");
        if (dashboard.JUDULSTATUS.equalsIgnoreCase("s")) {
            url = "http://lms.novalherdinata.net/beritaparsing.php?kelas=" + jkelas;
            //new LoadInbox().execute();
            LoadBeritaSiswa();
        }

        if (dashboard.JUDULSTATUS.equalsIgnoreCase("g")) {
            url = "http://lms.novalherdinata.net/beritaparsingguru.php?nama=" + user.get(SessionManager.KEY_NAME);
            //new LoadInbox().execute();
            LoadBeritaGuru();
        }
    }

    private void LoadBeritaSiswa() {

        System.out.println("kelas " + dashboard.JUDULKElas);
        linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
        linlaHeaderProgress.setVisibility(View.VISIBLE);

        ServiceBerita service = ApiGenerator.createService(ServiceBerita.class);
        Call<List<Berita>> call = service.getBeritaByKelas(dashboard.JUDULKElas);
        call.enqueue(new Callback<List<Berita>>() {
            @Override
            public void onResponse(Call<List<Berita>> call, Response<List<Berita>> response) {
                linlaHeaderProgress.setVisibility(View.GONE);
                List<Berita> list = response.body();
                for(Berita berita : list){

                    // creating new HashMap
                    HashMap<String, String> map = new HashMap<String, String>();

                    map.put(TAG_JUDUL, berita.getJudul());
                    map.put(TAG_DESKRIPSI, berita.getDeskripsi());
                    map.put(TAG_NAMA, berita.getPengguna().getName());
                    map.put(TAG_IDBERITA, berita.getIdberita());
                    map.put(TAG_TGLUPLOAD, berita.getTgl());

                    // adding HashList to ArrayList
                    contactList.add(map);
                }

                populateList();
            }

            @Override
            public void onFailure(Call<List<Berita>> call, Throwable t) {
                linlaHeaderProgress.setVisibility(View.GONE);
                DialogInfo.showErrorDialog(listberita.this, getString(R.string.error_connection));
            }
        });
    }



    private void LoadBeritaGuru() {
        linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
        linlaHeaderProgress.setVisibility(View.VISIBLE);

        ServiceBerita service = ApiGenerator.createService(ServiceBerita.class);
        Call<List<Berita>> call = service.getBeritaByEmail(user.get(SessionManager.KEY_EMAIL));
        call.enqueue(new Callback<List<Berita>>() {
            @Override
            public void onResponse(Call<List<Berita>> call, Response<List<Berita>> response) {
                linlaHeaderProgress.setVisibility(View.GONE);
                List<Berita> list = response.body();
                for(Berita berita : list){

                    // creating new HashMap
                    HashMap<String, String> map = new HashMap<String, String>();

                    map.put(TAG_JUDUL, berita.getJudul());
                    map.put(TAG_DESKRIPSI, berita.getDeskripsi());
                    map.put(TAG_NAMA, berita.getPengguna().getName());
                    map.put(TAG_IDBERITA, berita.getIdberita());
                    map.put(TAG_TGLUPLOAD, berita.getTgl());

                    // adding HashList to ArrayList
                    contactList.add(map);
                }

                populateList();
            }

            @Override
            public void onFailure(Call<List<Berita>> call, Throwable t) {
                linlaHeaderProgress.setVisibility(View.GONE);
                DialogInfo.showErrorDialog(listberita.this, getString(R.string.error_connection));
            }
        });
    }

    private void populateList() {
        adapter2 = new BeritaAdapter(listberita.this, contactList);
        adapter2.notifyDataSetChanged();
        lv.setAdapter(adapter2);
        lv.invalidateViews();

        lv.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent,
                                    View view, int position, long id) {

            }
        });
    }

    class LoadInbox extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
            linlaHeaderProgress.setVisibility(View.VISIBLE);
        }

        /**
         * getting Inbox JSON
         */
        protected String doInBackground(String... args) {
            // Creating JSON Parser instance
            JSONParser jParser = new JSONParser();

            // getting JSON string from URL
            JSONObject json = jParser.getJSONFromUrl(url);

            try {
                // Getting Array of Contacts
                contacts = json.getJSONArray(TAG_CONTACTS);

                // looping through All Contacts
                for (int i = 0; i < contacts.length(); i++) {
                    JSONObject c = contacts.getJSONObject(i);

                    // Storing each json item in variable
                    String judul = c.getString(TAG_JUDUL);
                    String deskripsi = c.getString(TAG_DESKRIPSI);
                    String nama = c.getString(TAG_NAMA);
                    String idberita = c.getString(TAG_IDBERITA);
                    String tgl = c.getString(TAG_TGLUPLOAD);

                    // creating new HashMap
                    HashMap<String, String> map = new HashMap<String, String>();

                    // adding each child node to HashMap key => value
                    // adding each child node to HashMap key => value
                    map.put(TAG_JUDUL, judul);
                    map.put(TAG_DESKRIPSI, deskripsi);
                    map.put(TAG_NAMA, nama);
                    map.put(TAG_IDBERITA, idberita);
                    map.put(TAG_TGLUPLOAD, tgl);

                    // adding HashList to ArrayList
                    contactList.add(map);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products
            // HIDE THE SPINNER AFTER LOADING FEEDS
            linlaHeaderProgress.setVisibility(View.GONE);

            if (contactList.size() < 1) {
                tidakhasil.setVisibility(View.VISIBLE);
            }
            // LoadMore button
            if (contactList.size() > 9) {


            }

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    adapter2 = new BeritaAdapter(listberita.this, contactList);
                    adapter2.notifyDataSetChanged();
                    lv.setAdapter(adapter2);
                    lv.invalidateViews();

                    lv.setOnItemClickListener(new OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent,
                                                View view, int position, long id) {

                        }
                    });
                }
            });

        }
    }

    private class loadMoreListView extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            // Showing progress dialog before sending http request
            pDialog = new ProgressDialog(listberita.this);
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        protected Void doInBackground(Void... unused) {
            // Creating JSON Parser instance
            current_page += 1;

            if (dashboard.JUDULSTATUS.equalsIgnoreCase("s")) {

                url = "http://lms.novalherdinata.net/coursemurid.php?nama=" + jkelas + "&page="
                        + current_page;
            } else if (dashboard.JUDULSTATUS.equalsIgnoreCase("g")) {
                url = "http://lms.novalherdinata.net/courseparsing.php?nama=" + user.get(SessionManager.KEY_NAME) + "&page="
                        + current_page;
            }
            JSONParser jParser = new JSONParser();

            // getting JSON string from URL
            JSONObject json = jParser.getJSONFromUrl(url);

            try {
                // Getting Array of Contacts
                contacts = json.getJSONArray(TAG_CONTACTS);

                // looping through All Contacts
                for (int i = 0; i < contacts.length(); i++) {
                    JSONObject c = contacts.getJSONObject(i);

                    // Storing each json item in variable
                    String judul = c.getString(TAG_JUDUL);
                    String deskripsi = c.getString(TAG_DESKRIPSI);
                    String nama = c.getString(TAG_NAMA);
                    String idberita = c.getString(TAG_IDBERITA);
                    String tgl = c.getString(TAG_TGLUPLOAD);

                    // creating new HashMap
                    HashMap<String, String> map = new HashMap<String, String>();

                    // adding each child node to HashMap key => value
                    // adding each child node to HashMap key => value
                    map.put(TAG_JUDUL, judul);
                    map.put(TAG_DESKRIPSI, deskripsi);
                    map.put(TAG_NAMA, nama);
                    map.put(TAG_IDBERITA, idberita);
                    map.put(TAG_TGLUPLOAD, tgl);
                    // adding HashList to ArrayList
                    contactList.add(map);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(Void unused) {
            // closing progress dialog
            pDialog.dismiss();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    int currentPosition = lv.getFirstVisiblePosition();
                    adapter2 = new BeritaAdapter(listberita.this, contactList);
                    adapter2.notifyDataSetChanged();
                    lv.setAdapter(adapter2);
                    lv.invalidateViews();
                    lv.setSelectionFromTop(currentPosition + 1, 0);
                    lv.setOnItemClickListener(new OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent,
                                                View view, int position, long id) {

                        }
                    });
                }
            });
        }
    }

    /**
     * Showing Dialog
     */
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type:
                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Download file. Mohon Tunggu...");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }

    /**
     * Background Async Task to download file
     */
    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                String extStorageDirectory = Environment
                        .getExternalStorageDirectory().toString();
                folderpict = new File(extStorageDirectory, "/MobileLearning");
                if (!folderpict.exists()) {
                    folderpict.mkdir();
                } else {
                    folder = new File(extStorageDirectory,
                            "/MobileLearning/Course");
                    if (!folder.exists()) {
                        folder.mkdir();
                    }

                }

                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(),
                        8192);

                // Output stream to write file
                OutputStream output = new FileOutputStream(extStorageDirectory
                        + "/MobileLearning/Course/" + txtjudul + ".pdf");

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();
                MediaScannerConnection
                        .scanFile(listberita.this,
                                new String[]{extStorageDirectory
                                        + "/MobileLearning/Course/" + txtjudul + ".pdf"},
                                new String[]{"application/pdf"}, null);
                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            dismissDialog(progress_bar_type);
            showPdf();

            // Displaying downloaded image into image view
            // Reading image path from sdcard
        }

    }

    public void showPdf() {

        String extStorageDirectory = Environment
                .getExternalStorageDirectory().toString();
        File file = new File(extStorageDirectory
                + "/MobileLearning/Course/" + txtjudul + ".pdf");
        PackageManager packageManager = getPackageManager();
        Intent testIntent = new Intent(Intent.ACTION_VIEW);
        testIntent.setType("application/pdf");
        List list = packageManager.queryIntentActivities(testIntent, PackageManager.MATCH_DEFAULT_ONLY);
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        Uri uri = Uri.fromFile(file);
        intent.setDataAndType(uri, "application/pdf");
        startActivity(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        MenuItem tambah = (MenuItem) menu.findItem(R.id.action_tambah);

        if (dashboard.JUDULSTATUS.equalsIgnoreCase("g")) {
            tambah.setVisible(true);
        }
        if (dashboard.JUDULSTATUS.equalsIgnoreCase("s")) {
            tambah.setVisible(false);
        }

        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title

        // Handle action bar actions click
        switch (item.getItemId()) {
            case R.id.action_tambah:
                Intent in = new Intent(listberita.this, inputberita.class);
                startActivity(in);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // do something on back.
            Intent intent = new Intent(listberita.this, dashboard.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

}