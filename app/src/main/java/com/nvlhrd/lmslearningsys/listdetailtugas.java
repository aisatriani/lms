package com.nvlhrd.lmslearningsys;


import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import com.nvlhrd.lmslearningsys.adapter.ImageLoader;
import com.nvlhrd.lmslearningsys.adapter.JSONParser;
import com.nvlhrd.lmslearningsys.api.ApiGenerator;
import com.nvlhrd.lmslearningsys.api.ServiceTugas;
import com.nvlhrd.lmslearningsys.api.pojo.NilaiTugas;
import com.nvlhrd.lmslearningsys.jawabonlinetext.LoadInbox2;
import com.nvlhrd.lmslearningsys.library.SessionManager;
import com.nvlhrd.lmslearningsys.listcourse.DownloadFileFromURL;


import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Typeface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.opengl.Visibility;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.webkit.MimeTypeMap;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class listdetailtugas extends Activity {
	
	private ProgressDialog pDialog;
	// url to make request
	static String url;
	JSONArray contacts = null;

	TextView judul, ket;
	static final String JUDUL ="a", JUDUL2="b", JUDUL3="c", JUDUL4="d",JUDUL5="e",JUDUL6="f",JUDUL7="g",JUDUL8="h",JUDUL9="i",JUDUL10="j",JUDUL11="k",JUDUL12="l";
	static String foto;
	String namenya, des, fotodes;
	String PLACE_LATITUDE, PLACE_LONGITUDE;
	ImageView map,telp;
	LinearLayout aa;
	 LinearLayout count_layout;
	 int count = 0;
	 String deskrip, fotdet1, fotdet2, fotdet3, klan;
	 TextView txtdes, txtfotdet1, txtfotdet2;
	 ArrayList<String> list = new ArrayList<String>();
	 double screenInches;
	 ScrollView scroll;
	 
	 TextView namaprofile, kelasprofile,desktugas;
	 TextView  txtask, txtdeskask;
	String txtjudul, txtdeskripsi, txtidtugas, txtidguru, txtnama, txtfoto, txtfile, txttglupload;
	Button  upload;
	public static final int progress_bar_type = 0;
	File folder, folderpict;
	
	 private static final int SELECT_FILE1 = 1;
	  String selectedPath1 = "NONE";
	  HttpEntity resEntity;
	  ImageView fileupload;
	  ProgressDialog progressDialog;
	  SessionManager session;
		HashMap<String, String> user;
		
		public static final String TAG_CONTACTS = "info";
		public static final String TAG_NAMA = "nama";
		private ProgressDialog pDialog3;
		String url2;
		
		String id;
		Cursor cursor;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
		setContentView(R.layout.list_rowtugadetails);
		session = new SessionManager(getApplicationContext());
        session.checkLogin();
		 user = session.getUserDetails();
		
		namaprofile = (TextView) findViewById(R.id.namaprofile);
		desktugas = (TextView) findViewById(R.id.deskripsitugas);
		upload = (Button) findViewById(R.id.btnuploadtugas);
		fileupload = (ImageView) findViewById(R.id.fotofilecourse);
		
		Intent in = getIntent();
		 txtjudul = in.getStringExtra(JUDUL);
		 txtdeskripsi = in.getStringExtra(JUDUL2);
		 txtfile = in.getStringExtra(JUDUL3);
		 txtidguru = in.getStringExtra(JUDUL4);
		 txtnama = in.getStringExtra(JUDUL5);
		 txttglupload = in.getStringExtra(JUDUL6);
		 txtidtugas = in.getStringExtra(JUDUL7);
		 txtfoto = in.getStringExtra(JUDUL8);
		 
		 url = "http://lms.novalherdinata.net/nilaitugascek.php?nama="+txtidtugas+"&idsiswa="+user.get(SessionManager.KEY_NAME);	
		 //new LoadInbox2().execute();
		checkTugas(txtidtugas,user.get(SessionManager.KEY_EMAIL));

		 
		 //txtfile = txtfile.replaceAll(" ", "%20");
		 
		 namaprofile.setText(""+txtnama);
		 desktugas.setText(""+txtdeskripsi);
		 
			ActionBar actionBar = getActionBar();

			// Enabling Up / Back navigation
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setTitle("Tugas");
        
        runOnUiThread(new Runnable() {
			public void run() {
				 ImageView image = (ImageView) findViewById(R.id.foto);
			        ImageLoader imgLoader = new ImageLoader(getApplicationContext());
			        imgLoader.DisplayImage(txtfoto, image);
							}
						});
        
  
        
        upload.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String idtugas = txtidtugas.toString();
				String idguru = txtidguru.toString();
				String siswa = "rahmat@gmail.com";
				String kelas = "XII RPL 2";
				
				 if(!(selectedPath1.trim().equalsIgnoreCase("NONE"))){
					 doinput();
				progressDialog = ProgressDialog.show(listdetailtugas.this, "", "Proses.....", false);
                Thread thread=new Thread(new Runnable(){
                       public void run(){
                    doFileUpload();
                           runOnUiThread(new Runnable(){
                               public void run() {
                                   if(progressDialog.isShowing())
                                       progressDialog.dismiss();
                                   Toast.makeText(listdetailtugas.this, "Berhasil di tambahkan", Toast.LENGTH_SHORT).show();
                				   Intent intent = new Intent(listdetailtugas.this, listtugas.class);
                			        startActivity(intent);	
                               }
                           });
                       }
               });
               thread.start();
               
			        
				 }
			}
			
		});
        
	fileupload.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				openGallery(SELECT_FILE1);
			}
		});
	
	}

	private void checkTugas(String txtidtugas, String s) {

		pDialog = new ProgressDialog(
				listdetailtugas.this);
		pDialog.setMessage("Proses..");
		pDialog.setIndeterminate(true);
		pDialog.setCancelable(false);
		pDialog.show();

		ServiceTugas service = ApiGenerator.createService(ServiceTugas.class);
		Call<NilaiTugas> call = service.checkTugas(txtidtugas, s);
		call.enqueue(new Callback<NilaiTugas>() {
			@Override
			public void onResponse(Call<NilaiTugas> call, Response<NilaiTugas> response) {
				pDialog.dismiss();
				if(response.isSuccessful()){
					if(response.body() != null){
						Toast.makeText(listdetailtugas.this, "Anda sudah mengumpulkan", Toast.LENGTH_LONG).show();
						Intent intent = new Intent(listdetailtugas.this, listtugas.class);
						startActivity(intent);
					}
				}
			}

			@Override
			public void onFailure(Call<NilaiTugas> call, Throwable t) {
				pDialog.dismiss();
			}
		});

	}
	
	 public void openGallery(int req_code){
		 
	        Intent intent = new Intent("com.sec.android.app.myfiles.PICK_DATA");
	        intent.setType("application/pdf");
	        intent.setAction(Intent.ACTION_GET_CONTENT);
	        startActivityForResult(Intent.createChooser(intent,"Select file to upload "), req_code);
	   }
	 
	 public void onActivityResult(int requestCode, int resultCode, Intent data) {
		 
	        if (resultCode == RESULT_OK) {
	            Uri selectedImageUri = data.getData();
	            if (requestCode == SELECT_FILE1)
	            {
	                selectedPath1 = getPath(selectedImageUri);
//	                fotologo.setImageResource(R.drawable.checklis);
	                cursor.close();
	                fileupload.setImageResource(R.drawable.checklis);
	                System.out.println("selectedPath1 : " + selectedPath1);
	            }
	        }
	        }
	 
	  public String getPath(Uri uri) {
		  ContentResolver cr =getContentResolver();


		  String sortOrder = null;
		  
		  String[] projection = { MediaStore.Files.FileColumns.DATA };

		  String selectionMimeType = MediaStore.Files.FileColumns.MIME_TYPE + "=?";

		  String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension("pdf"); 

		  String[] selectionArgsMp3 = new String[]{ mimeType };

		  cursor = cr.query(uri, projection, selectionMimeType, selectionArgsMp3, sortOrder);
		   int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
	        cursor.moveToFirst();
	        
	        return cursor.getString(column_index);
	    }
	  
	  private void doFileUpload(){
		  
	        File file1 = new File(selectedPath1);
	        //String urlString = "http://lms.novalherdinata.net/uploadtugas.php?idiklan="+txtidtugas;
		    String urlString = Config.URL_API + "jawabtugas/"+txtidtugas+"/upload";
	        try
	        {
	             HttpClient client = new DefaultHttpClient();
	             HttpPost post = new HttpPost(urlString);
	             FileBody bin1 = new FileBody(file1);
	             MultipartEntity reqEntity = new MultipartEntity();
	             reqEntity.addPart("uploadedfile1", bin1);
	             reqEntity.addPart("user", new StringBody("User"));
	             post.setEntity(reqEntity);
	             HttpResponse response = client.execute(post);
	             resEntity = response.getEntity();
	             final String response_str = EntityUtils.toString(resEntity);
	             if (resEntity != null) {
	                 Log.i("RESPONSE",response_str);
	                 runOnUiThread(new Runnable(){
	                        public void run() {
	                             try {
	                                Toast.makeText(getApplicationContext(),"Upload Complete. Check the server uploads directory.", Toast.LENGTH_LONG).show();
	                            } catch (Exception e) {
	                                e.printStackTrace();
	                            }
	                           }
	                    });
	             }
	        }
	        catch (Exception ex){
	             Log.e("Debug", "error: " + ex.getMessage(), ex);
	        }
	      }
	  
	  private void doinput() {
			// TODO Auto-generated method stub

		  String idtugas = txtidtugas.toString();
			String idguru = txtidguru.toString();
			String siswa =  user.get(SessionManager.KEY_EMAIL);
			String kelas = dashboard.JUDULKElas;
			
			 //String input_data= "http://lms.novalherdinata.net/inputtugas.php"; //URL website anda dengan file insert.php yang telah dibuat
		     String input_data = Config.URL_API + "tugas/jawab";
	            
	         HttpClient httpClient = new DefaultHttpClient();  
	         HttpPost httpPost = new HttpPost(input_data);  
	         ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();  
	         param.add(new BasicNameValuePair("idtugas", idtugas)); 
	         param.add(new BasicNameValuePair("idguru", idguru)); 
	        param.add(new BasicNameValuePair("idsiswa", siswa));
	        param.add(new BasicNameValuePair("kelas", kelas));
		    param.add(new BasicNameValuePair("file", "test"));
	         try {  
	              httpPost.setEntity(new UrlEncodedFormEntity(param));  
	              HttpResponse httpRespose = httpClient.execute(httpPost);  
	             HttpEntity httpEntity = httpRespose.getEntity();  
	             InputStream in = httpEntity.getContent();  
	             BufferedReader read = new BufferedReader(new InputStreamReader(in));  
	            
	             String isi= "";  
	             String baris= "";  
	            
	             while((baris = read.readLine())!=null){  
	                isi+= baris;  
	             }  
	               
	             //Jika isi tidak sama dengan "null " maka akan tampil Toast "Berhasil" sebaliknya akan tampil "Gagal"  
	             if(!isi.equals("null")){                    
	             }else{  
	             }  
	            
	       } catch (ClientProtocolException e) {  
	          // TODO Auto-generated catch block  
	          e.printStackTrace();  
	       } catch (IOException e) {  
	          // TODO Auto-generated catch block  
	          e.printStackTrace();  
	       }  
	    	
		}
	
	/**
	 * Showing Dialog
	 * */
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case progress_bar_type:
		
			return pDialog;
		default:
			return null;
		}
	}

	class DownloadFileFromURL extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Bar Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(listdetailtugas.this);
			pDialog.setMessage("Downloading file. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setMax(100);
			pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		/**
		 * Downloading file in background thread
		 * */
		@Override
		protected String doInBackground(String... f_url) {
			int count;
			try {
				String extStorageDirectory = Environment
						.getExternalStorageDirectory().toString();
				folderpict = new File(extStorageDirectory, "/MobileLearning");
				if (!folderpict.exists()) {
					folderpict.mkdir();
				} else {
					folder = new File(extStorageDirectory,
							"/MobileLearning/Course");
					if (!folder.exists()) {
						folder.mkdir();
					}

				}

				URL url = new URL(f_url[0]);
				URLConnection conection = url.openConnection();
				conection.connect();
				// getting file length
				int lenghtOfFile = conection.getContentLength();

				// input stream to read file - with 8k buffer
				InputStream input = new BufferedInputStream(url.openStream(),
						8192);

				// Output stream to write file
				OutputStream output = new FileOutputStream(extStorageDirectory
						+ "/MobileLearning/Course/" + txtjudul+".pdf");

				byte data[] = new byte[1024];

				long total = 0;

				while ((count = input.read(data)) != -1) {
					total += count;
					// publishing the progress....
					// After this onProgressUpdate will be called
					publishProgress("" + (int) ((total * 100) / lenghtOfFile));

					// writing data to file
					output.write(data, 0, count);
				}

				// flushing output
				output.flush();
				MediaScannerConnection
						.scanFile(listdetailtugas.this,
								new String[] { extStorageDirectory
										+ "/MobileLearning/Course/" + txtjudul+".pdf" },
								new String[] { "application/pdf" }, null);
				// closing streams
				output.close();
				input.close();

			} catch (Exception e) {
				Log.e("Error: ", e.getMessage());
			}

			return null;
		}

		/**
		 * Updating progress bar
		 * */
		protected void onProgressUpdate(String... progress) {
			// setting progress percentage
			pDialog.show();
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		@Override
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after the file was downloaded
			pDialog.dismiss();
			showPdf();

			// Displaying downloaded image into image view
			// Reading image path from sdcard
		}

	}
	
	 public void showPdf()
    {
		 
		 String extStorageDirectory = Environment
					.getExternalStorageDirectory().toString();
        File file = new File( extStorageDirectory
					+ "/MobileLearning/Course/" + txtjudul+".pdf" );
        PackageManager packageManager = getPackageManager();
        Intent testIntent = new Intent(Intent.ACTION_VIEW);
        testIntent.setType("application/pdf");
        List list = packageManager.queryIntentActivities(testIntent, PackageManager.MATCH_DEFAULT_ONLY);
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        Uri uri = Uri.fromFile(file);
        intent.setDataAndType(uri, "application/pdf");
        startActivity(intent);
    }
	 

		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.tugas, menu);
			
			MenuItem tambah = (MenuItem) menu.findItem(R.id.action_download);
			
			
			return super.onCreateOptionsMenu(menu);
			
		}
		
		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			// toggle nav drawer on selecting action bar app icon/title
		
			// Handle action bar actions click
			switch (item.getItemId()) {
			case R.id.action_download:
				
				if(txtfile.equalsIgnoreCase("")){
					Toast.makeText(listdetailtugas.this, "Tidak ada file", Toast.LENGTH_SHORT).show();
				}
				else{
				
				new DownloadFileFromURL().execute(txtfile);
				}
				break;
			default:
				return super.onOptionsItemSelected(item);
			}
			return true;
		}


}
