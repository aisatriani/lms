/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.nvlhrd.lmslearningsys;

public final class R {
    public static final class attr {
    }
    public static final class bool {
        public static final int use_activity=0x7f070000;
        public static final int use_provider=0x7f070001;
    }
    public static final class color {
        public static final int genre=0x7f090000;
        public static final int list_divider=0x7f090001;
        public static final int list_row_end_color=0x7f090002;
        public static final int list_row_hover_end_color=0x7f090003;
        public static final int list_row_hover_start_color=0x7f090004;
        public static final int list_row_start_color=0x7f090005;
        public static final int year=0x7f090006;
    }
    public static final class dimen {
        public static final int activity_horizontal_margin=0x7f080000;
        public static final int activity_vertical_margin=0x7f080001;
        public static final int genre=0x7f080002;
        public static final int list_item_padding=0x7f080003;
        public static final int list_padding=0x7f080004;
        public static final int rating=0x7f080005;
        public static final int title=0x7f080006;
        public static final int year=0x7f080007;
    }
    public static final class drawable {
        public static final int bg_card=0x7f020000;
        public static final int btn_events=0x7f020001;
        public static final int btn_friends=0x7f020002;
        public static final int btn_messages=0x7f020003;
        public static final int btn_newsfeed=0x7f020004;
        public static final int btn_photos=0x7f020005;
        public static final int btn_places=0x7f020006;
        public static final int checklis=0x7f020007;
        public static final int course=0x7f020008;
        public static final int courses=0x7f020009;
        public static final int events_default=0x7f02000a;
        public static final int events_pressed=0x7f02000b;
        public static final int events_selected=0x7f02000c;
        public static final int forum=0x7f02000d;
        public static final int friends_default=0x7f02000e;
        public static final int friends_pressed=0x7f02000f;
        public static final int friends_selected=0x7f020010;
        public static final int headerlogin=0x7f020011;
        public static final int ic_action_chat=0x7f020012;
        public static final int ic_action_download=0x7f020013;
        public static final int ic_action_event=0x7f020014;
        public static final int ic_action_new=0x7f020015;
        public static final int ic_chooser=0x7f020016;
        public static final int ic_file=0x7f020017;
        public static final int ic_folder=0x7f020018;
        public static final int ic_launcher=0x7f020019;
        public static final int ic_provider=0x7f02001a;
        public static final int list_row_bg=0x7f02001b;
        public static final int list_row_bg_hover=0x7f02001c;
        public static final int list_row_selector=0x7f02001d;
        public static final int logo=0x7f02001e;
        public static final int logohader=0x7f02001f;
        public static final int messages_default=0x7f020020;
        public static final int messages_pressed=0x7f020021;
        public static final int messages_selected=0x7f020022;
        public static final int news=0x7f020023;
        public static final int news_feed_default=0x7f020024;
        public static final int news_feed_pressed=0x7f020025;
        public static final int news_feed_selected=0x7f020026;
        public static final int photos_default=0x7f020027;
        public static final int photos_pressed=0x7f020028;
        public static final int photos_selected=0x7f020029;
        public static final int places_default=0x7f02002a;
        public static final int places_pressed=0x7f02002b;
        public static final int places_selected=0x7f02002c;
        public static final int report=0x7f02002d;
        public static final int teditprofile=0x7f02002e;
        public static final int tforumt=0x7f02002f;
        public static final int tlogout=0x7f020030;
        public static final int trapor=0x7f020031;
        public static final int ttsugas=0x7f020032;
        public static final int tugas=0x7f020033;
        public static final int upload=0x7f020034;
    }
    public static final class id {
        public static final int LinearLayouthasil=0x7f0b0002;
        public static final int action_download=0x7f0b0072;
        public static final int action_notif=0x7f0b0071;
        public static final int action_tambah=0x7f0b0070;
        public static final int alan=0x7f0b0052;
        public static final int asd=0x7f0b0054;
        public static final int atas=0x7f0b0050;
        public static final int atasatas=0x7f0b001c;
        public static final int beritades=0x7f0b0066;
        public static final int beritajudul=0x7f0b0065;
        public static final int btnLinkToLoginScreen=0x7f0b000e;
        public static final int btnLinkToRegisterScreen=0x7f0b0007;
        public static final int btnNo=0x7f0b0019;
        public static final int btnRegister=0x7f0b000d;
        public static final int btnYes=0x7f0b0018;
        public static final int btn_events=0x7f0b0030;
        public static final int btn_friends=0x7f0b002d;
        public static final int btn_messages=0x7f0b002e;
        public static final int btn_news_feed=0x7f0b002c;
        public static final int btn_photos=0x7f0b0031;
        public static final int btn_places=0x7f0b002f;
        public static final int btnberita=0x7f0b0067;
        public static final int btndate=0x7f0b0063;
        public static final int btneditpass=0x7f0b003b;
        public static final int btnforum=0x7f0b006a;
        public static final int btnkomenonline=0x7f0b0035;
        public static final int btnkomentar=0x7f0b006c;
        public static final int btnlogin=0x7f0b0006;
        public static final int btnnilai=0x7f0b0048;
        public static final int btnonline=0x7f0b0064;
        public static final int btnupload=0x7f0b006f;
        public static final int btnuploadtugas=0x7f0b0056;
        public static final int coursedeskripsi=0x7f0b006e;
        public static final int coursejudul=0x7f0b006d;
        public static final int desask=0x7f0b0023;
        public static final int deskripsi=0x7f0b0041;
        public static final int deskripsitugas=0x7f0b0053;
        public static final int downloadfile=0x7f0b004b;
        public static final int email=0x7f0b0037;
        public static final int emailuser=0x7f0b0013;
        public static final int file=0x7f0b0044;
        public static final int first_tab=0x7f0b0032;
        public static final int first_text=0x7f0b0027;
        public static final int forumdeskripsi=0x7f0b0069;
        public static final int forumjudul=0x7f0b0068;
        public static final int foto=0x7f0b001e;
        public static final int fotofilecourse=0x7f0b0055;
        public static final int frame_container=0x7f0b002b;
        public static final int home_root=0x7f0b0010;
        public static final int idberita=0x7f0b0042;
        public static final int idcourse=0x7f0b004d;
        public static final int idforum=0x7f0b004e;
        public static final int idguru=0x7f0b0059;
        public static final int idtugas=0x7f0b0058;
        public static final int iduser=0x7f0b004f;
        public static final int jawaban=0x7f0b004a;
        public static final int judultugasrapor=0x7f0b003c;
        public static final int kelas=0x7f0b0038;
        public static final int kelasprofile=0x7f0b0020;
        public static final int kelasrapor=0x7f0b003d;
        public static final int kelasuser=0x7f0b0015;
        public static final int komenlihat=0x7f0b0024;
        public static final int komenonline=0x7f0b0034;
        public static final int komentar=0x7f0b0051;
        public static final int komentardeskripsi=0x7f0b006b;
        public static final int linlaHeaderProgress=0x7f0b0011;
        public static final int list=0x7f0b0025;
        public static final int list_slidermenu=0x7f0b005c;
        public static final int loginEmail=0x7f0b0003;
        public static final int loginPassword=0x7f0b0004;
        public static final int login_error=0x7f0b0005;
        public static final int loginpass=0x7f0b005e;
        public static final int loginuser=0x7f0b005d;
        public static final int matpel=0x7f0b004c;
        public static final int nama=0x7f0b0036;
        public static final int namadialog=0x7f0b0017;
        public static final int namaguru=0x7f0b0040;
        public static final int namagurutugas=0x7f0b0057;
        public static final int namaprofile=0x7f0b001f;
        public static final int namauser=0x7f0b0014;
        public static final int nilai=0x7f0b0046;
        public static final int nilaiform=0x7f0b0047;
        public static final int nilairapor=0x7f0b003e;
        public static final int noitem=0x7f0b001a;
        public static final int onlinejudul=0x7f0b0061;
        public static final int onlinetanya=0x7f0b0062;
        public static final int pass1=0x7f0b0039;
        public static final int pass2=0x7f0b003a;
        public static final int profile=0x7f0b001d;
        public static final int progressBar1=0x7f0b0012;
        public static final int registerEmail=0x7f0b0009;
        public static final int registerName=0x7f0b0008;
        public static final int registerPassword=0x7f0b000a;
        public static final int registerPasswordconf=0x7f0b000b;
        public static final int register_error=0x7f0b000c;
        public static final int registerguru=0x7f0b0060;
        public static final int registersiswa=0x7f0b005f;
        public static final int sasa=0x7f0b0049;
        public static final int scrollView1=0x7f0b0000;
        public static final int second_tab=0x7f0b0033;
        public static final int second_text=0x7f0b0028;
        public static final int slideshow_layout=0x7f0b0001;
        public static final int spinnerkelas=0x7f0b000f;
        public static final int statususer=0x7f0b0016;
        public static final int tableRow1=0x7f0b0026;
        public static final int tanya=0x7f0b0021;
        public static final int textView1=0x7f0b001b;
        public static final int textView12=0x7f0b0029;
        public static final int textVieww1=0x7f0b005b;
        public static final int tgldeadline=0x7f0b005a;
        public static final int tglupload=0x7f0b0043;
        public static final int title=0x7f0b003f;
        public static final int txtask=0x7f0b0022;
        public static final int txtfoto=0x7f0b0045;
        public static final int viewPager=0x7f0b002a;
    }
    public static final class layout {
        public static final int activity_login=0x7f030000;
        public static final int activity_loginguru=0x7f030001;
        public static final int activity_registerguru=0x7f030002;
        public static final int activity_registersiswa=0x7f030003;
        public static final int dashboard_layout=0x7f030004;
        public static final int dashboard_layoutguru=0x7f030005;
        public static final int dialogrubahnama=0x7f030006;
        public static final int file=0x7f030007;
        public static final int footer_layout=0x7f030008;
        public static final int forumdetail=0x7f030009;
        public static final int forumhome=0x7f03000a;
        public static final int fragment_layout=0x7f03000b;
        public static final int fragment_layoutguru=0x7f03000c;
        public static final int indicatormenu=0x7f03000d;
        public static final int jawabonlinetext=0x7f03000e;
        public static final int layout_profil=0x7f03000f;
        public static final int layout_rapor=0x7f030010;
        public static final int list=0x7f030011;
        public static final int list_berita=0x7f030012;
        public static final int list_item=0x7f030013;
        public static final int list_itemforum=0x7f030014;
        public static final int list_itemgurunilai=0x7f030015;
        public static final int list_itemgurunilaionline=0x7f030016;
        public static final int list_row=0x7f030017;
        public static final int list_rowforum=0x7f030018;
        public static final int list_rowkomentar=0x7f030019;
        public static final int list_rowtugadetails=0x7f03001a;
        public static final int list_rowtugas=0x7f03001b;
        public static final int login=0x7f03001c;
        public static final int loginac=0x7f03001d;
        public static final int onlinetext=0x7f03001e;
        public static final int tambahberita=0x7f03001f;
        public static final int tambahforum=0x7f030020;
        public static final int tambahkomentar=0x7f030021;
        public static final int test=0x7f030022;
        public static final int uploadcourse=0x7f030023;
        public static final int uploadtugas=0x7f030024;
    }
    public static final class menu {
        public static final int forumkomentar=0x7f0a0000;
        public static final int main=0x7f0a0001;
        public static final int maindashboard=0x7f0a0002;
        public static final int tugas=0x7f0a0003;
    }
    public static final class string {
        public static final int action_settings=0x7f050004;
        public static final int app_name=0x7f050005;
        public static final int choose_file=0x7f050000;
        public static final int empty_directory=0x7f050001;
        public static final int error_connection=0x7f050006;
        public static final int error_selecting_file=0x7f050002;
        public static final int hello_world=0x7f050007;
        public static final int internal_storage=0x7f050008;
        public static final int invalid_login=0x7f050009;
        public static final int storage_removed=0x7f050003;
    }
    public static final class style {
        /**  API 11 theme customizations can go here. 
 API 14 theme customizations can go here. 

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        
         */
        public static final int AppBaseTheme=0x7f060000;
        /**  All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f060001;
        public static final int DashboardButton=0x7f060002;
        public static final int FooterBar=0x7f060003;
        public static final int fileChooserName=0x7f060004;
        public static final int h_line=0x7f060005;
        public static final int indicator_style=0x7f060006;
        public static final int layout_f_w=0x7f060007;
        public static final int layout_fill=0x7f060008;
        public static final int layout_wrap=0x7f060009;
        public static final int myTheme=0x7f06000a;
        public static final int myTheme_ActionBar=0x7f06000b;
        public static final int myTheme_ActionBar_Text=0x7f06000c;
        public static final int myTheme_ActionBar_TitleTextStyle=0x7f06000d;
        public static final int text_option=0x7f06000e;
        public static final int text_title=0x7f06000f;
    }
    public static final class xml {
        public static final int mimetypes=0x7f040000;
    }
}
